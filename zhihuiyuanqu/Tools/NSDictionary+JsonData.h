//
//  NSDictionary+JsonData.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/15.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JsonData)

//字典转换成Json字符串
- (NSString *)jsonData;

@end
