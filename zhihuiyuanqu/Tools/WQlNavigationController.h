//
//  WQlNavigationController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/20.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WQlNavigationController : UINavigationController
+ (void)navgationWithTitle:(NSString *)title LeftItemTitle:(NSString *)leftItemTitle BarTinColor:(UIColor *)barTincolor TinColor:(UIColor *)tincolor Font:(UIFont *)size;

- (void)navgationWithTitle:(NSString *)title LeftItemTitle:(NSString *)leftItemTitle BarTinColor:(UIColor *)barTincolor TinColor:(UIColor *)tincolor Font:(UIFont *)size;
@end
