//
//  NSString+ZHMd5.h
//  zhihuiyuanqu
//
//  Created by leo on 16/5/30.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ZHMd5)

//md5加密
- (NSString*)md5;

@end
