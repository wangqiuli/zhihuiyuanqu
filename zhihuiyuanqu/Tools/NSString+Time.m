//
//  NSString+Time.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "NSString+Time.h"

@implementation NSString (Time)

- (NSString *)timeChange{
    //时间戳转时间的方法:
    NSString *timeStr = [self substringToIndex:10];
    NSTimeInterval timeInterval = [timeStr doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    //实例化一个NSDateFormatter对象
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    //设定时间格式,这里可以设置成自己需要的格式
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
//    [formatter setDateFormat:@"YYYY-MM-dd"];
    return [formatter stringFromDate:date];
}

- (NSString *)changeTime{
    NSDate *date = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%zd",[date timeIntervalSince1970]]];
    
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:timeSp];//或GMT
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:date];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate *destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
    
    NSString *timeSp1 = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%zd",[destinationDateNow timeIntervalSince1970]]];
    NSString *timeStr = [timeSp1 substringToIndex:13];
    return timeStr;
}

@end
