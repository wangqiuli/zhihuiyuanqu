//
//  WQLNavViewController.h
//  1232435
//
//  Created by leo on 16/4/19.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WQLNavViewController : UIViewController

- (void)navgationWithTitle:(NSString *)title LeftItemTitle:(NSString *)leftItemTitle BarTinColor:(UIColor *)barTincolor TinColor:(UIColor *)tincolor Font:(UIFont *)size;

@end
