//
//  ZHAlertViewController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHAlertViewController : UIAlertController

- (void)showAlert:(NSString *)title message:(NSString *)message click:(void (^)(BOOL ret))ret;

//可自动消失提示框
- (void)showAlertWithMessage:(NSString *)message;
@end
