//
//  TitleLebal.m
//  TopSliderBar
//
//  Created by Xiaoyue on 16/3/1.
//  Copyright © 2016年 李运洋. All rights reserved.
//

#import "TitleLebal.h"

@implementation TitleLebal

-(void)setScale:(CGFloat)scale
{
    self.textColor = [UIColor colorWithRed:scale green:0 blue:0 alpha:1];
    
    CGFloat transformScale = 1+scale*0.2;
    
    self.transform = CGAffineTransformMakeScale(transformScale, transformScale);
    
}


@end
