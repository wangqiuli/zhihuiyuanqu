//
//  WQLNavViewController.m
//  1232435
//
//  Created by leo on 16/4/19.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "WQLNavViewController.h"

@interface WQLNavViewController ()

@end

@implementation WQLNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)navgationWithTitle:(NSString *)title LeftItemTitle:(NSString *)leftItemTitle BarTinColor:(UIColor *)barTincolor TinColor:(UIColor *)tincolor Font:(UIFont *)size{
    self.title = title;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:size,
       NSForegroundColorAttributeName:tincolor}];
    self.navigationController.navigationBar.barTintColor = barTincolor;
    
    //两侧按钮颜色
    self.navigationController.navigationBar.tintColor = tincolor;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:leftItemTitle style:UIBarButtonItemStyleDone target:self action:@selector(leftBatButtonItemAction)];
    
}
- (void)leftBatButtonItemAction{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
