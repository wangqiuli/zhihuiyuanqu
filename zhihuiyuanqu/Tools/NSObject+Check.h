//
//  NSObject+Check.h
//  封装
//
//  Created by leo on 16/5/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSObject (Check)

typedef void (^clickBlock)(NSError *isError);

//手机号码验证
- (BOOL)validPhoneNumber:(NSString *)phone;

//发送验证码
- (void)SendVerificationCode:(NSString *)phone clickButton:(UIButton *)button clickBlock:(clickBlock)isError;

@end
