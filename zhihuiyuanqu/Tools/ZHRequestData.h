//
//  ZHRequestData.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^HttpSuccess)(id json);

typedef void (^HttpFailure)(NSError *error);

@interface ZHRequestData : NSObject

//get请求
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(HttpSuccess)success failure:(HttpFailure)failure;

//post请求
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(HttpSuccess)success failure:(HttpFailure)failure;

//基于BaseURL的post请求
+ (void)serviceUploadFilesWithSubpath:(NSString *)subpath parameters:(NSDictionary *)parameters success:(HttpSuccess)successMethod failure:(HttpFailure)failure;

/*!
 基于BaseURL,追加subpath和parameters的POST请求---上传文件
 */
+ (void)uploadFilesWithSubpath:(NSString *)subpath parameters:(NSDictionary *)parameters fileArray:(NSArray *)files success:(HttpSuccess)successMethod failure:(HttpFailure)failure;
@end
