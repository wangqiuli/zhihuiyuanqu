//
//  NSString+Time.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Time)

//时间戳转化成时间
- (NSString *)timeChange;

//时间转化时间戳
- (NSString *)changeTime;
@end
