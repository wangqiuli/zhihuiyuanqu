//
//  NSObject+getUserToken.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/3.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^userToken)(NSString *token);

@interface NSObject (getUserToken)

- (void)getUserToken:(userToken)token;

@end
