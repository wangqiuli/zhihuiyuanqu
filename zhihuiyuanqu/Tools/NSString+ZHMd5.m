//
//  NSString+ZHMd5.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/30.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "NSString+ZHMd5.h"
#import <math.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
@implementation NSString (ZHMd5)

#define CC_MD5_DIGEST_LENGTH 16

- (NSString *)md5{
    const char *str = [self UTF8String];
    if (str == NULL) {
        str = "";
    }
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    return [[NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]] uppercaseString];
}

@end
