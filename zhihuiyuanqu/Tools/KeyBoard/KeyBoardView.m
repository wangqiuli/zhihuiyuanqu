//
//  KeyBoardView.m
//  KeyBoard
//
//  Created by 杨路文 on 16/5/5.
//  Copyright © 2016年 杨路文. All rights reserved.
//  729225316@qq.com

#import "KeyBoardView.h"

@interface KeyBoardView()

@end
@implementation KeyBoardView

- (void)createTheBGView:(UIView *)bgview
{
    
    [self loadViewWithView:bgview];
    
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(releaseKeyBoardAction) name:@"releaseKeyBoard" object:nil];
}
- (void)releaseKeyBoardAction{
    [self.textView resignFirstResponder];
    [self keyboardResignFirstResponder];
}

- (void)loadViewWithView:(UIView *)bgview
{
    
    
    [bgview addSubview:self.toolBar];
    [self.toolBar addSubview:self.textView];
    [self.toolBar addSubview:self.sendButton];
    [self textviewBegainEdit];
}

- (void)textviewBegainEdit
{

    //弹出键盘
    [self.textView becomeFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{

        self.toolBar.frame = CGRectMake(0, SCREENHEIGHT-250-96-44, SCREENWIDTH, 44);
        self.isClick = YES;
        CGFloat height = CGRectGetMinY(self.toolBar.frame);
        if ([self.delegate respondsToSelector:@selector(theKeyBoardWillShowWithKeyBoardHeight:)]) {
            [self.delegate theKeyBoardWillShowWithKeyBoardHeight:height];
        }
    }];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    // 光标移到最后
    NSRange insertionPoint = NSMakeRange(textView.text.length, 0);
    textView.selectedRange = insertionPoint;
    [self textviewBegainEdit];
}

- (void)sendTheMessage:(UIButton *)btn
{
    if ([self.textView isFirstResponder]) {
        [self.textView resignFirstResponder];
        
    }
    [self keyboardResignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(getTheMessage:)]) {
        [self.delegate getTheMessage:self.textView.text];
    }
}

//结束编辑 调用这个方法(隐藏键盘)
- (void)keyboardResignFirstResponder
{
    
    [UIView animateWithDuration:0.25 animations:^{

        [self.toolBar removeFromSuperview];
        self.toolBar = nil;
        self.isClick = NO;
        if ([self.delegate respondsToSelector:@selector(theKeyBoardDidHiden)]) {
            [self.delegate theKeyBoardDidHiden];
        }

    }];
}

- (UIView *)toolBar{
    if (_toolBar==nil) {
        _toolBar = [[UIView alloc]initWithFrame:CGRectMake(0, SCREENHEIGHT, SCREENWIDTH, 44)];
        _toolBar.backgroundColor = [UIColor lightGrayColor];
    }
    return _toolBar;
}
- (UITextView *)textView{
    if (_textView==nil) {
        _textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 6, SCREENWIDTH-95, 32)];
        _textView.delegate = self;
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.font = [UIFont systemFontOfSize:17];
        [_textView.layer setMasksToBounds:YES];
        [_textView.layer setCornerRadius:5];
    }
    return _textView;
}

- (UIButton *)sendButton{
    if (_sendButton==nil) {
        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendButton.frame = CGRectMake(SCREENWIDTH-75, 7, 65, 30);
        _sendButton.clipsToBounds = YES;
        _sendButton.layer.cornerRadius = 5;
        _sendButton.backgroundColor = [UIColor blueColor];
        [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
        [_sendButton addTarget:self action:@selector(sendTheMessage:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendButton;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"releaseKeyBoard" object:nil];
}

@end
