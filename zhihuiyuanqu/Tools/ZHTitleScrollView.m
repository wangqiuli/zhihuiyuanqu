//
//  ZHTitleScrollView.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/13.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHTitleScrollView.h"
#define scrollWidth [UIScreen mainScreen].bounds.size.width

@interface ZHTitleScrollView ()
{
    CGFloat buttonWid;
}
@property (nonatomic,strong)NSMutableArray *buttonArr;
@property (nonatomic, strong) UIView *slideView;//滑动的视图

@end
@implementation ZHTitleScrollView

-(instancetype)initWithSmllScroll:(NSArray *)arrays{
    if (self = [super init]) {
        
        buttonWid = SCREEN_WIDTH/arrays.count;
        
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.frame = CGRectMake(0, 0, scrollWidth, 40);
        self.contentSize = CGSizeMake(SCREEN_WIDTH , 40);
        self.backgroundColor = [UIColor whiteColor];
        for (int i = 0; i < arrays.count; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(buttonWid*i, 0, buttonWid,40);
            [btn setTitle:arrays[i] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
            [btn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = i +100;
            [self addSubview:btn];
            [self.buttonArr addObject:btn];
        }
        [self createSlideView];
        self.index = 0;
        
    }
    
    return self;
}
//创建滑动的视图
-(void)createSlideView{
    _slideView = [[UIView alloc]initWithFrame:CGRectMake(0, 38, buttonWid, 2)];
    _slideView.backgroundColor = [UIColor redColor];
    [self addSubview:_slideView];
}
-(void)buttonClicked:(UIButton *)button{
    self.index = button.tag - 100;
    //    block的回调，当button的index发生变化时，将index的值传给视图控制器
    if (_changeIndexValue) {
        _changeIndexValue(_index);
    }
}
-(void)setIndex:(NSInteger)index{
    //将上一个button设置成未选中状态

    UIButton *notSelectedButton = self.buttonArr[_index];
    notSelectedButton.selected = NO;
    UIButton *selectedButton = self.buttonArr[index];
    selectedButton.selected = YES;
    
    //设置选中的Button居中
    CGFloat offSetX = selectedButton.frame.origin.x - scrollWidth/2;
    //1、当offSetX的值小于0时（即点击的是中心点左边的button时），让offSetX为0；反之，让offSetX的值为selectedButton.frame.origin.x-kScreenWidth/2+kButtonWidth/2
    offSetX = offSetX > 0 ?(offSetX + buttonWid/2):0;
    //2、在大于0的情况下，又大于self.contentSize.width - kScreenWidth时，offSetX 的值为self.contentSize.width - kScreenWidth，反之为：selectedButton.frame.origin.x-kScreenWidth/2+kButtonWidth/2
    offSetX = offSetX > self.contentSize.width - scrollWidth ? self.contentSize.width - scrollWidth :offSetX;
    [UIView animateWithDuration:0.2 animations:^{
        self.contentOffset = CGPointMake(offSetX, 0);
    }];
    
    
    _index = index;
    CGRect frame = _slideView.frame;
    frame.origin.x = _index * buttonWid;
    [UIView animateWithDuration:0.2 animations:^{
        _slideView.frame = frame;
    }];
}
- (NSMutableArray *)buttonArr{
    if (_buttonArr==nil) {
        _buttonArr = [NSMutableArray array];
    }
    return _buttonArr;
}
@end

