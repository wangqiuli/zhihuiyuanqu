//
//  ZHTitleScrollView.h
//  zhihuiyuanqu
//
//  Created by leo on 16/5/13.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHTitleScrollView : UIScrollView<UIScrollViewDelegate>

-(instancetype)initWithSmllScroll:(NSArray *)arrays;
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) void (^changeIndexValue)(NSInteger);

@end
