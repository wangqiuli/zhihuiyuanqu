//
//  WQlNavigationController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/20.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "WQlNavigationController.h"

@interface WQlNavigationController ()

@end

@implementation WQlNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

+ (void)navgationWithTitle:(NSString *)title LeftItemTitle:(NSString *)leftItemTitle BarTinColor:(UIColor *)barTincolor TinColor:(UIColor *)tincolor Font:(UIFont *)size{
    
    UINavigationBar *appearance = [UINavigationBar appearance];
    [appearance  setTitleTextAttributes:@{NSFontAttributeName:size,
       NSForegroundColorAttributeName:tincolor}];
    appearance.barTintColor = barTincolor;
    appearance.tintColor = tincolor;
    UINavigationItem *item = [[UINavigationItem alloc]init];
    
   item.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:leftItemTitle style:UIBarButtonItemStyleDone target:self action:@selector(leftBatButtonItemAction)];
}
- (void)navgationWithTitle:(NSString *)title LeftItemTitle:(NSString *)leftItemTitle BarTinColor:(UIColor *)barTincolor TinColor:(UIColor *)tincolor Font:(UIFont *)size{
    self.title = title;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:size,
       NSForegroundColorAttributeName:tincolor}];
    self.navigationController.navigationBar.barTintColor = barTincolor;
    
    //两侧按钮颜色
    self.navigationController.navigationBar.tintColor = tincolor;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:leftItemTitle style:UIBarButtonItemStyleDone target:self action:@selector(leftBatButtonItemAction)];
}
- (void)leftBatButtonItemAction{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
