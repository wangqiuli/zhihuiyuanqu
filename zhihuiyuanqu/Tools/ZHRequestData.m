//
//  ZHRequestData.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHRequestData.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
@implementation ZHRequestData

+(void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(HttpSuccess)success failure:(HttpFailure)failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = responseObject;
        if (dict.count==0) {
            
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"未连接到服务器"];
        }else{
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
        if (error) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"网络连接中断"];
        }
    }];
}

+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(HttpSuccess)success failure:(HttpFailure)failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = responseObject;
        if (dict.count==0) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"未连接到服务器"];
        }else{
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
        if (error) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"网络连接中断"];
        }
    }];
}

+ (void)serviceUploadFilesWithSubpath:(NSString *)subpath parameters:(NSDictionary *)parameters success:(HttpSuccess)successMethod failure:(HttpFailure)failure{
    
    //获取路径
    NSString *fileUrl = [PATH_HeaderURL stringByAppendingString:subpath];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", nil];
    [manager POST:fileUrl parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
#warning 几种网络问题，没有判断
        if ([responseObject[@"message"] isEqualToString:@"用户没有登录"]) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"请先登录"];
            return ;
        }
        if ([responseObject[@"message"] isEqualToString:@"接口调用成功！"]) {
            successMethod(responseObject);
        }
        if ([responseObject[@"message"] isEqualToString:@"接口调用失败！"]) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"接口调用失败！"];
            return ;
        }
//        NSDictionary *dict = responseObject;
//        if (dict==nil) {
//
//            ZHRequestData *data = [[ZHRequestData alloc]init];
//            [data showHint:@"未连接到服务器"];
//        }else{
        
//        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if ([error.domain rangeOfString:@"未能连接到服务器"].location !=NSNotFound || [error.domain rangeOfString:@"not found (404)"].location !=NSNotFound) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"未能连接到服务器"];
            return ;
//            failure(error);
        }
        
        
    }];

}

+ (void)uploadFilesWithSubpath:(NSString *)subpath parameters:(NSDictionary *)parameters fileArray:(NSArray *)files success:(HttpSuccess)successMethod failure:(HttpFailure)failure{
    
    NSString *finalURL = [PATH_HeaderURL stringByAppendingString:subpath];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:finalURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (NSDictionary *item in files) {
            
            NSData *imageData = item[@"image"];
            NSString *fileName = item[@"fileKey"];
            NSString *name = item[@"nameKey"];
            NSString *mimeType = item[@"mimeType"];
            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:mimeType];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = responseObject;
        if (dict.count==0) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"未连接到服务器"];
        }else{
            successMethod(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
        if (error) {
            ZHRequestData *data = [[ZHRequestData alloc]init];
            [data showHint:@"网络连接中断"];
        }
    }];
   
}


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

- (void)showHint:(NSString *)hint
{
    if (hint==nil || [hint isEqualToString:@""]) {
        return;
    }
    //显示提示信息
    UIView *view = [[UIApplication sharedApplication].delegate window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = hint;
    hud.margin = 10.f;
    hud.yOffset = IS_IPHONE_5?200.f:150.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:1.5];
}

@end
