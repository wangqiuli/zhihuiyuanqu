//
//  NSDictionary+JsonData.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/15.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "NSDictionary+JsonData.h"

@implementation NSDictionary (JsonData)

- (NSString *)jsonData{
    NSError *parseError = nil;
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonValue = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonValue;
}

@end
