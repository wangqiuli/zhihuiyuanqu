//
//  NSObject+getUserToken.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/3.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "NSObject+getUserToken.h"

@implementation NSObject (getUserToken)

- (void)getUserToken:(userToken)token{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"token"];
    token(value);
}

@end
