//
//  ZHMyHeaderView.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHUserMessageModel;
@interface ZHMyHeaderView : UIView
// 在这里需要一个属性或者方法, 来接收vc里面传过来的所有的广告的信息
// 并且提供点击的回调
- (void)setMessageData:(NSArray *)messageArray
     clickCallBack:(void (^) (ZHUserMessageModel * model))click;

@end
