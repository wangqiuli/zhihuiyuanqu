//
//  ZHFirstPageFirstPageCommonView.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageFirstPageCommonView.h"

#import "UIImageView+WebCache.h"

#import "ZHFirstPageCommonModel.h"
@interface ZHFirstPageFirstPageCommonView ()



@end
@implementation ZHFirstPageFirstPageCommonView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 需要在initWithFrame里面调用, 能保证在实例化的时候, 肯定有frame
        [self icon];
        [self label];
    }
    return self;
}

- (UIImageView *)icon {
    if (_icon == nil) {
        // 给一个大小即可
        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/6, SCREEN_WIDTH/6)];
        // 通过修改center来改变位置
        // 获取当前视图中心点的坐标
        CGPoint center = CGPointMake(CGRectGetWidth(self.frame)/2.f, CGRectGetHeight(self.frame)/2.f);
        
        // 中心点网上稍微偏移一下
        center.y -= 10;
        _icon.center = center;
        
        // 添加
        [self addSubview:_icon];
    }
    
    return _icon;
}
- (UILabel *)label {
    if (_label == nil) {
        
        // 实例化一个label
        float labelHeight = 18;
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.icon.frame)+5, CGRectGetWidth(self.frame), labelHeight)];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.font = [UIFont systemFontOfSize:15];
        [self addSubview:_label];
    }
    
    return _label;
}
@end
