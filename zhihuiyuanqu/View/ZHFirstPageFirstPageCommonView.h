//
//  ZHFirstPageFirstPageCommonView.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHFirstPageCommonModel;

@interface ZHFirstPageFirstPageCommonView : UIView

/** 需要显示内容的子视图 */
@property (nonatomic, strong) UIImageView *icon;

@property (nonatomic, strong) UILabel *label;

@end
