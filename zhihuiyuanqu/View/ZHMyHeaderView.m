//
//  ZHMyHeaderView.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHMyHeaderView.h"
#import "ZHMessageView.h"
//#import "ZHMyFourView.h"
#import "ZHUserMessageModel.h"

@interface ZHMyHeaderView ()
{
    NSArray *_MessageArray;   // 所有的广告数据
    void (^_clickAction)(ZHUserMessageModel *);    // 点击广告的回调
}
@property(nonatomic,strong)ZHMessageView *messageView;//用户头像信息
//@property(nonatomic,strong)ZHMyFourView *fourView;//四大块

@end
@implementation ZHMyHeaderView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self createMessageView];
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetMaxY(self.messageView.frame));
    }
    return self;
}

#pragma mark messageView
- (void)setMessageData:(NSArray *)messageArray clickCallBack:(void (^)(ZHUserMessageModel *))click{
    // 在添加之前, 先移除原来的内容(因为下拉刷新会重复添加)
    for (UIView *subView in self.messageView.subviews) {
        [subView removeFromSuperview];
    }
    
    _MessageArray = messageArray;
    _clickAction = click;
    [self createMessageView];
}
- (void)createMessageView{
    self.messageView = [[ZHMessageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    self.messageView.model = _MessageArray.firstObject;
    [self.messageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(messageViewTapAction:)]];
    [self addSubview:self.messageView];
}
- (void)messageViewTapAction:(UITapGestureRecognizer *)tap{

    // 2. 根据下标获取模型
    ZHUserMessageModel *model = _MessageArray.firstObject;
    
    // 3. 事件回调
    if (_clickAction) {
        // 回调, 把model回调回去
        _clickAction(model);
    }

}

@end
