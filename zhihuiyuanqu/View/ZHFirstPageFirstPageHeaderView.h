//
//  ZHFirstPageFirstPageHeaderView.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHAdsFirstPageModel;
@class ZHFirstPageCommonModel;

@interface ZHFirstPageFirstPageHeaderView : UIView

// 在这里需要一个属性或者方法, 来接收vc里面传过来的所有的广告的信息
// 并且提供点击的回调
- (void)setAdsData:(NSArray *)allAds
     clickCallBack:(void (^) (ZHAdsFirstPageModel * model))click;

// 在这里, 同样需要一个方法, 用来接收vc里面传过来的所有功能模型的信息
// 并提供点击回调
- (void)setCommonData:(NSArray *)allData
        clickCallBacl:(void (^)(NSInteger integer))click;
@end
