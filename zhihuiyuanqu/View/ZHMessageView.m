//
//  ZHMessageView.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHMessageView.h"
#import "ZHUserMessageModel.h"
#import "UIImageView+WebCache.h"
@interface ZHMessageView ()

@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,strong)UILabel *namelabel;
@property(nonatomic,strong)UILabel *IDLabel;
@property(nonatomic,strong)UILabel *rightLabel;//向右箭头

@end
@implementation ZHMessageView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.icon];
        [self addSubview:self.namelabel];
        [self addSubview:self.IDLabel];
        [self addSubview:self.rightLabel];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setModel:(ZHUserMessageModel *)model{
    _model = model;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"token"];
    //判断账号是否登录
    if (value) {
        self.namelabel.text = model.phone;
        self.IDLabel.text = model.userName;
    }
    else{
        self.namelabel.text = @"手机号";
        self.IDLabel.text = @"用户名";
    }

    if (model.image.length==0||(model.image == nil)) {
        self.icon.image = [UIImage imageNamed:@"1"];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,model.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    
    //获取通知中心单利对象（退出登录通知）
    NSNotificationCenter *center2 = [NSNotificationCenter defaultCenter];
    //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
    [center2 addObserver:self selector:@selector(noticAction) name:@"outLogin" object:nil];
}
- (void)noticAction{
    self.namelabel.text = @"手机号";
    self.IDLabel.text = @"用户名";
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"outLogin" object:nil];
}

#pragma mark 懒加载
- (UIImageView *)icon{
    if (!_icon) {
        _icon = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2.f-150, 50, 100, 100)];
    }
    return _icon;
}
- (UILabel *)namelabel{
    if (_namelabel==nil) {
        _namelabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+10, 60, 120, 30)];
        _namelabel.text = @"手机号";
    }
    return _namelabel;
}
- (UILabel *)IDLabel{
    if (_IDLabel==nil) {
        _IDLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+10, CGRectGetMaxY(self.namelabel.frame)+10, 100, 30)];
        _IDLabel.text = @"用户名";
    }
    return _IDLabel;
}
- (UILabel *)rightLabel{
    if (_rightLabel==nil) {
        _rightLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-70, 75, 50, 50)];
        _rightLabel.text = @">";
        _rightLabel.font = [UIFont systemFontOfSize:40];
    }
    return _rightLabel;
}

@end
