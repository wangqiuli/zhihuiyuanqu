//
//  ZHSetUpViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSetUpViewController.h"
#import "ZHLoginViewController.h"
#import "NSObject+getUserToken.h"

#import "ZHFixPasswordViewController.h"
#import "ZHMessageNotificationViewController.h"

@interface ZHSetUpViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSArray *dataDSource;
@end

@implementation ZHSetUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)setUpViewHeight:(CGFloat)height{
    [self.view addSubview:self.tableView];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataDSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (indexPath.row!=self.dataDSource.count-1) {
        cell.textLabel.text = self.dataDSource[indexPath.row];
        
    }else{
        
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.text = self.dataDSource[indexPath.row];
        cell.textLabel.layer.borderColor = [UIColor redColor].CGColor;
        cell.textLabel.layer.borderWidth = 1;
        cell.textLabel.layer.cornerRadius = 20;
        cell.textLabel.layer.masksToBounds = YES;
    }
    cell.selectionStyle = NO;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    switch (row) {
        case 0:{
            ZHMessageNotificationViewController *notifi = [[ZHMessageNotificationViewController alloc]init];
            notifi.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:notifi animated:YES];
            break;
        }
        case 1:{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *token = [defaults objectForKey:@"token"];
            if (token == nil) {
                [self showHint:@"请先登录"];
            }else{
                UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];
                ZHFixPasswordViewController *newPassw = [story instantiateViewControllerWithIdentifier:@"ZHFixPasswordViewController"];
                [self.navigationController pushViewController:newPassw animated:YES];
            }
            break;
        }
        case 2:{
            
            break;
        }
        case 3:{
            
            break;
        }
        case 4:{
            
            break;
        }
        case 5:{
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *value = [defaults objectForKey:@"token"];
            if (value.length>0) {
                [self showAlert:@"提示" message:@"你已经登录，确定要退出吗" click:^(BOOL ret) {
                    if (ret) {
                        
                        [defaults setObject:@"" forKey:@""];
                        
                        NSDictionary *dict = [defaults dictionaryRepresentation];
                        for (id key in dict) {
                            [defaults removeObjectForKey:key];
                        }
                        [defaults synchronize];
                        
                        NSNotification *notic = [NSNotification notificationWithName:@"outLogin" object:nil];
                        //发送消息
                        [[NSNotificationCenter defaultCenter]postNotification:notic];
                        
                        //退出环信登录
                        EMError *error = [[EMClient sharedClient] logout:YES];
                        if (!error) {
                            NSLog(@"退出环信成功");
                        }
                    }
                }];
                
            }else{
            
            UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];
            ZHBaseNavViewController *baseNav = [story instantiateViewControllerWithIdentifier:@"LoginAndRegist"];
            
            [self.navigationController presentViewController:baseNav animated:YES completion:nil];
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}
- (NSArray *)dataDSource{
    if (_dataDSource==nil) {
        _dataDSource = @[@"消息通知",@"修改密码",@"清除缓存",@"意见反馈",@"关于HOME+",@"退出登录"];
    }
    return _dataDSource;
}
@end
