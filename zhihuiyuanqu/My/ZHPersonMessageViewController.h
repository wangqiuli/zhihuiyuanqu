//
//  ZHPersonMessageViewController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseViewController.h"

@class ZHUserMessageModel;
@interface ZHPersonMessageViewController : ZHBaseViewController

@property(nonatomic,copy)void(^diaryBlock)(NSString *);
@end
