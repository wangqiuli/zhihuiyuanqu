//
//  ZHFixPasswordViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/31.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFixPasswordViewController.h"
#import "NSObject+getUserToken.h"
#import "NSString+ZHMd5.h"
#import "NSObject+Check.h"
#import <SMS_SDK/SMSSDK.h>
@interface ZHFixPasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;//手机号码
@property (strong, nonatomic) IBOutlet UITextField *verificationTextField;//验证码
@property (weak, nonatomic) IBOutlet UIButton *verificationButton;//短信验证按钮
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;//旧密码
@property (weak, nonatomic) IBOutlet UITextField *newsPasswordTextField;//新密码
@end

@implementation ZHFixPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"修改密码";
}

#pragma mark 获取验证码按钮
- (IBAction)clickVerification:(id)sender {
    //1.判断手机号格式是否正确
    if (![self validPhoneNumber:self.phoneTextField.text]) {
        [self showHint:@"手机格式不正确!"];
    }else{
        
        //2.判断手机号是否已经注册
        [ZHRequestData serviceUploadFilesWithSubpath:PATH_TelNumTestURL parameters:@{@"telNum":self.phoneTextField.text} success:^(id json) {
            
            BOOL isTest = [json[@"returning"] boolValue];
            
            if (!isTest) {
                [self SendVerificationCode:self.phoneTextField.text clickButton:self.verificationButton clickBlock:^(NSError *isError) {
                    if (isError) {
                        NSLog(@"短信验证错误信息:%@",isError);
                    }else{
                        NSLog(@"获取短信验证码成功");
                    }
                }];
                
            }else{
                [self showHint:@"该手机号未注册"];
                
            }
        } failure:^(NSError *error) {
            NSLog(@"检测手机号是否注册错误:%@",error);
        }];
        
    }
}

#pragma mark 保存按钮
- (IBAction)saveAction:(id)sender {
    
    //判断验证码是否正确
    [SMSSDK commitVerificationCode:self.verificationTextField.text phoneNumber:self.phoneTextField.text zone:@"86" result:^(NSError *error) {
        if (error) {
            NSLog(@"%@",error);
            [self showHint:@"验证码错误"];
        }else{
            
            if (self.oldPasswordTextField.text.length<6&&self.newsPasswordTextField.text.length<6) {
                [self showHint:@"密码的长度不能少于六位"];
            }else{
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *token = [defaults objectForKey:@"token"];
                NSDictionary *parameters = @{
                                             @"token":token,
                                             @"phone":self.phoneTextField.text,
                                             @"oldPwd":[self.oldPasswordTextField.text md5],
                                             @"newPwd":[self.newsPasswordTextField.text md5]
                                             };

                [ZHRequestData serviceUploadFilesWithSubpath:PATH_ChangeUserPasswordURL parameters:parameters success:^(id json) {
                    if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
                        [self showHint:@"密码修改成功"];
                        UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];
                        ZHBaseNavViewController *baseNav = [story instantiateViewControllerWithIdentifier:@"LoginAndRegist"];
                        
                        [self.navigationController presentViewController:baseNav animated:YES completion:nil];
                    }
                    
                } failure:^(NSError *error) {
                    NSLog(@"修改用户密码错误:%@",error);
                }];
                
            }
        }
    }];
}


@end
