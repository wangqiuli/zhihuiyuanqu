//
//  ZHForgetPasswordViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/31.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHForgetPasswordViewController.h"
#import "NSString+ZHMd5.h"
#import "NSObject+Check.h"
#import <SMS_SDK/SMSSDK.h>
#import "ZHNewPasswordViewController.h"
@interface ZHForgetPasswordViewController ()

@property (strong, nonatomic) IBOutlet UILabel *detailLabel;//手机号码
@property (strong, nonatomic) IBOutlet UITextField *verificationTextField;//验证码
@property (weak, nonatomic) IBOutlet UIButton *verificationButton;//短信验证按钮

@end

@implementation ZHForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"忘记密码";
    
    NSString *phone = [self.labelText stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    self.detailLabel.text = [NSString stringWithFormat:@"您的密保手机号%@,点击获取验证码",phone];
}

#pragma mark 获取验证码按钮
- (IBAction)clickVerificationAction:(id)sender {
    [self SendVerificationCode:self.labelText clickButton:self.verificationButton clickBlock:^(NSError *isError) {
        if (isError) {
            NSLog(@"短信验证错误信息:%@",isError);
        }else{
            NSLog(@"获取短信验证码成功");
        }
    }];
}

#pragma mark 保存按钮
- (IBAction)saveAction:(id)sender {
    
    if (self.verificationTextField.text.length==0) {
        [self showHint:@"验证码不能为空"];
        return;
    }
    
    //判断验证码是否正确
    [SMSSDK commitVerificationCode:self.verificationTextField.text phoneNumber:self.labelText zone:@"86" result:^(NSError *error) {
        if (error) {
            [self showHint:@"验证码输入错误"];
            
        }else{
            
            ZHNewPasswordViewController *newPassw = [self.storyboard instantiateViewControllerWithIdentifier:@"ZHNewPasswordViewController"];
            newPassw.labelText = self.labelText;
            [self.navigationController pushViewController:newPassw animated:YES];
        }
    }];
}


@end
