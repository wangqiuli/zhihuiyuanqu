//
//  ZHNewPasswordViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/23.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNewPasswordViewController.h"
#import "NSString+ZHMd5.h"
#import "NSObject+Check.h"
#import <SMS_SDK/SMSSDK.h>
@interface ZHNewPasswordViewController ()

@property (strong, nonatomic) IBOutlet UITextField *firstPassword;//设置密码
@property (strong, nonatomic) IBOutlet UITextField *secondPassword;//重复密码

@end

@implementation ZHNewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"忘记密码";
}
- (IBAction)sureButtonAction:(id)sender {
    if (self.firstPassword.text.length<6) {
        [self showHint:@"密码的长度不能少于六位"];
    }else if(![self.secondPassword.text isEqualToString:self.firstPassword.text]){
        [self showHint:@"两次密码输入不一致"];
    }else{
        NSDictionary *parameters = @{@"phone":self.labelText,
                                     @"newPwd":[self.firstPassword.text md5]};
        [ZHRequestData serviceUploadFilesWithSubpath:PATH_changeUserPasswordNoLoginURL parameters:parameters success:^(id json) {
            [self showHint:@"密码修改成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            } failure:^(NSError *error) {
                NSLog(@"修改用户密码错误:%@",error);
            }];
    }
}


@end
