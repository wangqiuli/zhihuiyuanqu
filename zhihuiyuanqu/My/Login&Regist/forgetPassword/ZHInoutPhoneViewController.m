//
//  ZHInoutPhoneViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/23.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHInoutPhoneViewController.h"
#import <SMS_SDK/SMSSDK.h>
#import "NSObject+Check.h"
#import "ZHForgetPasswordViewController.h"
@interface ZHInoutPhoneViewController ()


@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;

@end

@implementation ZHInoutPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"忘记密码";
}
- (IBAction)nextButton:(id)sender {
    
    if (self.phoneTextField.text.length==0) {
        [self showHint:@"手机号码不能为空"];
    }else if (![self validPhoneNumber:self.phoneTextField.text]) {
        [self showHint:@"手机格式不正确"];
    }else{
        [ZHRequestData serviceUploadFilesWithSubpath:PATH_TelNumTestURL parameters:@{@"telNum":self.phoneTextField.text} success:^(id json) {
            
            BOOL isTest = [json[@"returning"] boolValue];
            
            if (isTest) {
                [self showHint:@"该手机号尚未注册"];
            }else{
                ZHForgetPasswordViewController *forgetPassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ZHForgetPasswordViewController"];
                forgetPassword.labelText = self.phoneTextField.text;
                [self.navigationController pushViewController:forgetPassword animated:YES];
            }
        } failure:^(NSError *error) {
            NSLog(@"检测手机号是否注册错误:%@",error);
        }];
    }
    
}


@end
