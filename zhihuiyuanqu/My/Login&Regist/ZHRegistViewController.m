//
//  ZHRegistViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHRegistViewController.h"
#import <SMS_SDK/SMSSDK.h>
#import "NSString+ZHMd5.h"
#import "NSObject+Check.h"
@interface ZHRegistViewController ()
{
    NSInteger timeCount;
}
@property (strong, nonatomic) IBOutlet UITextField *userNumTextField;//用户名
@property (strong, nonatomic) IBOutlet UITextField *verificationTextField;//验证码
@property (strong, nonatomic) IBOutlet UITextField *passWTextField;//登录密码
@property (strong, nonatomic) IBOutlet UITextField *secondPassWTextField;//重复密码
@property (strong, nonatomic) IBOutlet UIButton *verificationButton;//短信验证按钮
@property (strong, nonatomic) IBOutlet UIButton *agreeBookButton;//同意按钮
@property(nonatomic,strong)NSTimer *timer;

@end

@implementation ZHRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"注册";
    
}

- (IBAction)registButtonAction:(id)sender {

    //判断验证码是否正确
    [SMSSDK commitVerificationCode:self.verificationTextField.text phoneNumber:self.userNumTextField.text zone:@"86" result:^(NSError *error) {
        if (error) {
            [self showHint:@"验证码错误"];
        }else{
            
            if (self.passWTextField.text.length<6) {
                [self showHint:@"密码的长度不能少于六位"];
            }else{
                if (self.agreeBookButton.selected==NO) {
                    [self showHint:@"请同意《Home网协议书》"];
                    return ;
                }
            
            NSDictionary *parameters = @{@"telNum":self.userNumTextField.text,@"password":[self.passWTextField.text md5]};
            [ZHRequestData serviceUploadFilesWithSubpath:PATH_RegistURL parameters:parameters success:^(id json) {
                if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
                    [self showHint:@"注册成功"];
                    if (self.registBlock) {
                        self.registBlock(self.userNumTextField.text);
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }failure:^(NSError *error) {
                NSLog(@"注册账户错误:%@",error);
            }];
            
            }
        }
    }];
    
}

//点击获取验证码
- (IBAction)verificationButtonAction:(UIButton *)sender {
    if (![self validPhoneNumber:self.userNumTextField.text]) {
        [self showHint:@"手机格式不正确!"];
    }else{
        [ZHRequestData serviceUploadFilesWithSubpath:PATH_TelNumTestURL parameters:@{@"telNum":self.userNumTextField.text} success:^(id json) {
            
            BOOL isTest = [json[@"returning"] boolValue];
            
            if (!isTest) {
                [self showHint:@"该手机号已经注册"];
            }else{
                [self SendVerificationCode:self.userNumTextField.text clickButton:self.verificationButton clickBlock:^(NSError *isError) {
                    if (isError) {
                        NSLog(@"短信验证错误信息:%@",isError);
                    }else{
                        NSLog(@"获取短信验证码成功");
                    }
                }];
            }
        } failure:^(NSError *error) {
            NSLog(@"检测手机号是否注册错误:%@",error);
        }];
    }
}

//是否同意协议书
- (IBAction)agreeBookAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
}

@end
