//
//  ZHLoginViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLoginViewController.h"
#import "ZHRegistViewController.h"
#import "NSString+ZHMd5.h"
#import "ZHForgetPasswordViewController.h"
@interface ZHLoginViewController ()
@property (strong, nonatomic) IBOutlet UITextField *userNumTextField;//用户名
@property (strong, nonatomic) IBOutlet UITextField *passWordTextField;//密码

@end

@implementation ZHLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}

#pragma mark 登录按钮
- (IBAction)loginButtonAction:(id)sender {
    NSDictionary *parameters = @{@"userName":self.userNumTextField.text,@"password":[self.passWordTextField.text md5]};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_LoginURL parameters:parameters success:^(id json) {
        if ([json[@"message"] isEqualToString:@"手机号码和密码不匹配"]) {
            [self showHint:@"手机号码和密码不匹配"];
        }else {
            
            //保存用户信息
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:json[@"returning"] forKey:@"token"];
            [defaults synchronize];
            
//            //环信登录（判断之前有没有登录过）
            BOOL isAutoLogin = [EMClient sharedClient].options.isAutoLogin;
            if (!isAutoLogin) {
                EMError *error = [[EMClient sharedClient] loginWithUsername:self.userNumTextField.text password:[self.userNumTextField.text md5]];
                if (!error) {
                    NSLog(@"环信登录成功");
                    [[EMClient sharedClient].options setIsAutoLogin:YES];
                    //获取数据库中的数据
                    [[EMClient sharedClient].chatManager loadAllConversationsFromDB];
                }else{
                    NSLog(@"环信登录错误%@",error);
                }
            }
            
            
            
            //告诉我界面更新用户信息
            //创建一个消息对象
            NSNotification *notic = [NSNotification notificationWithName:@"updateUserMessage" object:nil];
            //发送消息
            [[NSNotificationCenter defaultCenter]postNotification:notic];
            [self showHint:@"登录成功"];
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }

    } failure:^(NSError *error) {
        NSLog(@"登录错误:%@",error);
    }];
    
}

@end
