//
//  ZHSetUpViewController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseViewController.h"

@interface ZHSetUpViewController : ZHBaseViewController
- (void)setUpViewHeight:(CGFloat)height;
@end
