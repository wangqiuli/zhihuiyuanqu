//
//  ZHTransferViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHTransferViewController.h"
#import "UMSocial.h"
#import "ZHDiscountDetailTableViewCell.h"
#import "ZHGetReallyDiscountModel.h"
@interface ZHTransferViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,strong)UIButton *shareButton;//分享按钮
@end

@implementation ZHTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self requestData];
    [self setUpNavigationItem];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.shareButton];
    [self.view bringSubviewToFront:self.shareButton];
}

- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetDiscountURL parameters:@{@"token":token,@"id":[NSString stringWithFormat:@"%zd",self.id]} success:^(id json) {
        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"折扣中获取折扣数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{

    ZHGetReallyDiscountModel *model = [[ZHGetReallyDiscountModel alloc]initWithDictionary:data[@"returning"] error:nil];
    [self.dataSource addObject:model];
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    self.tableView.mj_header = header;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{

    self.title = @"折扣详情";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"收藏" style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
}
- (void)rightItemAction{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    ZHGetReallyDiscountModel *model = self.dataSource.firstObject;
    NSDictionary *dict = @{@"parentId":[NSString stringWithFormat:@"%zd",model.id],@"type":@"1"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SaveURL parameters:@{@"token":token,@"jsonStr":[dict jsonData]} success:^(id json) {
        if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
            [self showHint:@"收藏成功"];
        }
    } failure:^(NSError *error) {
        NSLog(@"折扣收藏数据失败:%@",error);
    }];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHDiscountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHDiscountDetailTableViewCell"];
    cell.discountModel = self.dataSource.firstObject;
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHDiscountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHDiscountDetailTableViewCell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHDiscountDetailTableViewCell" owner:self options:nil].lastObject;
    }
    cell.discountModel = self.dataSource.firstObject;
    return cell;
}
#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-80) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addRefreshControl];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHDiscountDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZHDiscountDetailTableViewCell"];
        
    }
    return _tableView;
}
#pragma mark 分享按钮
- (UIButton *)shareButton{
    if (_shareButton==nil) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(0, SCREEN_HEIGHT-40-64, SCREEN_WIDTH, 40);
        _shareButton.backgroundColor = [UIColor lightGrayColor];
        [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(shareButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}
- (void)shareButtonAction{
    //分享的地方
    NSArray *plateforms = @[UMShareToQQ,UMShareToQzone,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina];
    
    [UMSocialSnsService presentSnsIconSheetView:self appKey:@"56c13b9be0f55a16dd00207b" shareText:@"风景" shareImage:[UIImage imageNamed:@"background"] shareToSnsNames:plateforms delegate:nil];
}

- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.tabBarController.tabBar.hidden = NO;
}

@end
