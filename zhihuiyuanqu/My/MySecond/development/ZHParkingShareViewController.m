//
//  ZHParkingShareViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHParkingShareViewController.h"
#import "ZHNeighborJoinTableViewCell.h"
#import "ZHDevelopmentModel.h"
#import "ZHGetActiveModel.h"
//#import "ZHUser.h"
@interface ZHParkingShareViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,strong)NSMutableArray *userDataSource;//存储用户列表
@property(nonatomic,strong)UIButton *joinButton;

@end

@implementation ZHParkingShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self requestData];
    [self setNavgationItem];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.joinButton];
    [self.view bringSubviewToFront:self.joinButton];
}

- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetActiveURL parameters:@{@"token":token,@"id":[NSString stringWithFormat:@"%zd",self.id]} success:^(id json) {
        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        [self tableViewData:json];
        [self requestUserWithActiveData];
    } failure:^(NSError *error) {
        NSLog(@"活动中获取折扣数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{

    ZHGetActiveModel *model = [[ZHGetActiveModel alloc]initWithDictionary:data[@"returning"] error:nil];
    [self.dataSource addObject:model];
}
#pragma mark 请求参加人员的数据
- (void)requestUserWithActiveData{

    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetUserWithActiveURL parameters:@{@"activityId":[NSString stringWithFormat:@"%zd",self.id]} success:^(id json) {
        [self getUserWithActiveData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"活动获取人员列表错误:%@",error);
        [self stopRefresh];
    }];
    
}
- (void)getUserWithActiveData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    self.userDataSource = [NSMutableArray arrayWithArray:allData];
    [self.tableView reloadData];
}


#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    self.tableView.mj_header = header;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
}

#pragma mark 导航栏设置
- (void)setNavgationItem{
    self.title = @"活动详情";
    self.tabBarController.tabBar.hidden = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"收藏" style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
}
- (void)rightItemAction{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    ZHGetActiveModel *model = self.dataSource.firstObject;
    NSDictionary *dict = @{@"parentId":[NSString stringWithFormat:@"%zd",model.activity.id],@"type":@"3"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SaveURL parameters:@{@"token":token,@"jsonStr":[dict jsonData]} success:^(id json) {
        if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
            [self showHint:@"收藏成功"];
        }
    } failure:^(NSError *error) {
        NSLog(@"活动收藏数据失败:%@",error);
    }];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHNeighborJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.backgroundColor = UICOLOR_RGB;
    [cell getWithActivitysModel:self.dataSource.firstObject withUserArray:self.userDataSource];
    cell.selectionStyle = NO;
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHNeighborJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    [cell getWithActivitysModel:self.dataSource.firstObject withUserArray:self.userDataSource];
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-104) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[ZHNeighborJoinTableViewCell class] forCellReuseIdentifier:@"cell"];
        [self addRefreshControl];
        
    }
    return _tableView;
}
#pragma mark 我要参加按钮
- (UIButton *)joinButton{
    if (_joinButton==nil) {
        _joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _joinButton.frame = CGRectMake(0, SCREEN_HEIGHT-104, SCREEN_WIDTH, 40);
        _joinButton.backgroundColor = [UIColor lightGrayColor];
        [_joinButton setTitle:@"我要参加" forState:UIControlStateNormal];
        [_joinButton addTarget:self action:@selector(joinButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _joinButton;
}
- (void)joinButtonAction{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"userId"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_wantJoinActiveURL parameters:@{@"activityId":[NSString stringWithFormat:@"%zd",self.id],@"userId":userId} success:^(id json) {
        BOOL ret = [json[@"returning"] boolValue];
        if (ret==NO ) {
            [self showHint:@"您已报过名!"];
        }else{
            [self showHint:@"参加活动成功!"];
        }

    } failure:^(NSError *error) {
        NSLog(@"我要参加活动数据失败:%@",error);
    }];
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.tabBarController.tabBar.hidden = NO;
}
@end
