//
//  ZHGetCarViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHGetCarViewController.h"
#import <MAMapKit/MAMapKit.h>
#import "ZHSelfCarDetailTableViewCell.h"
#import "ZHUser.h"
#import "ZHGetCarModel.h"
#import "ZHDevelopmentModel.h"
#import "EaseMessageViewController.h"
#import "ChatViewController.h"
@interface ZHGetCarViewController ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate>
/** 界面展示 */
@property (nonatomic, strong) UITableView *tableView;

/** 全局数组, 用来存放分组的数据 */
@property (nonatomic, strong) NSMutableArray *dataSource;

@property(nonatomic,strong)UIView *headerView;//头视图

@property(nonatomic,strong)UIImageView *icon;//头像

@property(nonatomic,strong)UILabel *nameLabel;//联系人

@property(nonatomic,strong)MAMapView *mapView;//地图

@property(nonatomic,strong)UIButton *phoneButton;//电话联系
@property(nonatomic,strong)UIButton *chatButton;//聊天

@end

@implementation ZHGetCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //配置用户Key
    [MAMapServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";
    
    [self requestData];
    [self setUpNavigationItem];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.phoneButton];
    [self.view addSubview:self.chatButton];
    [self.view bringSubviewToFront:self.phoneButton];
    [self.view bringSubviewToFront:self.chatButton];
}

- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_FindCarByURL parameters:@{@"token":token,@"id":[NSString stringWithFormat:@"%zd",self.id]} success:^(id json) {
        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"车位共享中获取折扣数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{
    
    ZHGetCarModel *model = [[ZHGetCarModel alloc]initWithDictionary:data[@"returning"] error:nil];
    [self.dataSource addObject:model];
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    self.tableView.mj_header = header;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
}

#pragma mark 电话，聊天按钮
- (UIButton *)phoneButton{
    if (_phoneButton==nil) {
        _phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _phoneButton.frame = CGRectMake(0, SCREEN_HEIGHT-108, SCREEN_WIDTH/2.f, 44);
        _phoneButton.layer.borderWidth = 1;
        _phoneButton.backgroundColor = [UIColor lightGrayColor];
        [_phoneButton setTitle:@"电话联系" forState:UIControlStateNormal];
        [_phoneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_phoneButton addTarget:self action:@selector(phoneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _phoneButton;
}
- (UIButton *)chatButton{
    if (_chatButton==nil) {
        _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatButton.frame = CGRectMake(SCREEN_WIDTH/2.f, SCREEN_HEIGHT-108, SCREEN_WIDTH/2.f, 44);
        _chatButton.layer.borderWidth = 1;
        _chatButton.backgroundColor = [UIColor lightGrayColor];
        [_chatButton setTitle:@"聊天" forState:UIControlStateNormal];
        [_chatButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_chatButton addTarget:self action:@selector(chatButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatButton;
}
- (void)phoneButtonAction:(UIButton *)button{
    ZHGetCarModel *model = self.dataSource.firstObject;
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",model.parkingshare.phone]]];
}

- (void)chatButtonAction:(UIButton *)button{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"userId"];
    ZHGetCarModel *model = self.dataSource.firstObject;
    
    
    if (model.user.id == [userId integerValue]) {
        [self showHint:@"不能和自己聊天"];
    }else{
        //创建聊天会话、传递用户或群 ID 和会话类型（EMConversationType）。
        ChatViewController *chatController = [[ChatViewController alloc]initWithConversationChatter:model.user.phone conversationType:EMConversationTypeChat];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:model.user.userName forKey:[NSString stringWithFormat:@"userId%@",model.user.phone]];
        [defaults synchronize];
        chatController.hidesBottomBarWhenPushed = YES;
        chatController.title = model.user.userName;
        [self.navigationController pushViewController:chatController animated:YES];
    }
    
}

#pragma mark 高德地图
- (MAMapView *)mapView{
    if (_mapView==nil) {
        _mapView = [[MAMapView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.icon.frame)+10, SCREEN_WIDTH, SCREEN_WIDTH)];
        _mapView.delegate = self;
        
        _mapView.showsUserLocation = YES;//YES 为打开定位，NO为关闭定位
        [_mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];//地图跟着位置移动
    }
    return _mapView;
}
//当位置更新时，会进定位回调，通过回调函数，能获取到定位点的经纬度坐标，
- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
    
}


#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSelfCarDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHGetCarModel *model = self.dataSource.firstObject;
    cell.disModel = model;
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,model.user.image];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.nameLabel.text = model.user.userName;
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSelfCarDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHSelfCarDetailTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHGetCarModel *model = self.dataSource.firstObject;
    cell.disModel = model;
    return cell;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-114) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addRefreshControl];
        //滑动tableView收缩键盘
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _tableView.tableHeaderView = self.headerView;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHSelfCarDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
        
    }
    return _dataSource;
}

#pragma mark 头视图
- (UIView *)headerView{
    if (_headerView==nil) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH+80)];
        
        [_headerView addSubview:self.icon];
        [_headerView addSubview:self.nameLabel];
        [_headerView addSubview:self.mapView];
    }
    return _headerView;
}
- (UIImageView *)icon{
    if (_icon==nil) {
        _icon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 60)];
        
    }
    return _icon;
}
- (UILabel *)nameLabel{
    if (_nameLabel ==nil) {
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame)+10, 10, SCREEN_WIDTH-CGRectGetMaxX(self.icon.frame)-20, 30)];
        
    }
    return _nameLabel;
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.tabBarController.tabBar.hidden = YES;
    self.title = @"车位详情";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"收藏" style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
    
}
- (void)rightItemAction{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    ZHGetCarModel *model = self.dataSource.firstObject;
    NSDictionary *dict = @{@"parentId":[NSString stringWithFormat:@"%zd",model.parkingshare.id],@"type":@"2"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SaveURL parameters:@{@"token":token,@"jsonStr":[dict jsonData]} success:^(id json) {
        if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
            [self showHint:@"收藏成功"];
        }
    } failure:^(NSError *error) {
        NSLog(@"车位共享收藏数据失败:%@",error);
    }];
}

@end
