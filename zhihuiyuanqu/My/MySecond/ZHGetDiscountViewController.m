//
//  ZHGetDiscountViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHGetDiscountViewController.h"
#import "ZHDevelopmentModel.h"
#import "ZHSellDetailTableViewCell.h"
#import "ZHGetDiscountModel.h"
#import "ZHUser.h"
#import "ChatViewController.h"
@interface ZHGetDiscountViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@property(nonatomic,strong)UIButton *phoneButton;//电话联系
@property(nonatomic,strong)UIButton *chatButton;//聊天

@property(nonatomic,strong)NSString *tel;//电话号码

@end

@implementation ZHGetDiscountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self requestData];
    [self setUpNavigationItem];
    [self.view addSubview:self.phoneButton];
    [self.view addSubview:self.chatButton];
    [self.view addSubview:self.tableView];
    [self.view bringSubviewToFront:self.phoneButton];
    [self.view bringSubviewToFront:self.chatButton];
}

- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_FindByIdURL parameters:@{@"token":token,@"id":[NSString stringWithFormat:@"%zd",self.id]} success:^(id json) {
        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"二手置换中获取折扣数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{

    ZHGetDiscountModel *model = [[ZHGetDiscountModel alloc]initWithDictionary:data[@"returning"] error:nil];
    [self.dataSource addObject:model];
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];
    }];
    self.tableView.mj_header = header;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
}

#pragma mark tabieView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHSellDetailTableViewCell" owner:self options:nil].lastObject;
    }
    ZHGetDiscountModel *model = self.dataSource.firstObject;
    cell.disModel = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHGetDiscountModel *model = self.dataSource.firstObject;
    cell.disModel = model;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addRefreshControl];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHSellDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
        
    }
    return _dataSource;
}

#pragma mark 电话，聊天按钮
- (UIButton *)phoneButton{
    if (_phoneButton==nil) {
        _phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _phoneButton.frame = CGRectMake(0, SCREEN_HEIGHT-104, SCREEN_WIDTH/2.f, 40);
        _phoneButton.layer.borderWidth = 1;
        _phoneButton.backgroundColor = [UIColor whiteColor];
        [_phoneButton setTitle:@"电话联系" forState:UIControlStateNormal];
        [_phoneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_phoneButton addTarget:self action:@selector(phoneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _phoneButton;
}
- (UIButton *)chatButton{
    if (_chatButton==nil) {
        _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatButton.frame = CGRectMake(SCREEN_WIDTH/2.f, SCREEN_HEIGHT-104, SCREEN_WIDTH/2.f, 40);
        _chatButton.layer.borderWidth = 1;
        _chatButton.backgroundColor = [UIColor whiteColor];
        [_chatButton setTitle:@"聊天" forState:UIControlStateNormal];
        [_chatButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_chatButton addTarget:self action:@selector(chatButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatButton;
}
- (void)phoneButtonAction:(UIButton *)button{
    
    ZHGetDiscountModel *model = self.dataSource.firstObject;
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",model.transfer.phone]]];
    
}
- (void)chatButtonAction:(UIButton *)button{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"userId"];
    ZHGetDiscountModel *model = self.dataSource.firstObject;
    
    if (model.user.id == [userId integerValue]) {
        [self showHint:@"不能和自己聊天"];
    }else{
        //创建聊天会话、传递用户或群 ID 和会话类型（EMConversationType）。
        ChatViewController *chatController = [[ChatViewController alloc]initWithConversationChatter:model.user.phone conversationType:EMConversationTypeChat];
        chatController.hidesBottomBarWhenPushed = YES;
        chatController.title = model.user.userName;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:model.user.userName forKey:[NSString stringWithFormat:@"userId%@",model.user.phone]];
        [defaults synchronize];
        [self.navigationController pushViewController:chatController animated:YES];
    }
    
    
    
    
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"二手详情";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"收藏" style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
    
}
- (void)rightItemAction{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    ZHGetDiscountModel *model = self.dataSource.firstObject;
    NSDictionary *dict = @{@"parentId":[NSString stringWithFormat:@"%zd",model.transfer.id],@"type":@"5"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SaveURL parameters:@{@"token":token,@"jsonStr":[dict jsonData]} success:^(id json) {
        if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
            [self showHint:@"收藏成功"];
        }
    } failure:^(NSError *error) {
        NSLog(@"二手置换收藏数据失败:%@",error);
    }];
}
- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
}

@end
