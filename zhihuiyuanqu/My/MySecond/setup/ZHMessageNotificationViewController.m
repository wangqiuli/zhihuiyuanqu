//
//  ZHMessageNotificationViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHMessageNotificationViewController.h"
#import "ZHMessageNotificationTableViewCell.h"
@interface ZHMessageNotificationViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSArray *dataSource;
@end

@implementation ZHMessageNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataSource = @[@"声音提醒",@"推送通知",@"允许定位",@"震动"];
    [self setUpNavicationItem];
    [self.view addSubview:self.tableView];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHMessageNotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    if (cell==nil) {
//        cell = []
//    }
    cell.titleStr = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHMessageNotificationTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

#pragma mark 设置导航栏
- (void)setUpNavicationItem{
    self.title = @"消息通知";
}

@end
