//
//  ZHDevelopmentViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHDevelopmentViewController.h"
#import "ZHFirstPageTableViewCell.h"
#import "ZHDevelopmentModel.h"
#import "ZHMyViewController.h"
#import "ZHBaseNavViewController.h"

#import "ZHGetDiscountViewController.h"
#import "ZHGetCarViewController.h"
#import "ZHTransferViewController.h"
#import "ZHParkingShareViewController.h"

@interface ZHDevelopmentViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
}
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHDevelopmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lastId = 0;
    ret = YES;
    [self requestData];
}

- (void)setUpViewHeight:(CGFloat)height{
    [self.view addSubview:self.tableView];
    self.tableView.frame = CGRectMake(10, 0, SCREEN_WIDTH-20, height);
}

#pragma mark 请求数据
- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    if (token==nil) {
        [self showHint:@"请先登录"];
        return;
    }
    if (ret == YES) {
        lastId2 = @"0";
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"lastId":lastId2,
                                 };
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetUserReleaseURL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"获取用户发布的数据失败:%@",error);
        [self stopRefresh];
    }];

}
- (void)tableViewData:(NSDictionary *)data{

    NSArray *allData = data[@"returning"];

    for (NSDictionary *info in allData) {

        ZHDevelopmentModel *model = [[ZHDevelopmentModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{

    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId +10;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{

    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark tableView的协议方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.modelDev = self.dataSource[indexPath.section];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    ZHDevelopmentModel *model = self.dataSource[indexPath.section];
    NSArray *array = @[@"parkingshare",@"activity",@"transfer",@"discount"];
    if ([model.fromtable isEqualToString:array[0]]) {//车位共享
        ZHGetCarViewController *car = [[ZHGetCarViewController alloc]init];
        car.id = model.id;
        [self.navigationController pushViewController:car animated:YES];
    }else if ([model.fromtable isEqualToString:array[1]]){//活动
        ZHParkingShareViewController *parking = [[ZHParkingShareViewController alloc]init];
        parking.id = model.id;
        [self.navigationController pushViewController:parking animated:YES];
    }else if ([model.fromtable isEqualToString:array[2]]){//二手置换
        ZHGetDiscountViewController *detail = [[ZHGetDiscountViewController alloc]init];
        detail.id = model.id;
        [self.navigationController pushViewController:detail animated:YES];
    }else if ([model.fromtable isEqualToString:array[3]]){//折扣
        ZHTransferViewController *transfer = [[ZHTransferViewController alloc]init];
        
        transfer.id = model.id;
        [self.navigationController pushViewController:transfer animated:YES];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.modelDev = self.dataSource[indexPath.section];
    return cell.maxY;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {

        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, self.view.bounds.size.height-40-64) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;

        // 刷新集成
        [self addRefreshControl];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHFirstPageTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
@end
