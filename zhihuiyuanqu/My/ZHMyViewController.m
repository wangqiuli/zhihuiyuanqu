//
//  ZHMyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHMyViewController.h"
#import "ZHFirstPageTableViewCell.h"

#import "ZHMyHeaderView.h"
#import "ZHPersonMessageViewController.h"
#import "ZHDevelopmentViewController.h"
#import "ZHSaveViewController.h"
#import "ZHSetUpViewController.h"
#import "ConversationListController.h"
#import "ZHUserMessageModel.h"
#import "TitleLebal.h"
@interface ZHMyViewController ()<UIScrollViewDelegate>

@property(nonatomic,strong)ZHMyHeaderView *headerView;
@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,strong)UIScrollView *titleScrollView;
@property(nonatomic,strong)UIScrollView *contentScrollView;
@property(nonatomic,strong)NSArray *titles;
@property (nonatomic, strong) UIView *topBackView;
@end

@implementation ZHMyViewController

#define number 4 //头上有多少label

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.headerView];
    [self requestData];//请求数据
    [self setUpNSNotification];
    
    [self initScorllView];
    [self addContentChildViewController];
    [self scrollViewDidEndScrollingAnimation:self.contentScrollView];
    [self scrollViewDidScroll:self.contentScrollView];
}

#pragma mark 通知
- (void)setUpNSNotification{
    //获取通知中心单利对象（登录通知）
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
    [center1 addObserver:self selector:@selector(noticAction) name:@"updateUserMessage" object:nil];
}
- (void)noticAction{
    [self requestData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateUserMessage" object:nil];
}
#pragma mark 请求数据
- (void)requestData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"token"];
    if (value==nil) {
        [self showHint:@"请先登录"];
        return;
    }
    NSLog(@"用户Token：%@",value);
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_UserMessage parameters:@{@"token":value} success:^(id json) {
        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        
        [self tableViewData:json];
    } failure:^(NSError *error) {
        NSLog(@"获取用户信息错误:%@",error);
    }];
}
- (void)tableViewData:(NSDictionary *)data{
    
    NSDictionary *allData = data[@"returning"];
    
    //保存用户信息(头像，昵称，手机号，id)
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:allData[@"id"] forKey:@"userId"];
    [defaults setObject:allData[@"image"] forKey:@"image"];
    [defaults setObject:allData[@"userName"] forKey:@"userName"];
    [defaults setObject:allData[@"phone"] forKey:@"phone"];
    NSLog(@"用户ID:%@",[defaults objectForKey:@"userId"]);
    [defaults synchronize];
    
    
    ZHUserMessageModel *model = [[ZHUserMessageModel alloc]initWithDictionary:allData error:nil];
    [self.dataSource addObject:model];

    __weak typeof(self) weakSelf = self;
    [self.headerView setMessageData:self.dataSource clickCallBack:^(ZHUserMessageModel *model) {
        [weakSelf noticAction:model];
    }];
}

#pragma mark 给headerView添加点击事件
- (void)noticAction:(ZHUserMessageModel *)model{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"token"];
    if (value.length>0) {
        ZHPersonMessageViewController *person = [[ZHPersonMessageViewController alloc]init];
        [self.navigationController pushViewController:person animated:YES];
    }else{
        [self showHint:@"请先登录"];
    }

}

#pragma mark ScrollView连动
- (void)addContentChildViewController{
    ZHDevelopmentViewController *develop = [[ZHDevelopmentViewController alloc]init];
    [develop setUpViewHeight:CGRectGetHeight(self.contentScrollView.frame)];
    develop.view.backgroundColor = UICOLOR_RGB;
    [self addChildViewController:develop];
    
    ZHSaveViewController *save = [[ZHSaveViewController alloc]init];
    [save setUpViewHeight:CGRectGetHeight(self.contentScrollView.frame)];
    save.view.backgroundColor = UICOLOR_RGB;
    [self addChildViewController:save];
    
    ConversationListController *mess = [[ConversationListController alloc]init];
    mess.view.backgroundColor = UICOLOR_RGB;
    [self addChildViewController:mess];
    
    ZHSetUpViewController *set = [[ZHSetUpViewController alloc]init];
    [set setUpViewHeight:CGRectGetHeight(self.contentScrollView.frame)];
    set.view.backgroundColor = UICOLOR_RGB;
    [self addChildViewController:set];
    
}

#pragma mark UIScrollViewDelegate
//人为的拖拽 会调用
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

#pragma mark -- 核心代码
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    
    //取出需要显示的控制器
    //拿到索引
    NSInteger  index =  scrollView.contentOffset.x/scrollView.frame.size.width;
    UIViewController *willShow =  self.childViewControllers[index];
    
    willShow.view.frame = CGRectMake(scrollView.contentOffset.x, 0, scrollView.frame.size.width, scrollView.frame.size.height);
    
    [self.contentScrollView addSubview:willShow.view];
}

//实时监控scrollView 的滚动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    CGFloat scale = scrollView.contentOffset.x/scrollView.frame.size.width;
    //获得需要操作的索引
    NSInteger  leftIndex =  scale;
    
    NSInteger  max = self.contentScrollView.contentSize.width/self.contentScrollView.frame.size.width;
    
    if (leftIndex == max -1) return;
    NSInteger rightIndex = leftIndex +1;
    //黑色 0 0 0
    //红色 1 0 0
    //取出lebal
    
    TitleLebal *leftLbl = self.titleScrollView.subviews[leftIndex];
    TitleLebal *rightLbl = self.titleScrollView.subviews[rightIndex];
    
    CGFloat rightScale = scale - leftIndex;
    
    CGFloat leftScale  = 1 - rightScale;
    
    //字体颜色
    //字体大小
    leftLbl.scale = leftScale;
    rightLbl.scale = rightScale;
    
    CGFloat lelW = self.titleScrollView.subviews[0].frame.size.width;
    
    CGFloat jishu = scrollView.contentOffset.x/scrollView.frame.size.width;
    
    CGPoint cent = CGPointMake(lelW/2, _topBackView.center.y);
    cent.x =  cent.x + jishu * lelW < SCREEN_WIDTH/number ? SCREEN_WIDTH/2/number:cent.x + jishu * lelW;
    
    _topBackView.center = cent;
    
}

#pragma mark 懒加载
-(void)initScorllView
{
    _titleScrollView = [[UIScrollView alloc]init];
    _titleScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame), SCREEN_WIDTH, 40);
    _titleScrollView.showsHorizontalScrollIndicator = NO;
    _titleScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
    [self.view addSubview:_titleScrollView];
    
    //初始化内容scrollView
    _contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScrollView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.titleScrollView.frame)-40-64-10)];
    _contentScrollView.backgroundColor = UICOLOR_RGB;
    _contentScrollView.contentSize = CGSizeMake(SCREEN_WIDTH*number, 0);
    _contentScrollView.showsHorizontalScrollIndicator = NO;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    _contentScrollView.pagingEnabled = YES;
    _contentScrollView.delegate = self;
    [self.view addSubview:_contentScrollView];
    
    //添加标题
    [self setupTitle];
    
}

-(void)setupTitle
{
    for (int i = 0; i< number; i++) {
        self.titles = @[@"发布",@"收藏",@"消息",@"设置"];
        TitleLebal *label = [[TitleLebal alloc]init];
        
        label.frame = CGRectMake(SCREEN_WIDTH/number*i, 0, SCREEN_WIDTH/number, 40);
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.titles[i];
        [label addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelCick:)]];
        label.userInteractionEnabled = YES;
        [self.titleScrollView addSubview:label];
    }

    self.topBackView = [[UIView alloc]initWithFrame:CGRectMake(0, 38, SCREEN_WIDTH/number, 2)];
    self.topBackView.backgroundColor = [UIColor redColor];
    [self.titleScrollView addSubview:self.topBackView];
}
-(void)labelCick:(UITapGestureRecognizer *)tap;
{
    //取出当前点击的lebal 的index
    NSInteger index = [tap.view.superview.subviews indexOfObject:tap.view];
    
    //取出当前的偏移量
    CGPoint  offset = self.contentScrollView.contentOffset;
    offset.x = index * SCREEN_WIDTH;
    
    //让内容的scollView 滚动到相应位置
    [self.contentScrollView setContentOffset:offset animated:YES];
    [self scrollViewDidEndScrollingAnimation:self.contentScrollView];
}

- (ZHMyHeaderView *)headerView{
    if (_headerView==nil) {
        _headerView = [[ZHMyHeaderView alloc]init];
        _headerView.backgroundColor = UICOLOR_RGB;
        }
    return _headerView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (void)viewWillAppear:(BOOL)animated{
    if (self.headerView) {
        [self requestData];
    }
}
@end
