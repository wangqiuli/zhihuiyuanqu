//
//  ZHPersonMessageViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.

#import "ZHPersonMessageViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ZHPersonMessageTableViewCell.h"
#import "ZHUserMessageModel.h"

#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAssetRepresentation.h>

@interface ZHPersonMessageViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSArray *titleArray;
@property(nonatomic,strong)NSMutableArray *iconArray;//存储读取的相片
@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,strong)UITextField *textField;//编辑新的数据
@property(nonatomic,strong)NSMutableDictionary *dictData;//存储所有的字典
@end

@implementation ZHPersonMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:self.tableView];
    [self createArray];
    [self requestData];
}

#pragma mark 请求数据
- (void)requestData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"token"];
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_UserMessage parameters:@{@"token":value} success:^(id json) {
        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        [self tableViewData:json];
    } failure:^(NSError *error) {
        NSLog(@"获取用户信息错误:%@",error);
    }];
}
- (void)tableViewData:(NSDictionary *)data{
    
    NSDictionary *allData = data[@"returning"];
    self.dictData = [NSMutableDictionary dictionaryWithDictionary:allData];
    ZHUserMessageModel *model = [[ZHUserMessageModel alloc]initWithDictionary:allData error:nil];
    [self.dataSource addObject:model];
    [self.tableView reloadData];
}

- (void)createArray{
    self.titleArray = @[@"头像",@"用户ID",@"昵称",@"性别",@"手机"];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.titleArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 100;
    }
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    ZHPersonMessageTableViewCell *personCell = [tableView dequeueReusableCellWithIdentifier:@"personCell"];
   
    personCell.selectionStyle = NO;
    cell.selectionStyle = NO;
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = self.titleArray[indexPath.section];
    ZHUserMessageModel *model = self.dataSource.firstObject;
    
    switch (indexPath.section) {
        case 0:{
            personCell.model = model;
            return personCell;
            break;
        }
        case 1:{
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%zd",model.ID];
            return cell;
            break;
        }
        case 2:{
            cell.detailTextLabel.text = model.userName;
            return cell;
            break;
        }
        case 3:{
            if (model.gender==1) {
                cell.detailTextLabel.text = @"男";
            }else{
                cell.detailTextLabel.text = @"女";
            }
            return cell;
            break;
        }
        case 4:{
            cell.detailTextLabel.text = model.phone;
            return cell;
            break;
        }
        default:
            return 0;
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section != 0) {
        if (indexPath.section==4) {
            
            return;
        }
        NSArray *messages = @[@"请输入用户ID",@"请输入用户昵称",@"请输入用户性别"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:messages[indexPath.section-1] preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            self.textField = textField;
        }];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
        
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (self.diaryBlock) {
                self.diaryBlock(self.textField.text);
            }
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];

    }else{
          //读取相册和相机
        [self editImageSelect];
    }
    
    __weak ZHPersonMessageViewController *weakSelf = self;
    self.diaryBlock = ^(NSString *message){
        switch (indexPath.section) {
            case 1:{
                cell.detailTextLabel.text = message;
                [weakSelf.dictData setValue:message forKey:@"id"];
                break;
            }
            case 2:{
                cell.detailTextLabel.text = message;
                [weakSelf.dictData setValue:message forKey:@"userName"];
                break;
            }
            case 3:{
                cell.detailTextLabel.text = message;
                [weakSelf.dictData setValue:message forKey:@"gender"];
                break;
            }
            default:
                break;
        }
        
        [weakSelf changeUserMessage];
        
    };
}
- (void)changeUserMessage{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"token"];
    
    //把字典转换成json字符串
    NSError *parseError = nil;
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:self.dictData options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonValue = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *parameters = @{@"token":value,@"jsonStr":jsonValue};
   
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_changeUserMessageURL parameters:parameters success:^(id json) {
        NSLog(@"修改用户信息：%@",json[@"message"]);
    } failure:^(NSError *error) {
        NSLog(@"修改用户信息错误:%@",error);
    }];
}

#pragma mark 读取相机和相册
- (void)editImageSelect{
    UIAlertController *alertController = [[UIAlertController alloc]init];
    
    UIAlertAction *action0 = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = YES;
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有摄像头"];
        }
        
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.allowsEditing = YES;
            controller.delegate = self;
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showAlert:@"提示" message:@"你没有相册" click:^(BOOL ret) {
            }];
        }
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:action0];
    [alertController addAction:action1];
    [alertController addAction:action2];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];

    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        if (self.iconArray.count != 0) {
            [self.iconArray removeAllObjects];
        }
        
        if ([picker allowsEditing]) {
            [self.iconArray addObject:[info objectForKey:UIImagePickerControllerEditedImage]];
            
        }else{
            [self.iconArray addObject:[info objectForKey:UIImagePickerControllerOriginalImage]];
            
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //修改界面上的图片
    [self changeUserImage];
}
- (void)changeUserImage{

    NSNotification *notic = [NSNotification notificationWithName:@"readPhoto" object:nil userInfo:@{@"image":self.iconArray.firstObject}];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
    //将图片存到本地
    [self saveImage:self.iconArray.firstObject];
}
- (void)saveImage:(UIImage *)image{
    NSData* imageData = UIImageJPEGRepresentation(image, 0.5);
    NSDictionary *fileParameters = @{@"image":imageData,
                                     @"fileKey":@"file",
                                     @"nameKey":@"file",
                                     @"mimeType":@"image/jpeg"};
    
    [ZHRequestData uploadFilesWithSubpath:PATH_uploadFileURL parameters:nil fileArray:@[fileParameters] success:^(id json) {
        NSLog(@"上传成功%@",json);
        [self.dictData setObject:json[@"returning"] forKey:@"image"];
        [self changeUserMessage];
        
    } failure:^(NSError *error) {
        NSLog(@"上传失败:%@",error);
    }];
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHPersonMessageTableViewCell" bundle:nil] forCellReuseIdentifier:@"personCell"];
    }
    return _tableView;
}
- (NSArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (NSMutableArray *)iconArray{
    if (_iconArray==nil) {
        _iconArray = [NSMutableArray array];
    }
    return _iconArray;
}
- (NSMutableDictionary *)dictData{
    if (_dictData==nil) {
        _dictData = [NSMutableDictionary dictionary];
    }
    return _dictData;
}
@end
