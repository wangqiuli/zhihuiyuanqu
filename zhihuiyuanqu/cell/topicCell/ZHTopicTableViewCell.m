//
//  ZHTopicTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/8.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHTopicTableViewCell.h"
#import "ZHActivitysModel.h"
#import "UIImageView+WebCache.h"
#import "ZHUser.h"
#define baseTag 200
@interface ZHTopicTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *headerImage;//用户头像
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;//用户名
@property (weak, nonatomic) IBOutlet UILabel *activeLabel;//活动内容
@property(nonatomic,strong)UIImageView *iconSecond;//重新添加的图片
@property(nonatomic,strong)NSMutableArray *iconsArray;//存放多张显示的图片
@property(nonatomic,strong)NSMutableArray *iconArray;//存放单张显示的图片
@property (strong, nonatomic)UILabel *joinNum;//参加人数
@property (strong, nonatomic)UILabel *stopTime;//停止时间
@property(nonatomic,strong)UIButton *joinButton;//参加按钮
@property(nonatomic,strong)ZHActivitysModel *getModel;
@end
@implementation ZHTopicTableViewCell
- (void)setModel:(ZHActivitysModel *)model{
    _model = model;
    self.getModel = model;
    Activity *activity = model.activity;
    ZHUser *user = model.user;
    
    //头像
    NSString *headeerUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,user.image];
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:headeerUrl]];
    self.headerImage.tag = baseTag + 5;
    
    //先移除
    if (self.iconsArray.count != 0) {
        for (UIImageView *icon in self.iconsArray) {
            [icon removeFromSuperview];
        }
        [self.iconsArray removeAllObjects];
    }
    if (self.iconArray.count != 0) {
        for (UIImageView *icon in self.iconArray) {
            [icon removeFromSuperview];
        }
        [self.iconArray removeAllObjects];
    }
    
    float width = (SCREEN_WIDTH-CGRectGetMaxX(self.headerImage.frame)-20)/3.f-10;
    
    if ([activity.image rangeOfString:@";"].location != NSNotFound) {//判断是否有多张图片
        NSArray *array = [activity.image componentsSeparatedByString:@";"];

        //循环添加图片
        for (int i=0; i<array.count; i++) {
            self.iconSecond = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.headerImage.frame)+10+(width+10)*(i%3), CGRectGetMaxY(self.activeLabel.frame)+(width+10)*(i/3), width, width)];
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array[i]];
            [self.iconSecond sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            [self.contentView addSubview:self.iconSecond];
            [self.iconsArray addObject:self.iconSecond];
        }
        
    }else{
        
        self.iconSecond = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.headerImage.frame)+10, CGRectGetMaxY(self.activeLabel.frame), SCREEN_WIDTH-CGRectGetMaxX(self.headerImage.frame)-20, SCREEN_WIDTH-CGRectGetMaxX(self.headerImage.frame)-20)];
        NSString *iconUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,activity.image];
        [self.iconSecond sd_setImageWithURL:[NSURL URLWithString:iconUrl]];
        [self.contentView addSubview:self.iconSecond];
        [self.iconArray addObject:self.iconSecond];
    }

    self.nameLabel.text = user.userName;
    self.activeLabel.text = activity.detail;
    self.joinNum.text = [NSString stringWithFormat:@"已有%zd人报名",activity.personNums];
    self.joinNum.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame), CGRectGetMaxY(self.iconSecond.frame)+5, SCREEN_WIDTH/2.f, 30);
    
    NSString *timeStr = [NSString stringWithFormat:@"%zd",activity.createTime];
    self.stopTime.text = [NSString stringWithFormat:@"截止日期:%@",[timeStr timeChange]];
    self.stopTime.frame = CGRectMake(CGRectGetMaxX(self.headerImage.frame), CGRectGetMaxY(self.joinNum.frame), SCREEN_WIDTH/2.f+50, 30);
    
    [self.joinButton setTitle:@"我要参加" forState:UIControlStateNormal];
    self.joinButton.frame = CGRectMake(SCREEN_WIDTH-90, CGRectGetMaxY(self.iconSecond.frame), 80, 60);
    
    self.maxY = CGRectGetMaxY(self.stopTime.frame)+5;
}

//评论按钮
- (void)joinButtonAction{
    if (self.delegate) {
       [self.delegate tableViewCell:self didClickedLinkWithData:self.model];
    }
}

#pragma mark 懒加载
- (NSMutableArray *)iconsArray{
    if (_iconsArray==nil) {
        _iconsArray = [NSMutableArray array];
    }
    return _iconsArray;
}
- (NSMutableArray *)iconArray{
    if (_iconArray==nil) {
        _iconArray = [NSMutableArray array];
    }
    return _iconArray;
}
- (UILabel *)joinNum{
    if (_joinNum==nil) {
        _joinNum = [[UILabel alloc]init];
        [self.contentView addSubview:_joinNum];
        
    }
    return _joinNum;
}
- (UILabel *)stopTime{
    if (_stopTime==nil) {
        _stopTime = [[UILabel alloc]init];
        _stopTime.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_stopTime];
    }
    return _stopTime;
}
- (UIButton *)joinButton{
    if (_joinButton==nil) {
        _joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_joinButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_joinButton addTarget:self action:@selector(joinButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_joinButton];
    }
    return _joinButton;
}
@end
