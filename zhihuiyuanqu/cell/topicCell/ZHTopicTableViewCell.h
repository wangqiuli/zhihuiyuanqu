//
//  ZHTopicTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/8.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHActivitysModel;
@class ZHTopicTableViewCell;
//制定协议
@protocol ZHTopicTableViewCellDelegate<NSObject>

//协议方法，点击评论按钮，可以评论
- (void)tableViewCell:(ZHTopicTableViewCell *)cell didClickedLinkWithData:(id)data;

@end
@interface ZHTopicTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHActivitysModel *model;

@property(nonatomic,assign)CGFloat maxY;

//设置代理
@property(nonatomic,weak)id <ZHTopicTableViewCellDelegate>delegate;
@end
