//
//  ZHSellDetailTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHSecondhandModel;
@class ZHGetDiscountModel;
@interface ZHSellDetailTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHSecondhandModel *model;
@property(nonatomic,strong)ZHGetDiscountModel *disModel;
@property(nonatomic,assign)CGFloat maxY;

@end
