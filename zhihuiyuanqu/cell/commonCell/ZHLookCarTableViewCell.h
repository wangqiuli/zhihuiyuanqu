//
//  ZHLookCarTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHCarSharesModel;
@interface ZHLookCarTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHCarSharesModel *model;
@property(nonatomic,assign)CGFloat maxY;
@end
