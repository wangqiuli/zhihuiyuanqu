//
//  ZHNoticDetailTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;
@interface ZHNoticDetailTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHFirstPageAdsModel *model;

@property(nonatomic,assign)CGFloat maxY;

@end
