//
//  ZHNoticDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNoticDetailTableViewCell.h"

@interface ZHNoticDetailTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *mangerLabel;

@end
@implementation ZHNoticDetailTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.icon.image = [UIImage imageNamed:@"background"];
    NSString *message = [NSString stringWithFormat:@"2345天气预报提供安徽合肥天气预报,未来安徽合肥15天天气,通过2345天气预报详细了解安徽合肥天气预报以及安徽合肥周边各地区未来15天天气情况,温度,空气质量,降水,风力他们收集了几百块赭石，其中包括两块刻有纵横交叉的三角形和水平横线的石头。"];
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:15]
                          };
    
    CGSize labelSize = [message boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.contentLabel.text = message;
    
    self.contentLabel.frame= CGRectMake(10,CGRectGetMaxY(self.icon.frame)+10, SCREEN_WIDTH-20, labelSize.height);
    self.mangerLabel.text = @"123物业";
    self.maxY = CGRectGetMaxY(self.mangerLabel.frame);

}

@end
