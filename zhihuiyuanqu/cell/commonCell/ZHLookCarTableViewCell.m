//
//  ZHLookCarTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLookCarTableViewCell.h"
#import "ZHCarSharesModel.h"
#import "UIImageView+WebCache.h"
@interface ZHLookCarTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *icon;//图片
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;//位置
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;//时间
@property (weak, nonatomic) IBOutlet UILabel *duringLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;//价格

@end
@implementation ZHLookCarTableViewCell

- (void)setModel:(ZHCarSharesModel *)model{
    _model = model;
    Parkingshare *parkingshare = model.parkingshare;
    if (parkingshare.image.length==0||(parkingshare.image == nil)) {
        self.icon.image = [UIImage imageNamed:@"1"];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,parkingshare.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    self.placeLabel.text = parkingshare.home;
    self.timeLabel.text = @"停车时间";
    self.duringLabel.text = parkingshare.time;
    self.priceLabel.text = parkingshare.price;
    self.maxY = CGRectGetMaxY(self.icon.frame)+10;
}

- (IBAction)chatButtonAction:(id)sender {
    
}


@end
