//
//  ZHSecondHandsNavTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol uploadPhotosDelegate <NSObject>

- (void)uploadPhotos:(UIButton*)button;

@end
@protocol changeSortButtonTextDelegate <NSObject>

- (void)changeSortButtonText:(UIButton *)button;

@end
@class ZHSecondhandModel;
@interface ZHSecondHandsNavTableViewCell : UITableViewCell
@property(nonatomic,strong)ZHSecondhandModel *model;
@property(nonatomic,strong)NSMutableArray *photosArray;
@property(nonatomic,assign)CGFloat maxY;

@property(nonatomic,assign)id<uploadPhotosDelegate> uploadDelegate;
@property(nonatomic,assign)id<changeSortButtonTextDelegate> changeSortDelegate;
@end
