//
//  ZHLiftCollectionViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLiftCollectionViewCell.h"

@interface ZHLiftCollectionViewCell ()

@end
@implementation ZHLiftCollectionViewCell

- (void)setTitleButton:(UIButton *)titleButton{
    _titleButton = titleButton;
    self.maxY = CGRectGetMaxY(titleButton.frame);
}

- (void)setSelected:(BOOL)selected tag:(NSIndexPath *)tag{
    if (selected&&tag.row==1) {
        self.titleButton.selected = YES;
    }else if (selected&&tag.row==2){
        self.titleButton.selected = YES;
    }else{
        self.titleButton.selected = YES;
    }
}


@end
