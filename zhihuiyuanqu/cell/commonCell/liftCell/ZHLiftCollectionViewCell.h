//
//  ZHLiftCollectionViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHLiftCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *icon;

@property (strong, nonatomic) IBOutlet UIButton *titleButton;

@property(nonatomic,assign)CGFloat maxY;

- (void)setSelected:(BOOL)selected tag:(NSIndexPath*)tag;

@end
