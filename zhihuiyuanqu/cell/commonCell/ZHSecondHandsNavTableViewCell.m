//
//  ZHSecondHandsNavTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSecondHandsNavTableViewCell.h"
#import "ZHSecondhandModel.h"
@interface ZHSecondHandsNavTableViewCell ()
@property (strong, nonatomic) IBOutlet UIButton *sellButton;
@property (strong, nonatomic) IBOutlet UIButton *buyButton;

@property (strong, nonatomic) IBOutlet UITextField *titleField;//标题
@property (strong, nonatomic) IBOutlet UITextField *detailField;//描述
@property (strong, nonatomic) IBOutlet UITextField *moneyField;//价位
@property (weak, nonatomic) IBOutlet UIButton *sortButton;//分类
@property (strong, nonatomic) IBOutlet UITextField *contactField;//联系人
@property (strong, nonatomic) IBOutlet UITextField *phoneField;//联系电话
@property (weak, nonatomic) IBOutlet UILabel *uploadLabel;
@property(nonatomic,strong)NSString *type;//类型( 1001转让 , 1002 求购)

@property(nonatomic,strong)UIButton *button;//上传按钮
@property(nonatomic,strong)UIImageView *icon;//
@property(nonatomic,assign)NSInteger tagIndex;
@property(nonatomic,assign)NSMutableArray *iconsArray;//存放读取的图片
@end
@implementation ZHSecondHandsNavTableViewCell

- (void)setModel:(ZHSecondhandModel *)model{
    _model = model;
    self.tagIndex = 200;
    
    [self button];
    self.maxY = CGRectGetMaxY(self.button.frame)+10;

    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(clickReleaseButtonNoticAction:) name:@"SecondHandClickReleaseButton" object:nil];
    
    NSNotificationCenter *center2ReadPhotos = [NSNotificationCenter defaultCenter];
    [center2ReadPhotos addObserver:self selector:@selector(changePhotos:) name:@"SecondHandreadPhoto" object:nil];
    
}

- (void)setPhotosArray:(NSMutableArray *)photosArray{
    _photosArray = photosArray;
    self.iconsArray = photosArray;
    [self addButton:photosArray];
}

- (void)changePhotos:(NSNotification *)notification{
    NSArray *iconsArray = notification.userInfo[@"image"];
    self.iconsArray = [NSMutableArray arrayWithArray:iconsArray];
    [self addButton:iconsArray];
}
- (void)addButton:(NSArray *)iconsArray{
    float width = (SCREEN_WIDTH-60)/5.f;
    for (UIView* uiImageView in self.contentView.subviews) {
        if( [uiImageView  isKindOfClass :[UIImageView class]]){
            [uiImageView removeFromSuperview];
        }
    }
    for (int i = 0 ; i < iconsArray.count ; i++) {
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.uploadLabel.frame)+ 10+i/5*(width+10), width, width)];
        self.icon.image = iconsArray[i];
        self.icon.tag = self.tagIndex++;
        self.icon.backgroundColor = [UIColor redColor];
        self.icon.userInteractionEnabled = YES;
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        [self.contentView addSubview:self.icon];
    }
    [self.button removeFromSuperview];
    self.button = nil;
    [self button];
    self.button.frame = CGRectMake(iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.uploadLabel.frame)+ 10+iconsArray.count/5*(width+10), width, width );
    self.maxY = CGRectGetMaxY(self.button.frame)+20;
    
}

- (void)iconAction:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag+self.tagIndex;
    [self.iconsArray removeObjectAtIndex:tag-self.tagIndex];
    
    [self addButton:self.iconsArray];
    
}

- (void)clickReleaseButtonNoticAction:(NSNotification *)notification{
    
    NSString *address = notification.userInfo[@"address"];//地址
    NSString *image = notification.userInfo[@"image"];//图片
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"userId"];
    
    //判断信息是否输入完善
    if (self.titleField.text.length==0||self.detailField.text.length==0||self.moneyField.text.length==0||self.contactField.text.length==0||address.length==0||self.phoneField.text.length==0) {
        NSNotification *notic1 = [NSNotification notificationWithName:@"SecondHand Incomplete information" object:nil];
        [[NSNotificationCenter defaultCenter]postNotification:notic1];
    }else{
        
        if (self.type==nil) {
            self.type = @"1001";
        }
        NSNotification *notic2 = [NSNotification notificationWithName:@"SecondHand Complete information" object:nil userInfo:@{@"address":address,@"contact":self.contactField.text,@"detail":self.detailField.text,@"price":self.moneyField.text,@"image":image,@"title":self.titleField.text,@"userId":userId,@"type":@"生活用品",@"transferType":self.type,@"phone":self.phoneField.text}];
        [[NSNotificationCenter defaultCenter]postNotification:notic2];
    }
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SecondHandClickReleaseButton" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SecondHandreadPhoto" object:nil];
}
- (IBAction)sellButtonAction:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:139/255.f green:255/255.f blue:253/255.f alpha:1];
    self.buyButton.backgroundColor = [UIColor whiteColor];
    self.type = @"1001";
}

- (IBAction)buyButtonAction:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:139/255.f green:255/255.f blue:253/255.f alpha:1];
    self.sellButton.backgroundColor = [UIColor whiteColor];
    self.type = @"1002";
}
- (IBAction)clickSortButtonAction:(UIButton *)sender {
    if (self.changeSortDelegate) {
        [self.changeSortDelegate changeSortButtonText:sender];
    }
}

- (UIButton *)button{
    if (_button==nil) {
         float width = (SCREEN_WIDTH-60)/5.f;
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor lightGrayColor];
        _button.frame = CGRectMake(10,CGRectGetMaxY(self.uploadLabel.frame)+10, width, width);
        [_button setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        [self.contentView addSubview:_button];
        [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
- (void)buttonAction:(UIButton *)button{
    //读取相册和相机
    if (self.uploadDelegate) {
        [self.uploadDelegate uploadPhotos:button];
    }
}
@end
