//
//  ZHRepairMyTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHRepairModel;
@interface ZHRepairMyTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHRepairModel *model;

@property(nonatomic,assign)CGFloat maxY;
@end
