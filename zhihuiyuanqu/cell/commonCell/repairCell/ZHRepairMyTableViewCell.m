//
//  ZHRepairMyTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHRepairMyTableViewCell.h"
#import "ZHRepairModel.h"
@interface ZHRepairMyTableViewCell ()

@property(nonatomic,strong)UILabel *typeLabel;//维修类型
@property (strong, nonatomic)UILabel *timeLabel;//时间
@property(nonatomic,strong)UILabel *statueLabel;//维修状态
@property(nonatomic,strong)UILabel *HLabel1;//第一条横线
@property (strong, nonatomic)UILabel *contentLabel;
@property (strong, nonatomic)UILabel *Hlabel2;//第二条横线
@property (strong, nonatomic)UIButton *cancelFixButton;//撤销维修按钮

@end
@implementation ZHRepairMyTableViewCell

- (void)setModel:(ZHRepairModel *)model{
    _model = model;
    self.typeLabel.text = model.type;
    NSString *timeStr = [NSString stringWithFormat:@"%zd",model.createTime];
    self.timeLabel.text = [timeStr timeChange];
    self.statueLabel.text = @"待维修";
    [self HLabel1];
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:17]
                          };
    NSString *mes = model.detail;
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.contentLabel.text = mes;
    self.contentLabel.frame = CGRectMake(10, CGRectGetMaxY(self.HLabel1.frame)+10, SCREEN_WIDTH-20, labelSize.height);
    
    [self Hlabel2];
    if (model.process == 1) {
        [self.cancelFixButton setTitle:@"待维修" forState:UIControlStateNormal];
    }else if (model.process == 2){
        [self.cancelFixButton setTitle:@"维修完成" forState:UIControlStateNormal];
    }else{
        [self.cancelFixButton setTitle:@"已撤销" forState:UIControlStateNormal];
    }
    
    self.maxY = CGRectGetMaxY(self.cancelFixButton.frame)+10;
}

#pragma mark 懒加载
- (UILabel *)typeLabel{
    if (_typeLabel==nil) {
        _typeLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 80, 30)];
        _typeLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_typeLabel];
    }
    return _typeLabel;
}
- (UILabel *)timeLabel{
    if (_timeLabel==nil) {
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.typeLabel.frame)+10, 0, SCREEN_WIDTH-CGRectGetMaxX(self.typeLabel.frame)-20, 30)];
        [self.contentView addSubview:_timeLabel];
    }
    return _timeLabel;
}
- (UILabel *)statueLabel{
    if (_statueLabel==nil) {
        _statueLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-80, 0, 70, 30)];
        _statueLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_statueLabel];
    }
    return _statueLabel;
}
- (UILabel *)HLabel1{
    if (_HLabel1==nil) {
        _HLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.typeLabel.frame), SCREEN_WIDTH-20, 1)];
        _HLabel1.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_HLabel1];
    }
    return _HLabel1;
}
- (UILabel *)contentLabel{
    if (_contentLabel==nil) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}
- (UILabel *)Hlabel2{
    if (_Hlabel2==nil) {
        _Hlabel2 = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.contentLabel.frame)+10, SCREEN_WIDTH-20, 1)];
        _Hlabel2.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_Hlabel2];
    }
    return _Hlabel2;
}
- (UIButton *)cancelFixButton{
    if (_cancelFixButton==nil) {
        _cancelFixButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelFixButton.frame = CGRectMake(SCREEN_WIDTH-90, CGRectGetMaxY(self.Hlabel2.frame)+10, 80, 30);
        [_cancelFixButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        _cancelFixButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _cancelFixButton.layer.borderWidth = 2;
        _cancelFixButton.layer.cornerRadius = 5;
        _cancelFixButton.layer.masksToBounds = YES;
        [self.contentView addSubview:_cancelFixButton];
    }
    return _cancelFixButton;
}


@end
