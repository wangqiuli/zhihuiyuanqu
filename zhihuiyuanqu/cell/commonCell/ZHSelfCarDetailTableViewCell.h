//
//  ZHSelfCarDetailTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHCarSharesModel;
@class ZHGetCarModel;
@interface ZHSelfCarDetailTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHCarSharesModel *model;
@property(nonatomic,strong)ZHGetCarModel *disModel;
@property(nonatomic,strong)NSString *tel;

@property(nonatomic,assign)CGFloat maxY;
@end
