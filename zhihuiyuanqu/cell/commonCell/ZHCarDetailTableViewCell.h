//
//  ZHCarDetailTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;
@interface ZHCarDetailTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHFirstPageAdsModel *model;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度
@end
