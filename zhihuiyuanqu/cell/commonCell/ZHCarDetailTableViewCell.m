//
//  ZHCarDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCarDetailTableViewCell.h"

@interface ZHCarDetailTableViewCell ()

@property (strong, nonatomic) IBOutlet UIButton *haveCarButton;
@property (strong, nonatomic) IBOutlet UIButton *lookCarButton;
@property (weak, nonatomic) IBOutlet UITextField *placeTextField;//选择小区
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;//共享时间
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;//理想价位
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;//联系电话
@property(nonatomic,strong)NSString *type;//类型( 1001 我有车位 , 1002 我找车位)

@end
@implementation ZHCarDetailTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.maxY = CGRectGetMaxY(self.phoneTextField.frame)+1;
    
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(clickReleaseButtonNoticAction:) name:@"clickReleaseButton" object:nil];
}
- (void)clickReleaseButtonNoticAction:(NSNotification *)notification{
    
    NSString *address = notification.userInfo[@"address"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"userId"];
    
    //判断信息是否输入完善
    if (self.placeTextField.text.length==0||self.timeTextField.text.length==0||self.moneyTextField.text.length==0||self.phoneTextField.text.length==0||address.length==0||self.moneyTextField.text.length==0) {
        NSNotification *notic1 = [NSNotification notificationWithName:@"Incomplete information" object:nil];
        [[NSNotificationCenter defaultCenter]postNotification:notic1];
    }else{
        
        if (self.type==nil) {
            self.type = @"1001";
        }
        NSNotification *notic2 = [NSNotification notificationWithName:@"Complete information" object:nil userInfo:@{@"address":address,@"home":self.placeTextField.text,@"phone":self.phoneTextField.text,@"price":self.moneyTextField.text,@"time":self.timeTextField.text,@"userId":userId,@"type":self.type}];
        [[NSNotificationCenter defaultCenter]postNotification:notic2];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"clickReleaseButton" object:nil];
    
}

- (IBAction)haveCarAction:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:139/255.f green:255/255.f blue:253/255.f alpha:1];
    self.lookCarButton.backgroundColor = [UIColor whiteColor];
    self.type = @"1001";
}

- (IBAction)lookCarAction:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:139/255.f green:255/255.f blue:253/255.f alpha:1];
    self.haveCarButton.backgroundColor = [UIColor whiteColor];
    self.type = @"1002";
}

@end
