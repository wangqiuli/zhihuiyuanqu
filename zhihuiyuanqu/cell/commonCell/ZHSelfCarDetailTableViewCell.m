//
//  ZHSelfCarDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSelfCarDetailTableViewCell.h"
#import "ZHCarSharesModel.h"
#import "ZHGetCarModel.h"
#import "ZHUser.h"
@interface ZHSelfCarDetailTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *placeLabel;//所在位置
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;//共享时间
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;//理想价位
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;//联系电话

@end
@implementation ZHSelfCarDetailTableViewCell

- (void)setModel:(ZHCarSharesModel *)model{
    _model = model;
    Parkingshare *parkingshare = model.parkingshare;
    
    self.placeLabel.text = parkingshare.home;
    self.moneyLabel.text = parkingshare.price;
    self.timeLabel.text = parkingshare.time;
    self.phoneLabel.text = parkingshare.phone;
    self.tel = self.phoneLabel.text;
    
    self.maxY = CGRectGetMaxY(self.phoneLabel.frame);
}

- (void)setDisModel:(ZHGetCarModel *)disModel{
    _disModel = disModel;
    ZHParkingshare *parkingshare = disModel.parkingshare;
    self.placeLabel.text = parkingshare.home;
    self.moneyLabel.text = parkingshare.price;
    self.timeLabel.text = parkingshare.time;
    self.phoneLabel.text = parkingshare.phone;
    self.tel = self.phoneLabel.text;
    self.maxY = CGRectGetMaxY(self.phoneLabel.frame);
}

@end
