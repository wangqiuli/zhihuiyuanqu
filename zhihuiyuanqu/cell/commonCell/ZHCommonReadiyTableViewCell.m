//
//  ZHCommonReadiyTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCommonReadiyTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ZHGetDynamic.h"
#import "ZHDynamic.h"
@interface ZHCommonReadiyTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property(nonatomic,strong)NSMutableArray *iconArray;//存放图片
@end
@implementation ZHCommonReadiyTableViewCell

- (void)setModel:(ZHGetDynamic *)model{
    _model = model;
    ZHDynamic *dynamic = model.dynamic;
    
    float width = (SCREEN_WIDTH-40)/3.f;
    
    for (UIView *subView in self.contentView.subviews) {
        [subView isKindOfClass:[self.icon class]];
    }
    if (self.iconArray.count != 0) {
        for (UIImageView *icon in self.iconArray) {
            [icon removeFromSuperview];
        }
        [self.iconArray removeAllObjects];
    }
    if ([dynamic.image rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [dynamic.image componentsSeparatedByString:@";"];
        for (int i=0; i<array.count; i++) {
            self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(10+(width+10)*(i%3), CGRectGetMaxY(self.detailLabel.frame)+(width+10)*(i/3), width, width)];
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array[i]];
            [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            [self.contentView addSubview:self.icon];
            [self.iconArray addObject:self.icon];
        }
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,dynamic.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }

    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:15]
                          };
    NSString *mes = dynamic.detail;
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.detailLabel.text = mes;
    self.detailLabel.frame = CGRectMake(10, 0, SCREEN_WIDTH-20, labelSize.height);
    NSString *timeStr = [NSString stringWithFormat:@"%zd",dynamic.createTime];
    self.timeLabel.text = [timeStr timeChange];
    self.maxY = CGRectGetMaxY(self.timeLabel.frame);
}

- (IBAction)deleteButton:(id)sender {
}

- (NSMutableArray *)iconArray{
    if (_iconArray==nil) {
        _iconArray = [NSMutableArray array];
    }
    return _iconArray;
}

@end
