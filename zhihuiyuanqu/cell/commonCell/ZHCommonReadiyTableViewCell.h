//
//  ZHCommonReadiyTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHGetDynamic;
@interface ZHCommonReadiyTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHGetDynamic *model;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度

@end
