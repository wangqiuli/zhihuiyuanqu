//
//  ZHSellDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSellDetailTableViewCell.h"
#import "ZHSecondhandModel.h"
#import "ZHGetDiscountModel.h"
#import "ZHUser.h"
@interface ZHSellDetailTableViewCell ()<UIScrollViewDelegate>
{
    NSArray *_allAds;   // 所有的广告数据
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//标题
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;//图片
@property (nonatomic, strong) UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;//描述
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;//价位
@property (weak, nonatomic) IBOutlet UILabel *contactsLabel;//联系人
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;//联系电话
@property (nonatomic, strong) NSTimer *timer;

@end
@implementation ZHSellDetailTableViewCell

#define timer_duration 2

- (void)setModel:(ZHSecondhandModel *)model{
    _model = model;
    Transfer1 *transfer = model.transfer;
    ZHUser *user = model.user;
    self.titleLabel.text = transfer.title;
    if ([transfer.image rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [transfer.image componentsSeparatedByString:@";"];
        [self setUpScrollView:array];
    }else{
        NSArray *array = [NSArray arrayWithObject:transfer.image];
        [self setUpScrollView:array];
    }

    self.detailLabel.text = transfer.title;
    self.moneyLabel.text = transfer.price;
    self.contactsLabel.text = user.userName;
    self.phoneLabel.text = transfer.phone;
    self.maxY = CGRectGetMaxY(self.phoneLabel.frame)+10;

}

- (void)setDisModel:(ZHGetDiscountModel *)disModel{
    _disModel = disModel;
    TransferDis *transfer = disModel.transfer;
    ZHUser *user = disModel.user;
    self.titleLabel.text = transfer.title;
    if ([transfer.image rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [transfer.image componentsSeparatedByString:@";"];
        [self setUpScrollView:array];
    }else{
        NSArray *array = [NSArray arrayWithObject:transfer.image];
        [self setUpScrollView:array];
    }
    
    self.detailLabel.text = transfer.title;
    self.moneyLabel.text = transfer.price;
    self.contactsLabel.text = user.userName;
    self.phoneLabel.text = transfer.phone;
    self.maxY = CGRectGetMaxY(self.phoneLabel.frame)+10;
}

- (void)setUpScrollView:(NSArray *)iconArray{
    _allAds = iconArray;
    for (UIView *subView in self.scrollView.subviews) {
        [subView removeFromSuperview];
    }
    
    for (int i=0; i<iconArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.frame))];
        
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,iconArray[i]];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        [self.scrollView addSubview:imageView];
        // 设置contentSize
        self.scrollView.contentSize = CGSizeMake(CGRectGetMaxX(imageView.frame), 0);
    }
    
    // 设置默认显示第一页
    self.scrollView.contentOffset = CGPointMake(SCREEN_WIDTH, 0);
    if (_allAds.count==0) {
        return;
    }
    self.pageControl.numberOfPages = _allAds.count;
    [self addSubview:self.pageControl];
}
#pragma mark scrollview的懒加载
- (UIPageControl *)pageControl {
    if (_pageControl==nil) {
        
        float height = 30;
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.scrollView.frame)-height, SCREEN_WIDTH, height)];
        _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
        _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    }
    return _pageControl;
}

#pragma mark ScrollView的协议方法
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self pageControlWithContentOffset:scrollView.contentOffset.x];
}
// 循环滚动相关
- (void)pageControlWithContentOffset:(CGFloat)x {
    
    // 获取当前分页
    NSInteger page = x / SCREEN_WIDTH;
    if (page == 0) {
        // 跳转到最后一个需要显示的图片的位置
        [self.scrollView setContentOffset:CGPointMake(SCREEN_WIDTH *_allAds.count, 0) animated:NO];
        self.pageControl.currentPage = _allAds.count-1;
    }
    else if (page == _allAds.count+1) {
        // 跳转到第一个需要显示的图片的位置, 即1
        [self.scrollView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:NO];
        self.pageControl.currentPage = 0;
    }
    else {
        self.pageControl.currentPage = page - 1;
    }
}

@end
