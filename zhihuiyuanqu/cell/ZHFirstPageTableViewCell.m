//
//  ZHFirstPageTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageTableViewCell.h"
#import "ZHDiscountModel.h"
#import "ZHDevelopmentModel.h"
#import "ZHSaveModel.h"
#import "UIImageView+WebCache.h"
@interface ZHFirstPageTableViewCell ()
@end
@implementation ZHFirstPageTableViewCell

- (void)setModel:(ZHDiscountModel *)model{
    _model = model;
    Discount *discount = model.discount;
    self.titleLabel.text = discount.detail;
    
    if ([discount.imageSrc rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [discount.imageSrc componentsSeparatedByString:@";"];
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array.firstObject];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,discount.imageSrc];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    
    NSString *timeStr = [NSString stringWithFormat:@"%zd",discount.createTime];
    self.timeLabel.text = [timeStr timeChange];
    self.maxY = CGRectGetMaxY(self.timeLabel.frame);
}

- (void)setModelDev:(ZHDevelopmentModel *)modelDev{
    _modelDev = modelDev;
    self.titleLabel.text = modelDev.title;
    
    if ([modelDev.image rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [modelDev.image componentsSeparatedByString:@";"];
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array.firstObject];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,modelDev.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    
    NSString *timeStr = [NSString stringWithFormat:@"%zd",modelDev.createTime];
    self.timeLabel.text = [timeStr timeChange];
    self.maxY = CGRectGetMaxY(self.timeLabel.frame);
}

- (void)setSaveModel:(ZHSaveModel *)saveModel{
    _saveModel = saveModel;
    self.titleLabel.text = saveModel.title;
    
    if (saveModel.image.length==0) {
        [self.icon removeFromSuperview];
        self.timeLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), SCREEN_WIDTH/2.f+50, 30);
//        self.icon.image = [UIImage imageNamed:@"1"];
    }else{
        if ([saveModel.image rangeOfString:@";"].location != NSNotFound) {
            NSArray *array = [saveModel.image componentsSeparatedByString:@";"];
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array.firstObject];
            [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        }else{
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,saveModel.image];
            [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        }
    }
    
    NSString *timeStr = [NSString stringWithFormat:@"%zd",saveModel.createTime];
    self.timeLabel.text = [timeStr timeChange];
    self.maxY = CGRectGetMaxY(self.timeLabel.frame);
}

@end
