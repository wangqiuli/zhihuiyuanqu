//
//  ZHAddPlacesTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHHomeModel;
@interface ZHAddPlacesTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHHomeModel *model;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *selectButton;

- (void)clickButton;

@end
