//
//  ZHUploadHomeTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/20.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHUploadHomeTableViewCell.h"
#import "ZHSearchHomeModel.h"
@interface ZHUploadHomeTableViewCell ()
@property (weak, nonatomic) IBOutlet UITextField *homeNameTextField;//小区名称
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;//小区地址
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;//联系电话

@end
@implementation ZHUploadHomeTableViewCell

- (void)setModel:(ZHSearchHomeModel *)model{
    _model = model;
    self.homeNameTextField.text = model.name;
    self.addressTextField.text = model.address;
    self.phoneTextField.text = model.tel;
    self.maxY = CGRectGetMaxY(self.phoneTextField.frame)+10;
}

@end
