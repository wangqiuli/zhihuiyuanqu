//
//  ZHNavLocationTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/15.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHHomeModel;
@class ZHAddHomeModel;
@interface ZHNavLocationTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHHomeModel *model;
@property(nonatomic,strong)ZHAddHomeModel *addHomeModel;
@property(nonatomic,strong)NSArray *addSource;//存储小区
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度

//设置按钮
@property(nonatomic,copy)void(^setButtonActionBlock)(void);

//添加按钮
@property(nonatomic,copy)void(^addButtonActionBlock)(void);

@end
