//
//  ZHNeighborTableViewCell.m
//  
//
//  Created by leo on 16/4/7.
//
//

#import "ZHNeighborTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ZHGetDynamic.h"
#import "ZHDynamic.h"
#import "ZHUser.h"
#import "ZHDiscusslistModel.h"
#define baseTag 200
@interface ZHNeighborTableViewCell ()<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imagePerson;//头像
@property (strong, nonatomic) IBOutlet UILabel *name;//名字
@property (strong, nonatomic) IBOutlet UILabel *journal;//发表内容
@property (strong, nonatomic) IBOutlet UIImageView *icon;//发表图片
@property (strong, nonatomic)UILabel *stopTime;//发表时间

@property(nonatomic,strong)UIButton *commentButton;//点击评论的按钮
@property(nonatomic,strong)UILabel *commentUserLabel;//评论人
@property(nonatomic,strong)UILabel *commentContentLabel;//评论内容
@property(nonatomic,strong)UILabel *replyLabel;//“回复”label

@property(nonatomic,strong)NSMutableArray *iconArray;//存放图片
@property(nonatomic,strong)NSMutableArray *commentUserArray;//存放评论的人
@property(nonatomic,strong)NSMutableArray *commentContentArray;//存放评论内容

@end
@implementation ZHNeighborTableViewCell

- (void)setModel:(ZHGetDynamic *)model{
    _model = model;
    ZHDynamic *dynamic = model.dynamic;
    ZHUser *user = model.user;
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,user.image];
    [self.imagePerson sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.name.text = user.userName;
    
    NSString *mes = dynamic.detail;
    CGSize size = CGSizeMake(SCREEN_WIDTH-30-CGRectGetMaxX(self.imagePerson.frame), CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:17]
                          };
    
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.journal.text = mes;
    self.journal.frame = CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10, CGRectGetMaxY(self.name.frame), SCREEN_WIDTH-30-CGRectGetMaxX(self.imagePerson.frame), labelSize.height+10);
    
    self.imagePerson.tag = baseTag + 5;

    float width = (SCREEN_WIDTH-CGRectGetMaxX(self.imagePerson.frame)-20)/3.f-10;
        
    for (UIView *subView in self.contentView.subviews) {
        if (subView.tag != baseTag+5) {
            [subView isKindOfClass:[self.icon class]];
            }
        }
        if (self.iconArray.count != 0) {
            for (UIImageView *icon in self.iconArray) {
                [icon removeFromSuperview];
            }
            [self.iconArray removeAllObjects];
        }
    
    if ([dynamic.image rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [dynamic.image componentsSeparatedByString:@";"];
        for (int i=0; i<array.count; i++) {
            self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10+(width+10)*(i%3), CGRectGetMaxY(self.journal.frame)+(width+10)*(i/3), width, width)];
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array[i]];
            [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            [self.contentView addSubview:self.icon];
            [self.iconArray addObject:self.icon];
        }
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,dynamic.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }

    NSString *timeStr = [NSString stringWithFormat:@"%zd",dynamic.createTime];
    self.stopTime.text = [timeStr timeChange];
    self.stopTime.frame = CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10, CGRectGetMaxY(self.icon.frame), 200, 30);
    
//    __weak typeof(self) weakSelf = self;
//    self.commentButtonClick = ^(NSString *text){
//        weakSelf.commentContentLabel.text = text;
//        weakSelf.commentContentLabel.frame = CGRectMake(CGRectGetMaxX(weakSelf.imagePerson.frame)+10, CGRectGetMaxY(weakSelf.stopTime.frame), SCREEN_WIDTH-CGRectGetMaxX(weakSelf.imagePerson.frame)-30, 30);
//    };
    
    //评论块
    if (self.commentUserArray.count>0) {
        for (UILabel *label in self.commentUserArray) {
            [label removeFromSuperview];
        }
    }
    if (self.commentContentArray.count>0) {
        for (UILabel *label in self.commentContentArray) {
            [label removeFromSuperview];
        }
    }
    
    if (model.discussList.count>0) {
        for (int i=0; i<model.discussList.count; i++) {
            NSDictionary *userModel = model.discussList[i];
            self.commentUserLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10, CGRectGetMaxY(self.stopTime.frame)+i*30, 60, 30)];
            self.commentUserLabel.text = [NSString stringWithFormat:@"%@:",userModel[@"userName"]];
            self.commentUserLabel.textColor = [UIColor blueColor];
            
            self.commentContentLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.commentUserLabel.frame),CGRectGetMaxY(self.stopTime.frame)+i*30 , SCREEN_WIDTH-CGRectGetMaxX(self.commentUserLabel.frame)-10, 30)];
            self.commentContentLabel.text = userModel[@"discuss"];
            
            [self.contentView addSubview:self.commentUserLabel];
            [self.contentView addSubview:self.commentContentLabel];
            [self.commentUserArray addObject:self.commentUserLabel];
            [self.commentContentArray addObject:self.commentContentLabel];
        }
        
        self.commentButton.frame = CGRectMake(SCREEN_WIDTH-60, CGRectGetMaxY(self.commentUserLabel.frame), 50, 30);
        [self.contentView addSubview:self.commentButton];
        
    }else{
        self.commentButton.frame = CGRectMake(SCREEN_WIDTH-60, CGRectGetMinY(self.stopTime.frame), 50, 30);
        [self.contentView addSubview:self.commentButton];
    }
    self.maxY = CGRectGetMaxY(self.commentButton.frame)+5;
    
}

- (UILabel *)stopTime{
    if (_stopTime==nil) {
        _stopTime = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imagePerson.frame)+10, CGRectGetMaxY(self.icon.frame)+5, SCREEN_WIDTH/2.f, 30)];
        _stopTime.textColor = [UIColor grayColor];
        [self.contentView addSubview:_stopTime];
    }
    return _stopTime;
}
- (UIButton *)commentButton{
    if (_commentButton==nil) {
        _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_commentButton setImage:[UIImage imageNamed:@"pinglun"] forState:UIControlStateNormal];
        [_commentButton addTarget:self action:@selector(commentButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentButton;
}
- (void)commentButtonAction:(UIButton *)button{
    

        if (self.delegate) {
            [self.delegate commentButtonClickDelegateAction];
        }

    
}
- (NSMutableArray *)iconArray{
    if (_iconArray==nil) {
        _iconArray = [NSMutableArray array];
    }
    return _iconArray;
}
- (NSMutableArray *)commentUserArray{
    if (_commentUserArray==nil) {
        _commentUserArray = [NSMutableArray array];
    }
    return _commentUserArray;
}
- (NSMutableArray *)commentContentArray{
    if (_commentContentArray==nil) {
        _commentContentArray = [NSMutableArray array];
    }
    return _commentContentArray;
}
@end
