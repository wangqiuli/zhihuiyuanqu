//
//  ZHUploadActiveTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol uploadPhotosDelegate <NSObject>

- (void)uploadPhotos:(UIButton*)button;

@end

@class ZHFirstPageAdsModel;
@interface ZHUploadActiveTableViewCell : UITableViewCell
@property(nonatomic,strong)ZHFirstPageAdsModel *model;
@property(nonatomic,strong)NSMutableArray *photosArray;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度

@property(nonatomic,assign)id<uploadPhotosDelegate> uploadDelegate;

@end
