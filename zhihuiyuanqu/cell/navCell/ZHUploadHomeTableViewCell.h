//
//  ZHUploadHomeTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/20.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHSearchHomeModel;
@interface ZHUploadHomeTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHSearchHomeModel *model;

@property(nonatomic,assign)CGFloat maxY;

@end
