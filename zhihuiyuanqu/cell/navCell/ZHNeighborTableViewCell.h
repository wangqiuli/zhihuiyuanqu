//
//  ZHNeighborTableViewCell.h
//  
//
//  Created by leo on 16/4/7.
//
//

#import <UIKit/UIKit.h>

@protocol commentButtonClickDelegate <NSObject>

- (void)commentButtonClickDelegateAction;

@end
@class ZHGetDynamic;
@interface ZHNeighborTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHGetDynamic *model;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度
@property(nonatomic,strong)void(^commentButtonClick)();//点击评论按钮的回调
@property(nonatomic,weak)id<commentButtonClickDelegate> delegate;
@end
