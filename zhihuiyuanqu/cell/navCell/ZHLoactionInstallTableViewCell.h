//
//  ZHLoactionInstallTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHLoactionInstallTableViewCell : UITableViewCell

@property(nonatomic,strong)NSArray *defaultSource;//存储默认小区

@property(nonatomic,strong)NSArray *addSource;//存储添加小区

@property(nonatomic,assign)CGFloat maxY;//cell的最大高度

@end
