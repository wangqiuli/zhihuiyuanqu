//
//  ZHAddPlacesTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHAddPlacesTableViewCell.h"
#import "ZHHomeModel.h"
@interface ZHAddPlacesTableViewCell ()

@end
@implementation ZHAddPlacesTableViewCell

- (void)setModel:(ZHHomeModel *)model{
    _model = model;
    self.titleLabel.text = model.homeName;
    
}

- (void)clickButton{
    if (self.selectButton.selected) {
        self.selectButton.selected = NO;
        
    }else{
        self.selectButton.selected = YES;
        
    }
}

@end
