//
//  ZHLoactionInstallTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLoactionInstallTableViewCell.h"
#import "ZHAddHomeModel.h"
#define wid SCREEN_WIDTH/2.f-30
#define Base_Tag 20000
@interface ZHLoactionInstallTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *defaultLabel;//默认小区
@property (weak, nonatomic) IBOutlet UILabel *addLabel;//添加小区label
@property(nonatomic,strong)UIButton *addButton;//添加小区Button
@property(nonatomic,strong)UIButton *button;//按钮
@property(nonatomic,strong)NSMutableArray *arraySource;
@end
@implementation ZHLoactionInstallTableViewCell


- (void)setDefaultSource:(NSArray *)defaultSource{
    _defaultSource = defaultSource;
    ZHAddHomeModel *model = defaultSource.firstObject;
    self.defaultLabel.text = model.home;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.defaultLabel.text forKey:@"defaultHome"];
}
- (void)setAddSource:(NSArray *)addSource{
    _addSource = addSource;
    CGFloat height = [UIScreen mainScreen].bounds.size.width /2.f-10;
    for (int i=0; i<self.addSource.count; i++) {
        ZHAddHomeModel *model = self.addSource[i];
        self.addButton = [[UIButton alloc]initWithFrame:CGRectMake((i%2)*height +20, CGRectGetMaxY(self.addLabel.frame)+10+50*(i/2), wid, 40)];
        self.addButton.backgroundColor = [UIColor whiteColor];
        [self.addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.addButton setTitle:model.home forState:UIControlStateNormal];
        [self.addButton addTarget:self action:@selector(addButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:self.addButton];
        
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake(CGRectGetWidth(self.addButton.frame)-40, 0, 40, 40);
        [self.button setImage:[UIImage imageNamed:@"RadioButton-Unselected"] forState:UIControlStateNormal];
        [self.button setImage:[UIImage imageNamed:@"RadioButton-Selected"] forState:UIControlStateSelected];
        
        self.button.userInteractionEnabled = NO;
        [self.addButton addSubview:self.button];
        [self.arraySource addObject:self.button];
        self.addButton.tag = Base_Tag + i;
        self.button.tag = Base_Tag/2 + i;
        
    }
    self.maxY = CGRectGetMaxY(self.addButton.frame)+20;

}
- (void)addButtonAction:(UIButton *)button{

    UIButton *clickButton = button.subviews.firstObject;
    
    if (clickButton.selected) {
        clickButton.selected = NO;
    }else{
        clickButton.selected = YES;
        for (int i=0; i<self.arraySource.count; i++) {
            UIButton *b = self.arraySource[i];
            if (b.tag != clickButton.tag) {
                b.selected = NO;
            }
        }
        self.defaultLabel.text = button.titleLabel.text;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.defaultLabel.text forKey:@"defaultHome"];
    }
}
- (NSMutableArray *)arraySource{
    if (_arraySource==nil) {
        _arraySource = [NSMutableArray array];
    }
    return _arraySource;
}
@end
