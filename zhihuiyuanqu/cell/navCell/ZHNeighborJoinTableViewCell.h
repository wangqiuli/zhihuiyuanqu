//
//  ZHNeighborJoinTableViewCell.h
//  
//
//  Created by leo on 16/4/7.
//
//

#import <UIKit/UIKit.h>

@class ZHGetActiveModel;
@class ZHUser;
@interface ZHNeighborJoinTableViewCell : UITableViewCell

- (void)getWithActivitysModel:(ZHGetActiveModel *)activeModel withUserArray:(NSMutableArray *)userArray;

@property(nonatomic,assign)CGFloat maxY;
@end
