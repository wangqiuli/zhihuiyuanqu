//
//  ZHNeighborJoinTableViewCell.m
//  
//
//  Created by leo on 16/4/7.
//
//

#import "ZHNeighborJoinTableViewCell.h"
//#import "ZHActivitysModel.h"
#import "ZHGetActiveModel.h"
#import "ZHUser.h"
@interface ZHNeighborJoinTableViewCell ()
@property (strong, nonatomic)UIImageView *icon;//动态图片
@property (strong, nonatomic)UILabel *journalLabel;//动态说说
@property (strong, nonatomic)UILabel *nameLabel;//发起人名
@property (strong, nonatomic)UILabel *placeLabel;//地点
@property (strong, nonatomic)UILabel *startLabel;//发起日期
@property (strong, nonatomic)UILabel *stopLabel;//截止日期
@property(strong,nonatomic)UILabel *hLabel1;//横线1
@property (strong, nonatomic)UILabel *activeLabel;//活动介绍
@property(strong,nonatomic)UILabel *hLabel2;//横线2
@property(strong,nonatomic)UILabel *userLabel;//活动参加人
@property (strong, nonatomic)UILabel *numberLabel;//参加人数
@property (strong, nonatomic) UIImageView *iconPeople;

@end
@implementation ZHNeighborJoinTableViewCell

- (void)getWithActivitysModel:(ZHGetActiveModel *)activeModel withUserArray:(NSMutableArray *)userArray{
    
    [self createControl];
    
    ZHActivity *activity = activeModel.activity;
    ZHUser *user = activeModel.user;
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,activity.image];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.journalLabel.text = activity.activity;
    self.nameLabel.text = [NSString stringWithFormat:@"发起人:%@",user.userName];
    self.placeLabel.text = [NSString stringWithFormat:@"活动地点:%@",activity.toWhere];;
    
    NSString *startStr = [NSString stringWithFormat:@"%zd",activity.createTime];
    self.startLabel.text = [NSString stringWithFormat:@"发起日期:%@",[startStr timeChange]];
    NSString *stopStr = [NSString stringWithFormat:@"%zd",activity.activityTime];
    self.stopLabel.text = [NSString stringWithFormat:@"截止日期:%@",[stopStr timeChange]];
    
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:17]
                          };
    NSString *mes = activity.detail;
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.activeLabel.text = [NSString stringWithFormat:@"活动介绍:\n       %@",mes];
    self.activeLabel.frame = CGRectMake(10, CGRectGetMaxY(self.hLabel1.frame)+5, SCREEN_WIDTH-20, labelSize.height+30);
    [self.contentView addSubview:self.hLabel2];
    [self.contentView addSubview:self.userLabel];
    [self.contentView addSubview:self.numberLabel];
    
    self.numberLabel.text = [NSString stringWithFormat:@"%zd人",userArray.count];
    float width = (SCREEN_WIDTH-60)/5.f;
    if (userArray.count>0) {
        for (int i=0; i<userArray.count; i++) {
            NSDictionary *info = userArray[i][@"user"];
            
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,info[@"image"]];
            self.iconPeople = [[UIImageView alloc]initWithFrame:CGRectMake(i % 5 * (width+10)+10,CGRectGetMaxY(self.userLabel.frame)+10+ i / 5 * (width+10), width, width)];
            [self.iconPeople sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            self.iconPeople.backgroundColor = [UIColor redColor];
            [self.contentView addSubview:self.iconPeople];
        }
        self.maxY = CGRectGetMaxY(self.iconPeople.frame)+10;

    }else{
        self.maxY = CGRectGetMaxY(self.userLabel.frame)+10;
    }
    
}

- (UIImageView *)icon{
    if (_icon==nil) {
        _icon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 250)];
    }
    return _icon;
}
- (UILabel *)journalLabel{
    if (_journalLabel==nil) {
        _journalLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.icon.frame), SCREEN_WIDTH-20, 30)];
    }
    return _journalLabel;
}
- (UILabel *)nameLabel{
    if (_nameLabel==nil) {
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.journalLabel.frame), SCREEN_WIDTH-20, 30)];
    }
    return _nameLabel;
}
- (UILabel *)placeLabel{
    if (_placeLabel==nil) {
        _placeLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.nameLabel.frame), SCREEN_WIDTH-20, 30)];
    }
    return _placeLabel;
}
- (UILabel *)startLabel{
    if (_startLabel==nil) {
        _startLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.placeLabel.frame), SCREEN_WIDTH-20, 30)];
    }
    return _startLabel;
}
- (UILabel *)stopLabel{
    if (_stopLabel==nil) {
        _stopLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.startLabel.frame), SCREEN_WIDTH-20, 30)];
    }
    return _stopLabel;
}
- (UILabel *)hLabel1{
    if (_hLabel1==nil) {
        _hLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.stopLabel.frame)+5, SCREEN_WIDTH-20, 1)];
        _hLabel1.backgroundColor = [UIColor lightGrayColor];
    }
    return _hLabel1;
}
- (UILabel *)activeLabel{
    if (_activeLabel==nil) {
        _activeLabel = [[UILabel alloc]init];
        _activeLabel.numberOfLines = 0;
    }
    return _activeLabel;
}
- (UILabel *)hLabel2{
    if (_hLabel2==nil) {
        _hLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.activeLabel.frame)+5, SCREEN_WIDTH-20, 1)];
        _hLabel2.backgroundColor = [UIColor lightGrayColor];
    }
    return _hLabel2;
}
- (UILabel *)userLabel{
    if (_userLabel==nil) {
        _userLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.hLabel2.frame), 150, 30)];
        _userLabel.text = @"活动参与人:";
    }
    return _userLabel;
}
- (UILabel *)numberLabel{
    if (_numberLabel==nil) {
        _numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.userLabel.frame), CGRectGetMaxY(self.hLabel2.frame), SCREEN_WIDTH-CGRectGetMaxX(self.userLabel.frame)-20, 30)];
        _numberLabel.textAlignment = NSTextAlignmentRight;
    }
    return _numberLabel;
}

- (void)createControl{
        [self.contentView addSubview:self.icon];
        [self.contentView addSubview:self.journalLabel];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.placeLabel];
        [self.contentView addSubview:self.startLabel];
        [self.contentView addSubview:self.stopLabel];
        [self.contentView addSubview:self.hLabel1];
        [self.contentView addSubview:self.activeLabel];
}

@end
