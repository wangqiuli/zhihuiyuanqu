//
//  ZHUploadActiveTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHUploadActiveTableViewCell.h"
#import "PickerChoiceView.h"
@interface ZHUploadActiveTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *placeLabel;//活动地点
@property (strong, nonatomic) IBOutlet UITextField *nameTextF;//名称
@property (strong, nonatomic) IBOutlet UITextField *detailTextF;//描述
@property (strong, nonatomic) IBOutlet UIButton *timeButton;//时间
@property (strong, nonatomic) IBOutlet UILabel *uploadLabel;//上传图片label

@property(nonatomic,strong)UIButton *button;//上传按钮
@property(nonatomic,strong)UIImageView *icon;//
@property(nonatomic,assign)NSInteger tagIndex;
@property(nonatomic,assign)NSMutableArray *iconsArray;//存放读取的图片

@end
@implementation ZHUploadActiveTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.tagIndex = 200;
    [self button];
    self.maxY = CGRectGetMaxY(self.button.frame)+20;
    
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(clickReleaseButtonNoticAction:) name:@"UploadActiveDiscountClickReleaseButton" object:nil];
    
    NSNotificationCenter *center2ReadPhotos = [NSNotificationCenter defaultCenter];
    [center2ReadPhotos addObserver:self selector:@selector(changePhotos:) name:@"UploadActiveDiscountHandreadPhoto" object:nil];
    
    NSNotificationCenter *startButtonStr = [NSNotificationCenter defaultCenter];
    [startButtonStr addObserver:self selector:@selector(changePhotos1:) name:@"UploadActivestartButtonStr" object:nil];
    
    NSNotificationCenter *updateUserPlace = [NSNotificationCenter defaultCenter];
    [updateUserPlace addObserver:self selector:@selector(updateUserPlace:) name:@"updateUserPlace" object:nil];
}
- (void)updateUserPlace:(NSNotification *)notification{
    self.placeLabel.text = notification.userInfo[@"address"];
}


- (void)changePhotos1:(NSNotification *)notification{
    [self.timeButton setTitle:notification.userInfo[@"startButton"] forState:UIControlStateNormal];
}


- (void)setPhotosArray:(NSMutableArray *)photosArray{
    _photosArray = photosArray;
    self.iconsArray = photosArray;
    [self addButton:photosArray];
}
- (void)changePhotos:(NSNotification *)notification{
    NSArray *iconsArray = notification.userInfo[@"image"];
    self.iconsArray = [NSMutableArray arrayWithArray:iconsArray];
    [self addButton:iconsArray];
}
- (void)addButton:(NSArray *)iconsArray{
    float width = (SCREEN_WIDTH-60)/5.f;
    for (UIView* uiImageView in self.contentView.subviews) {
        if( [uiImageView  isKindOfClass :[UIImageView class]]){
            [uiImageView removeFromSuperview];
        }
    }
    for (int i = 0 ; i < iconsArray.count ; i++) {
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.uploadLabel.frame)+ 10+i/5*(width+10), width, width)];
        self.icon.image = iconsArray[i];
        self.icon.tag = self.tagIndex++;
        self.icon.backgroundColor = [UIColor redColor];
        self.icon.userInteractionEnabled = YES;
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        [self.contentView addSubview:self.icon];
    }
    [self.button removeFromSuperview];
    self.button = nil;
    [self button];
    self.button.frame = CGRectMake(iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.uploadLabel.frame)+ 10+iconsArray.count/5*(width+10), width, width );
    self.maxY = CGRectGetMaxY(self.button.frame)+20;
    
}

- (void)iconAction:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag+self.tagIndex;
    [self.iconsArray removeObjectAtIndex:tag-self.tagIndex];
    
    [self addButton:self.iconsArray];
    
}

- (void)clickReleaseButtonNoticAction:(NSNotification *)notification{
    
    NSLog(@"活动名称:%@",self.nameTextF.text);
    
    NSString *address = notification.userInfo[@"address"];//地址
    NSString *image = notification.userInfo[@"image"];//图片
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"userId"]];
    
    //判断信息是否输入完善
    if (self.detailTextF.text.length==0||self.placeLabel.text.length==0||self.nameTextF.text.length==0||address.length==0||self.timeButton.titleLabel.text.length==0) {
        NSNotification *notic1 = [NSNotification notificationWithName:@"UploadActiveDiscount Incomplete information" object:nil];
        [[NSNotificationCenter defaultCenter]postNotification:notic1];
    }else{
        
        NSNotification *notic2 = [NSNotification notificationWithName:@"UploadActiveDiscount Complete information" object:nil userInfo:@{@"activity":self.nameTextF.text,@"detail":self.detailTextF.text,@"activityTime":[self.timeButton.titleLabel.text changeTime],@"image":image,@"userId":userId,@"toWhere":self.placeLabel.text}];
        [[NSNotificationCenter defaultCenter]postNotification:notic2];
    }
}

/**{"activity":{"activity":"1111111","activityTime":1469694805190,"detail":"11111111","image":"group1/M00/00/03/wKgKe1dyp4iATV3PAACtROPfePc712.png","toWhere":"新长城星云商务酒店-餐厅","userId":1},"lat":31.848888,"lng":117.231033}*/
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UploadActiveDiscountClickReleaseButton" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UploadActiveDiscountreadPhoto" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UploadActivestartButtonStr" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UploadActivestopButtonStr" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateUserPlace" object:nil];
}


- (UIButton *)button{
    if (_button==nil) {
        float width = (SCREEN_WIDTH-60)/5.f;
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor lightGrayColor];
        _button.frame = CGRectMake(10,CGRectGetMaxY(self.uploadLabel.frame)+10, width, width);
        [_button setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        [self.contentView addSubview:_button];
        [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
- (void)buttonAction:(UIButton *)button{
    //读取相册和相机
    if (self.uploadDelegate) {
        [self.uploadDelegate uploadPhotos:button];
    }
}

- (IBAction)startTimeButtonAction:(UIButton *)sender {
    
    NSNotification *notic = [NSNotification notificationWithName:@"UploadActiveDiscountStartButton" object:nil ];
    [[NSNotificationCenter defaultCenter]postNotification:notic];

}
- (void)StartPickerSelectorIndixString:(NSString *)str{
    [self.timeButton setTitle:str forState:UIControlStateNormal];
}



@end
