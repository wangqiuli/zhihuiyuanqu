//
//  ZHNavLocationTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/15.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNavLocationTableViewCell.h"
#import "ZHHomeModel.h"
#import "ZHAddHomeModel.h"
#define wid SCREEN_WIDTH/2.f-30

@interface ZHNavLocationTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;//GPS定位小区
@property (strong, nonatomic) IBOutlet UILabel *defaultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *addLabel;//添加小区label
@property(strong,nonatomic)UIButton *addButton;//添加按钮
@end
@implementation ZHNavLocationTableViewCell

- (void)setModel:(ZHHomeModel *)model{
    _model = model;
    Home *home = model.home;
    self.locationLabel.text = home.home;
    
}
- (void)setAddHomeModel:(ZHAddHomeModel *)addHomeModel{
    _addHomeModel = addHomeModel;
    self.defaultsLabel.text = addHomeModel.home;
}
- (void)setAddSource:(NSArray *)addSource{
    _addSource = addSource;
    NSInteger last = self.addSource.count;
    CGFloat height = [UIScreen mainScreen].bounds.size.width /2.f-10;
    for (int i=0; i<self.addSource.count; i++) {
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake((i%2)*height +20, CGRectGetMaxY(self.addLabel.frame)+10+50*(i/2), wid, 40)];
        label.tag = 100+i;
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        ZHAddHomeModel *model = self.addSource[i];
        label.text =  model.home;
        [self.contentView addSubview:label];
        
    }
    
    [self.addButton setTitle:@"+添加" forState:UIControlStateNormal];
    self.addButton.frame = CGRectMake((last%2)*height+20, CGRectGetMaxY(self.addLabel.frame)+10+50*(last/2), wid, 40);
    self.maxY = CGRectGetMaxY(self.addButton.frame)+20;

}
#pragma mark 懒加载
- (UIButton *)addButton{
    if (_addButton==nil) {
        _addButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _addButton.backgroundColor = [UIColor whiteColor];
        _addButton.titleLabel.font = [UIFont systemFontOfSize:17];
        [_addButton addTarget:self action:@selector(addButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addButton];
    }
    return _addButton;
}

//设置按钮
- (IBAction)setButton:(UIButton *)sender {

    if (self.setButtonActionBlock) {
        self.setButtonActionBlock();
    }
    
}

////添加按钮
- (void)addButtonAction{
    if (self.addButtonActionBlock) {
        self.addButtonActionBlock();
    }
}


@end
