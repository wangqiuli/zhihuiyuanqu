//
//  ZHFirstPageTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ZHDiscountModel;
@class ZHDevelopmentModel;
@class ZHSaveModel;
@interface ZHFirstPageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//标题
@property (weak, nonatomic) IBOutlet UIImageView *icon;//图片
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;//发布时间

@property(nonatomic,strong)ZHDiscountModel *model;
@property(nonatomic,strong)ZHDevelopmentModel *modelDev;
@property(nonatomic,strong)ZHSaveModel *saveModel;
@property(nonatomic,assign)CGFloat maxY;
@end
