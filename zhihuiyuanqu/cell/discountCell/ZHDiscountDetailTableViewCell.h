//
//  ZHDiscountDetailTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHDiscountModel;
@class ZHGetReallyDiscountModel;
@interface ZHDiscountDetailTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHDiscountModel *model;
@property(nonatomic,strong)ZHGetReallyDiscountModel *discountModel;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度

@end
