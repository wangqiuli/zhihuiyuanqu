//
//  ZHReleaseDiscountTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol uploadPhotosDelegate <NSObject>

- (void)uploadPhotos:(UIButton*)button;

@end
@protocol changeSortButtonTextDelegate <NSObject>

- (void)changeSortButtonText:(UIButton *)button;

@end
@class ZHFirstPageAdsModel;
@interface ZHReleaseDiscountTableViewCell : UITableViewCell
@property(nonatomic,strong)ZHFirstPageAdsModel *model;
@property(nonatomic,strong)NSMutableArray *photosArray;
@property(nonatomic,assign)CGFloat maxY;//cell的最大高度

@property(nonatomic,assign)id<uploadPhotosDelegate> uploadDelegate;
@property(nonatomic,assign)id<changeSortButtonTextDelegate> changeSortDelegate;

@end
