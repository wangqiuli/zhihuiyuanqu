//
//  PickerChoiceView.h
//  TFPickerView
//
//  Created by TF_man on 16/5/11.
//  Copyright © 2016年 tituanwang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StartPickerDelegate <NSObject>
@optional;
- (void)StartPickerSelectorIndixString:(NSString *)str;
@end

@protocol StopPickerDelegate <NSObject>
@optional;
- (void)StopPickerSelectorIndixString:(NSString *)str;
@end

typedef NS_ENUM(NSInteger, ARRAYTYPE) {
    GenderArray,
    HeightArray,
    weightArray,
    DeteArray
};

@interface PickerChoiceView : UIView

@property (nonatomic, assign) ARRAYTYPE arrayType;

@property (nonatomic, strong) NSArray *customArr;

@property (nonatomic,strong)UILabel *selectLb;

@property (nonatomic,assign)id<StartPickerDelegate>StartDelegate;
@property (nonatomic,assign)id<StopPickerDelegate>StopDelegate;
@end
