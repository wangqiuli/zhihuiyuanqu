//
//  ZHReleaseDiscountTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHReleaseDiscountTableViewCell.h"
#import "PickerChoiceView.h"
@interface ZHReleaseDiscountTableViewCell ()
@property (strong, nonatomic) IBOutlet UITextField *detailTextField;//描述
@property (strong, nonatomic) IBOutlet UIButton *startTimeButton;
@property (strong, nonatomic) IBOutlet UIButton *stopTimeButton;
@property (strong, nonatomic) IBOutlet UIButton *sortButton;//分类
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField1;
@property (strong, nonatomic) IBOutlet UILabel *uploadLabel;//上传图片label

@property(nonatomic,strong)UIButton *button;//上传按钮
@property(nonatomic,strong)UIImageView *icon;//
@property(nonatomic,assign)NSInteger tagIndex;
@property(nonatomic,assign)NSMutableArray *iconsArray;//存放读取的图片

@end
@implementation ZHReleaseDiscountTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.tagIndex = 200;
    [self button];
    self.maxY = CGRectGetMaxY(self.button.frame)+20;
    
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(clickReleaseButtonNoticAction:) name:@"DiscountClickReleaseButton" object:nil];
    
    NSNotificationCenter *center2ReadPhotos = [NSNotificationCenter defaultCenter];
    [center2ReadPhotos addObserver:self selector:@selector(changePhotos:) name:@"DiscountHandreadPhoto" object:nil];
    
    NSNotificationCenter *startButtonStr = [NSNotificationCenter defaultCenter];
    [startButtonStr addObserver:self selector:@selector(changePhotos1:) name:@"startButtonStr" object:nil];
    
    NSNotificationCenter *stopButtonStr = [NSNotificationCenter defaultCenter];
    [stopButtonStr addObserver:self selector:@selector(changePhotos2:) name:@"stopButtonStr" object:nil];
    
}


- (void)changePhotos1:(NSNotification *)notification{
    [self.startTimeButton setTitle:notification.userInfo[@"startButton"] forState:UIControlStateNormal];
}
- (void)changePhotos2:(NSNotification *)notification{
    [self.stopTimeButton setTitle:notification.userInfo[@"stopButton"] forState:UIControlStateNormal];
}

- (void)setPhotosArray:(NSMutableArray *)photosArray{
    _photosArray = photosArray;
    self.iconsArray = photosArray;
    [self addButton:photosArray];
}
- (void)changePhotos:(NSNotification *)notification{
    NSArray *iconsArray = notification.userInfo[@"image"];
    self.iconsArray = [NSMutableArray arrayWithArray:iconsArray];
    [self addButton:iconsArray];
}
- (void)addButton:(NSArray *)iconsArray{
    float width = (SCREEN_WIDTH-60)/5.f;
    for (UIView* uiImageView in self.contentView.subviews) {
        if( [uiImageView  isKindOfClass :[UIImageView class]]){
            [uiImageView removeFromSuperview];
        }
    }
    for (int i = 0 ; i < iconsArray.count ; i++) {
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.uploadLabel.frame)+ 10+i/5*(width+10), width, width)];
        self.icon.image = iconsArray[i];
        self.icon.tag = self.tagIndex++;
        self.icon.backgroundColor = [UIColor redColor];
        self.icon.userInteractionEnabled = YES;
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        [self.contentView addSubview:self.icon];
    }
    [self.button removeFromSuperview];
    self.button = nil;
    [self button];
    self.button.frame = CGRectMake(iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.uploadLabel.frame)+ 10+iconsArray.count/5*(width+10), width, width );
    self.maxY = CGRectGetMaxY(self.button.frame)+20;
    
}

- (void)iconAction:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag+self.tagIndex;
    [self.iconsArray removeObjectAtIndex:tag-self.tagIndex];
    
    [self addButton:self.iconsArray];
    
}

- (void)clickReleaseButtonNoticAction:(NSNotification *)notification{
    
    NSString *address = notification.userInfo[@"address"];//地址
    NSString *image = notification.userInfo[@"image"];//图片
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"userId"]];

    //判断信息是否输入完善
    if (self.detailTextField.text.length==0||self.startTimeButton.titleLabel.text.length==0||self.stopTimeButton.titleLabel.text.length==0||self.sortButton.titleLabel.text.length==0||address.length==0||self.phoneTextField1.text.length==0) {
        NSNotification *notic1 = [NSNotification notificationWithName:@"Discount Incomplete information" object:nil];
        [[NSNotificationCenter defaultCenter]postNotification:notic1];
    }else{
        
        NSNotification *notic2 = [NSNotification notificationWithName:@"Discount Complete information" object:nil userInfo:@{@"address":address,@"detail":self.detailTextField.text,@"endTime":[self.stopTimeButton.titleLabel.text changeTime],@"imageSrc":image,@"startTime":[self.startTimeButton.titleLabel.text changeTime],@"userId":userId,@"type":self.sortButton.titleLabel.text,@"phone":self.phoneTextField1.text}];
        [[NSNotificationCenter defaultCenter]postNotification:notic2];
    }
}

/**{"address":"合肥市-蜀山区","discount":{"address":"合肥市-蜀山区","detail":"测试测试","endTime":1469635256437,"imageSrc":"group1/M00/00/03/wKgKe1dyVLqAYr19AAV7btVjJ2Y892.png","phone":"18680683334","startTime":1469635254725,"type":"美容","userId":1},"lat":31.840867,"lng":117.240821,"type":"美容"}*/
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DiscountClickReleaseButton" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DiscountreadPhoto" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"startButtonStr" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"stopButtonStr" object:nil];
}


- (UIButton *)button{
    if (_button==nil) {
        float width = (SCREEN_WIDTH-60)/5.f;
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = [UIColor lightGrayColor];
        _button.frame = CGRectMake(10,CGRectGetMaxY(self.uploadLabel.frame)+10, width, width);
        [_button setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        [self.contentView addSubview:_button];
        [_button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
- (void)buttonAction:(UIButton *)button{
    //读取相册和相机
    if (self.uploadDelegate) {
        [self.uploadDelegate uploadPhotos:button];
    }
}

- (IBAction)startTimeButtonAction:(UIButton *)sender {
    
    NSNotification *notic = [NSNotification notificationWithName:@"DiscountStartButton" object:nil ];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
//    PickerChoiceView *picker = [[PickerChoiceView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2.f)];
//    picker.StartDelegate = self;
//    picker.arrayType = DeteArray;
//    [self.contentView addSubview:picker];
}
- (void)StartPickerSelectorIndixString:(NSString *)str{
    [self.startTimeButton setTitle:str forState:UIControlStateNormal];
}

- (IBAction)stopTimeButtonAction:(UIButton *)sender {
    
    NSNotification *notic = [NSNotification notificationWithName:@"DiscountStopButton" object:nil ];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
//    PickerChoiceView *picker = [[PickerChoiceView alloc]initWithFrame:self.bounds];
//    picker.StopDelegate = self;
//    picker.arrayType = DeteArray;
//    [self.contentView addSubview:picker];
}
- (void)StopPickerSelectorIndixString:(NSString *)str{
    [self.stopTimeButton setTitle:str forState:UIControlStateNormal];
}

- (IBAction)sort1ButtonAction:(UIButton *)sender {
    if (self.changeSortDelegate) {
        [self.changeSortDelegate changeSortButtonText:sender];
    }
}



@end
