//
//  ZHDiscountDetailTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHDiscountDetailTableViewCell.h"
#import "ZHDiscountModel.h"
#import "ZHGetReallyDiscountModel.h"
@interface ZHDiscountDetailTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *icon;//图片
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;//名字
@property (weak, nonatomic) IBOutlet UILabel *stopTimeLabel;//停止时间
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;//具体描述
@property (weak, nonatomic) IBOutlet UILabel *touchShare;

@end
@implementation ZHDiscountDetailTableViewCell

- (void)setModel:(ZHDiscountModel *)model{
    _model = model;
    Discount *discount = model.discount;
    if ([discount.imageSrc rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [discount.imageSrc componentsSeparatedByString:@";"];
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array.firstObject];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,discount.imageSrc];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    self.nameLabel.text = discount.label;
    NSString *timeStr = [NSString stringWithFormat:@"%zd",discount.createTime];
    self.stopTimeLabel.text = [NSString stringWithFormat:@"截止日期:%@",[timeStr timeChange]];

//    NSString *mes = [NSString stringWithFormat:@"经过事实证明，拍手也能治百病。主要是通过长时间固定时的拍手，可以促进人体的血液循环，激发人体的自愈力，改善人体的免疫功能。每天拍手的总量多少、拍手的力道、拍手的频率和拍手的时间都很重要，拍手的心态更是显得十分重要。人的手上有多种反射区，在传统的拍手治百病的思维模式下，经常不断地拍手刺激这些区域，激发人体的自治力和免疫力，能起到预防甚至治愈某些疫病的可能，特别是一些慢性病。但拍手不是万能的，对重病和大病还是要上医院，这是应当肯定的。"];
    NSString *mes = discount.detail;
    
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:17]
                          };
    
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;

    self.detailLabel.text = mes;
    self.detailLabel.frame = CGRectMake(10, CGRectGetMaxY(self.stopTimeLabel.frame)+5, SCREEN_WIDTH-20, labelSize.height);
    self.touchShare.text = @"点击分享到，朋友圈，微博，QQ空间，即可参加该活动。";
    self.maxY = CGRectGetMaxY(self.touchShare.frame) +10 ;
    
}

- (void)setDiscountModel:(ZHGetReallyDiscountModel *)discountModel{
    _discountModel = discountModel;
    if ([discountModel.imageSrc rangeOfString:@";"].location != NSNotFound) {
        NSArray *array = [discountModel.imageSrc componentsSeparatedByString:@";"];
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array.firstObject];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,discountModel.imageSrc];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    self.nameLabel.text = discountModel.label;
    NSString *timeStr = [NSString stringWithFormat:@"%zd",discountModel.createTime];
    self.stopTimeLabel.text = [NSString stringWithFormat:@"截止日期:%@",[timeStr timeChange]];

    NSString *mes = discountModel.detail;
    
    CGSize size = CGSizeMake(SCREEN_WIDTH-20, CGFLOAT_MAX);
    NSDictionary *dic = @{
                          NSFontAttributeName : [UIFont systemFontOfSize:17]
                          };
    
    CGSize labelSize = [mes boundingRectWithSize:size options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    self.detailLabel.text = mes;
    self.detailLabel.frame = CGRectMake(10, CGRectGetMaxY(self.stopTimeLabel.frame)+5, SCREEN_WIDTH-20, labelSize.height);
    self.touchShare.text = @"点击分享到，朋友圈，微博，QQ空间，即可参加该活动。";
    self.maxY = CGRectGetMaxY(self.touchShare.frame) +10 ;
}

@end
