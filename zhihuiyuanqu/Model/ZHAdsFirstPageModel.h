//
//  ZHAdsFirstPageModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHAdsFirstPageModel : ZHBaseModel

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) NSInteger sortId;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, assign) long long endTime;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger effective;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, assign) long long startTime;

@property (nonatomic, assign) NSInteger createTime;

@property (nonatomic, copy) NSString *url;

@end
