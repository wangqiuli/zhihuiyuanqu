//
//  ZHDynamic.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/24.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHDynamic : ZHBaseModel

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, copy) NSString *point;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) NSInteger homeId;

@property (nonatomic, copy) NSString *video;

@end
