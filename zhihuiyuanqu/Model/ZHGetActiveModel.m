//
//  ZHGetActiveModel.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHGetActiveModel.h"

@implementation ZHGetActiveModel
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc]initWithDictionary:@{@"Activity":@"ZHActivity"}];
}
@end

@implementation ZHActivity

@end

