//
//  ZHDevelopmentModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHDevelopmentModel : ZHBaseModel

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *fromtable;

@end
