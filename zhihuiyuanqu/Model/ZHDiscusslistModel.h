//
//  ZHDiscusslistModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/7/1.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHDiscusslistModel : ZHBaseModel

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) NSInteger parentId;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *userName;

@property (nonatomic, copy) NSString *userIcon;

@property (nonatomic, copy) NSString *discuss;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, assign) NSInteger createTime;

@end
