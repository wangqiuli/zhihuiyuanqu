//
//  ZHGetCarModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class ZHParkingshare,ZHUser;
@interface ZHGetCarModel : ZHBaseModel

@property (nonatomic, strong) ZHParkingshare *parkingshare;

@property (nonatomic, strong) ZHUser *user;

@end
@interface ZHParkingshare : JSONModel

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *parkingDetail;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *price;

@property (nonatomic, copy) NSString *home;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;

@end

