//
//  ZHGetDynamic.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/24.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHGetDynamic.h"
#import "ZHDiscusslistModel.h"
@implementation ZHGetDynamic


+ (NSDictionary *)objectClassInArray{
    return @{@"discussList" : [ZHDiscusslistModel class]};
}
@end
