//
//  ZHSearchHomeModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/20.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHSearchHomeModel : ZHBaseModel

@property(nonatomic,strong)NSString *ID;

@property(nonatomic,strong)NSString *name;

@property(nonatomic,strong)NSString *address;

@property(nonatomic,strong)NSString *tel;

@property(nonatomic,strong)NSString *location;

@end
