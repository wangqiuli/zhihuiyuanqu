//
//  ZHUserMessageModel.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/31.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHUserMessageModel.h"

@implementation ZHUserMessageModel

+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc]initWithDictionary:@{@"id":@"ID"}];
}

@end
