//
//  ZHGetReallyDiscountModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHGetReallyDiscountModel : ZHBaseModel


@property (nonatomic, assign) NSInteger id;

@property (nonatomic, assign) NSInteger storeId;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *label;

@property (nonatomic, copy) NSString *imageSrc;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, assign) long long endTime;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *point;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) NSInteger sortId;

@property (nonatomic, assign) long long startTime;


@end
