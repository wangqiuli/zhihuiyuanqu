//
//  ZHAddHomeModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/17.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHAddHomeModel : ZHBaseModel


@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *home;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, copy) NSString *address;


@end
