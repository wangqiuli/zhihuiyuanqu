//
//  ZHRepairModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/23.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHRepairModel : ZHBaseModel


@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *doTime;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger rework;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, assign) NSInteger process;

@property (nonatomic, assign) long long createTime;


@end
