//
//  ZHCarSharesModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/13.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class Parkingshare,ZHUser;
@interface ZHCarSharesModel : ZHBaseModel

@property (nonatomic, copy) NSString *address;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger dis;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;

@property (nonatomic, strong) Parkingshare *parkingshare;

@property (nonatomic, strong) ZHUser *user;


@end
@interface Parkingshare : JSONModel

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *parkingDetail;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *price;

@property (nonatomic, copy) NSString *home;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) long long createTime;

@end

