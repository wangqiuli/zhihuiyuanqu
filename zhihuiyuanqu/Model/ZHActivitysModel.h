//
//  ZHActivitysModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/24.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class Activity,ZHUser;
@interface ZHActivitysModel : ZHBaseModel


@property (nonatomic, strong) Activity *activity;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;

@property (nonatomic, assign) NSInteger dis;

@property (nonatomic, strong) ZHUser *user;


@end
@interface Activity : JSONModel

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, copy) NSString *label;

@property (nonatomic, assign) long long activityTime;

@property (nonatomic, copy) NSString *gathering;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, assign) NSInteger personNums;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *activity;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, copy) NSString *toWhere;

@end

