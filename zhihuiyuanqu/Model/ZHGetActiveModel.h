//
//  ZHGetActiveModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class ZHActivity,ZHUser;
@interface ZHGetActiveModel : ZHBaseModel

@property (nonatomic, strong) ZHActivity *activity;
@property (nonatomic, strong) ZHUser *user;

@end
@interface ZHActivity : JSONModel

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, copy) NSString *label;

@property (nonatomic, assign) long long activityTime;

@property (nonatomic, copy) NSString *gathering;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, assign) NSInteger personNums;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *activity;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, copy) NSString *toWhere;

@end

