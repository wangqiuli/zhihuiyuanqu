//
//  ZHFirstPageAdsModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHFirstPageAdsModel : ZHBaseModel

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *slUrl;//图片

@property (nonatomic, copy) NSString *url;

@property(nonatomic,copy)NSString<Optional> *title;
@end
