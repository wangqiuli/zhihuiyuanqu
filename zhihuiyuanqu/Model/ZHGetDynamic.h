//
//  ZHGetDynamic.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/24.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class ZHDynamic,ZHUser;
@interface ZHGetDynamic : ZHBaseModel

@property (nonatomic, strong) NSArray *discussList;

@property (nonatomic, strong) ZHDynamic *dynamic;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, assign) NSInteger dis;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, strong) ZHUser *user;

@end


