//
//  ZHHomeModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/15.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class Home;
@interface ZHHomeModel : ZHBaseModel


@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *homeName;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, strong) Home *home;

@property (nonatomic, assign) NSInteger dis;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;


@end
@interface Home : JSONModel

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *point;

@property (nonatomic, assign) NSInteger estateId;

@property (nonatomic, copy) NSString *home;

@property (nonatomic, assign) long long createTime;

@end

