//
//  ZHSaveModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHSaveModel : ZHBaseModel


@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *fromtable;

@property (nonatomic, assign) NSInteger lat;

@property (nonatomic, assign) NSInteger lng;

@property (nonatomic, assign) long long createTime;


@end
