//
//  ZHGetDiscountModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/27.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class TransferDis,ZHUser;
@interface ZHGetDiscountModel : ZHBaseModel

@property (nonatomic, strong) TransferDis *transfer;

@property (nonatomic, strong) ZHUser *user;

@end
@interface TransferDis : JSONModel

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *contact;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *price;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger transferType;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, copy) NSString *point;

@property (nonatomic, assign) long long createTime;

@end
