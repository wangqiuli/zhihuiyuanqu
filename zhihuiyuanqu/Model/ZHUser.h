//
//  ZHUser.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/24.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@interface ZHUser : ZHBaseModel
@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, assign) NSInteger freeze;

@property (nonatomic, copy) NSString *work;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, copy) NSString *userName;

@property (nonatomic, copy) NSString *password;

@property (nonatomic, assign) long long birthday;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) long long createTime;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, assign) NSInteger gender;
@end
