//
//  ZHSecondhandModel.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/13.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseModel.h"

@class Transfer1,ZHUser;
@interface ZHSecondhandModel : ZHBaseModel
@property (nonatomic, copy) NSString *address;

@property (nonatomic, strong) ZHUser *user;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger transferType;

@property (nonatomic, assign) NSInteger dis;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;

@property (nonatomic, strong) Transfer1 *transfer;

@property (nonatomic, copy) NSString *type;


@end
@interface Transfer1 : JSONModel

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *detail;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *contact;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *price;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger transferType;

@property (nonatomic, assign) NSInteger hidden;

@property (nonatomic, copy) NSString *point;

@property (nonatomic, assign) long long createTime;

@end

