//
//  ZHPersonMessageTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHUserMessageModel;
@interface ZHPersonMessageTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHUserMessageModel *model;

@end
