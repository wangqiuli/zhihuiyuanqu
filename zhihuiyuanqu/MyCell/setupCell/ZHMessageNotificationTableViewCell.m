//
//  ZHMessageNotificationTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHMessageNotificationTableViewCell.h"

@interface ZHMessageNotificationTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
@implementation ZHMessageNotificationTableViewCell

- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    self.titleLabel.text = titleStr;
}

- (IBAction)switchAction:(UISwitch *)sender {
}


@end
