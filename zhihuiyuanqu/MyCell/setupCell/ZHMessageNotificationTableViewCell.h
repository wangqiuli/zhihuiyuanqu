//
//  ZHMessageNotificationTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/6/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHMessageNotificationTableViewCell : UITableViewCell

@property(nonatomic,strong)NSString *titleStr;

@end
