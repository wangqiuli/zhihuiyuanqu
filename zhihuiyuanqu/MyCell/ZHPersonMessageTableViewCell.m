//
//  ZHPersonMessageTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/5/4.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHPersonMessageTableViewCell.h"
#import "ZHUserMessageModel.h"
#import "UIImageView+WebCache.h"
@interface ZHPersonMessageTableViewCell ()
@property (strong, nonatomic) IBOutlet UIImageView *icon;

@end
@implementation ZHPersonMessageTableViewCell

- (void)setModel:(ZHUserMessageModel *)model{
    _model = model;
    
    if (model.image.length>0) {
        NSString *imageUrl = [PATH_HeaderImageURL stringByAppendingString:model.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }else{
        self.icon.image = [UIImage imageNamed:@"1"];
    }
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(noticAction:) name:@"readPhoto" object:nil];
}
- (void)noticAction:(NSNotification *)text{
    self.icon.image = text.userInfo[@"image"];
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"readPhoto" object:nil];
}
@end
