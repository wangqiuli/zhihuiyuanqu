//
//  AppDelegate.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "AppDelegate.h"
#import "EMSDKFull.h"
#import "GeTuiSdk.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaSSOHandler.h"
#import <SMS_SDK/SMSSDK.h>
#import "AMapLocationKit.h"
//#import "EaseUI.h"
@interface AppDelegate ()<GeTuiSdkDelegate, AMapLocationManagerDelegate>
{
    AMapLocationManager *locationManager;
    UIImageView *imgView;
    
}
@end

@implementation AppDelegate

/// 个推开发者网站中申请App时，注册的AppId、AppKey、AppSecret
#define kGtAppId           @"79GTNd50QjA1UbqsReEYS2"
#define kGtAppKey          @"LCAAsFeuxd6QBRa02dg766"
#define kGtAppSecret       @"6LV5YjXSdp65hhLk6pfvm9"

//短信验证
#define SMS_AppKey @"134e563b3f78c"
#define SMS_APPSecret @"9104aa604d84491695501bdc6ee601b7"

//环信
#define EMSDK_APPKey @"lingyu8908#home"
#define EMSDK_apns @""//推送证书的名字

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    [self.window makeKeyAndVisible];
    
    //添加启动页
//    [self setUpStartUpPicture];
    
    //调用个推
    [self setGeTuiPush];
    
    //调用友盟分享
    [self setUMengShare];
    
    //调用短信验证
    [self setSMSSDK];
    
    //调用定位功能
    [self setAMapLocation];
    
    //调用环信及时通讯
    [self setEMSDK:application with:launchOptions];
    
    return YES;
}

#pragma mark 环信及时通讯
- (void)setEMSDK:(UIApplication *)application with:(NSDictionary *)launchOptions{
    EMOptions *options = [EMOptions optionsWithAppkey:EMSDK_APPKey];
////    options.apnsCertName = EMSDK_apns;
    [[EMClient sharedClient]initializeSDKWithOptions:options];
    
//    [[EaseSDKHelper shareHelper] easemobApplication:application
//                      didFinishLaunchingWithOptions:launchOptions
//                                             appkey:EMSDK_APPKey
//                                       apnsCertName:EMSDK_apns
//                                        otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
}

#pragma mark 启动页
- (void)setUpStartUpPicture{
    
    imgView = [[UIImageView alloc]initWithFrame:self.window.bounds];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.image = [UIImage imageNamed:@"background"];
    [self.window addSubview:imgView];
    [self.window bringSubviewToFront:imgView];
    [self performSelector:@selector(scale_1) withObject:nil afterDelay:2];
    
//    [ZHRequestData serviceUploadFilesWithSubpath:PATH_StartUpURL parameters:nil success:^(id json) {
//        
//        if (json[@"returning"]) {
//            NSArray *array = json[@"returning"];
//            NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,array.firstObject[@"image"]];
//            [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
//        }else{
//            imgView.image = [UIImage imageNamed:@"background"];
//        }
//        [self performSelector:@selector(scale_1) withObject:nil afterDelay:2];
//        
//    } failure:^(NSError *error) {
//        NSLog(@"启动页图片加载错误:%@",error);
//        imgView.image = [UIImage imageNamed:@"background"];
//        [self performSelector:@selector(scale_1) withObject:nil afterDelay:2];
//    }];
    
    
}
-(void)scale_1
{
    [imgView removeFromSuperview];
}

#pragma mark 定位功能
- (void)setAMapLocation{
    //@"d243522a442101b43ddf8d9088676ef3"
    [AMapLocationServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";
    locationManager = [[AMapLocationManager alloc]init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
}
//持续定位的协议方法
- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location{
    NSLog(@"持续定位location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    NSString *lat1 = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *lng1 = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    NSString *lat = [NSString stringWithString:lat1];
    NSString *lng = [NSString stringWithString:lng1];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:lat forKey:@"lat"];
    [defaults setObject:lng forKey:@"lng"];
    [defaults setObject:@"安徽省合肥市蜀山区潜山路399号" forKey:@"address"];
    [defaults synchronize];
   
}

#pragma mark 短信验证
- (void)setSMSSDK{
    //初始化应用，appKey和appSecret从后台申请得
    [SMSSDK registerApp:SMS_AppKey
             withSecret:SMS_APPSecret];
}

#pragma mark 友盟分享
- (void)setUMengShare{
    //5716f028e0f55ae85d00108a
    [UMSocialData setAppKey:@"5716f028e0f55ae85d00108a"];
    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:@"3921700954" secret:@"04b48b094faeb16683c32669824ebdad" RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
   
    [UMSocialQQHandler setQQWithAppId:@"1104908293" appKey:@"MnGtpPN5AiB6MNvj" url:@"http://www.baidu.com/"];
    
    [UMSocialWechatHandler setWXAppId:@"wxd930ea5d5a258f4f" appSecret:@"db426a9829e4b49a0dcac7b4162da6b6" url:@"http://www.umeng.com/social"];
}

#pragma mark 个推
- (void)setGeTuiPush{
    
    // 通过个推平台分配的appId、 appKey 、appSecret 启动SDK，注：该方法需要在主线程中调用
    [GeTuiSdk startSdkWithAppId:kGtAppId appKey:kGtAppKey appSecret:kGtAppSecret delegate:self];
    
    //注册APNS
    [self registerUserNotification];
    
}
- (void)registerUserNotification{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIMutableUserNotificationCategory *actionCategory;
        actionCategory = [[UIMutableUserNotificationCategory alloc] init];
    
        [actionCategory setIdentifier:@"123"];
        [actionCategory setActions:nil
                        forContext:UIUserNotificationActionContextDefault];
        
        NSSet *categories = [NSSet setWithObject:actionCategory];
        UIUserNotificationType types = (UIUserNotificationTypeAlert |
                                        UIUserNotificationTypeSound |
                                        UIUserNotificationTypeBadge);
        
        UIUserNotificationSettings *settings;
        settings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else {
        UIRemoteNotificationType apn_type = (UIRemoteNotificationType)(UIRemoteNotificationTypeAlert |
                                                                       UIRemoteNotificationTypeSound |
                                                                       UIRemoteNotificationTypeBadge);
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:apn_type];
    }
}
/** 远程通知注册成功委托 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken{
    NSString *token = [[deviceToken description]stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@" "];
    NSLog(@"\n>>>[DeviceToken Success]:%@\n\n", token);
    
    //向个推服务器注册deviceToken
    [GeTuiSdk registerDeviceToken:token];
}
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    /// Background Fetch 恢复SDK 运行
    [GeTuiSdk resume];
    completionHandler(UIBackgroundFetchResultNewData);
}
/** SDK启动成功返回cid */
- (void)GeTuiSdkDidRegisterClient:(NSString *)clientId{
    //个推SDK已注册，返回clientId
    NSLog(@"个推成功\n>>>[GeTuiSdk RegisterClient]:%@\n\n", clientId);
}
/** SDK遇到错误回调 */
- (void)GeTuiSdkDidOccurError:(NSError *)error {
    //个推错误报告，集成步骤发生的任何错误都在这里通知，如果集成后，无法正常收到消息，查看这里的通知。
    NSLog(@"个推错误\n>>>[GexinSdk error]:%@\n\n", [error localizedDescription]);
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

//app进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[EMClient sharedClient]applicationDidEnterBackground:application];
}

//app将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[EMClient sharedClient]applicationWillEnterForeground:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
