//
//  ZHInterface.h
//  
//
//  Created by leo on 16/4/21.
//
//

#ifndef ZHInterface_h
#define ZHInterface_h

//http://babywow.wicp.net:16744//webservice/rest/  外网
//http://192.168.10.105:8080/webservice/rest/   内网
#define PATH_HeaderURL @"http://192.168.10.105:8080/webservice/rest/"//数据头
#define PATH_HeaderImageURL @"http://192.168.10.120:8888/"//图片头

#pragma mark 首页
#define PATH_StartUpURL @"advertisementService/getStartPageAdvertisement"//启动页
#define PATH_FirstPage_ADSURL @"advertisementService/getAdvertisement"//广告栏
#define PATH_FirstPage_COMMONURL @""//功能区
#define PATH_FirstPage_tableViewUrl @"http://parent2.chinacloudapp.cn/api/topic/list.json?tid=2&pageSize=%zd"//折扣区

#pragma mark 定位小区
#define PATH_HomeListURL @"homeService/ListByPositon"//获取小区列表
#define PATH_SaveHomeURL @"homeService/saveHomes"//保存常用小区
#define PATH_GetCommonHomeURL @"homeService/getUserCommonHome"//获取常用小区
#define PATH_GetDefaultHomeURL @"homeService/getUserDefaultHome"//获取默认小区
#define PATH_SetDefaultHomeURL @"homeService/setUserDefaultHome"//设置默认小区
#define PATH_ReleaseHomeURL @"homeService/releaseHome"//发布小区

#pragma mark 车位共享
#define PATH_releaseParkingURL @"parkingshareService/releaseParkingshare"//发布车位
#define PATH_HaveCar_URL @"parkingshareService/ListByPositon"//我有车位
#define PATH_LookCar_URL @"parkingshareService/ListByPositon"//我找车位
#define PATH_FindCarByURL @"parkingshareService/findById"//获取活动

#pragma mark 折扣
#define PATH_Discount_URL @"discountService/ListByPositon"//获取折扣列表
#define PATH_GetDiscountURL @"discountService/findById"//获取折扣
#define PATH_ReleaseDiscountURL @"discountService/releaseDiscount"//发布折扣

#pragma mark 活动
#define PATH_Active_URL @"activityService/ListByPositon"//活动
#define PATH_GetActiveURL @"activityService/findById"//获取活动
#define PATH_ReleaseActiveURL @"activityService/releaseActivity"//发布活动
#define PATH_GetUserWithActiveURL @"applyService/getUserList"//获取申请参加活动所有人员列表接口
#define PATH_wantJoinActiveURL @"applyService/saveApply"// 用户报名接口

#pragma mark 邻里圈
#define PATH_Neighbors_URL @"http://parent2.chinacloudapp.cn/api/topic/reply.json?uid=5134&id=1109"//邻里圈
#define PATH_ReleaseDiscussURL @"discussService/releaseDiscuss"// 发布评论

#pragma mark 二手置换
#define PATH_releaseSecondHandURL @"transferService/releaseTransfer"//发布二手置换
#define PATH_SecondHand_Buy_URL @"transferService/ListByPositon"//求购
#define PATH_SecondHand_Sell_URL @"transferService/ListByPositon"//转让
#define PATH_FindByIdURL @"transferService/findById"//获取折扣

#pragma mark 物业维修
#define PATH_ReleaseRepairURL @"repairService/releaseRepair"//发布物业维修
#define PATH_GetRepairURL @"repairService/ListByPositon"//获取物业维修列表
#define APTH_updateRepairURL @"repairService/updateRepair"// 更新物业维修

#pragma mark 随手拍
#define PATH_ReadilyHand_URL @""//随手拍
#define PATH_GetDynamicURL @"dynamicService/ListByPositon"//获取动态列表
#define PATH_GetDetailDynamicURL @"dynamicService/findById"//获取具体的活动

#pragma mark 收藏
#define PATH_SaveURL @"collectionService/saveCollection"//用户收藏接口
#define PATH_GetSaveURL @"userservice/getUserCollection"//获取用户收藏的所有信息
#define PATH_GetUserReleaseURL @"userservice/getUserRelease"//获取用户发布的所有信息
#define PATH_CancelSaveURL @"collectionService/cancelCollection"//取消收藏

#pragma mark 我的
#define PATH_MyMessageURL @"http://parent2.chinacloudapp.cn/api/topic/reply.json?uid=5134&id=1109"
#define PATH_RegistURL @"userservice/userReg"//注册接口
#define PATH_LoginURL @"userservice/userlogin"//登录接口
#define PATH_TelNumTestURL @"userservice/isTelNumRegisted"//手机号码检测
#define PATH_UserMessage @"userservice/getUserByToken"//获取用户信息接口
#define PATH_ChangeUserPasswordURL @"userservice/changeUserPassword"//在用户登录的情况下修改用户密码
#define PATH_changeUserPasswordNoLoginURL @"userservice/changeUserPasswordNoLogin"//在用户没有登录的情况下修改用户密码
#define PATH_changeUserMessageURL @"userservice/changeUserInfo"//修改用户信息
#define PATH_uploadFileURL @"fileService/uploadFile"//上传文件

#endif /* ZHInterface_h */
