//
//  ZHHeader.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#ifndef ZHHeader_h
#define ZHHeader_h

#define SCREEN_WIDTH    [UIScreen mainScreen].bounds.size.width //屏幕宽
#define SCREEN_HEIGHT    [UIScreen mainScreen].bounds.size.height //屏幕高

#define UICOLOR_RGB [UIColor colorWithRed:239/255.f green:239/255.f blue:244/255.f alpha:1]//接近透明色

#endif /* ZHHeader_h */
