//
//  ZHBaseNavViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseNavViewController.h"

@interface ZHBaseNavViewController ()

@end

@implementation ZHBaseNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpNavgation];
}
- (void)setUpNavgation{
    
    self.navigationBar.translucent = NO;
    
    //导航栏背景颜色
    self.navigationBar.barTintColor = [UIColor lightGrayColor];

    [self.navigationBar setTitleTextAttributes:
          @{NSFontAttributeName:[UIFont systemFontOfSize:17],
            NSForegroundColorAttributeName:[UIColor whiteColor]}];
         self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
     
    //两侧按钮颜色
    self.navigationBar.tintColor = [UIColor whiteColor];
}


@end
