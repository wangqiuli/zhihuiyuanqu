/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "EaseConversationModel.h"

#import "EMConversation.h"

@implementation EaseConversationModel

- (instancetype)initWithConversation:(EMConversation *)conversation
{
    self = [super init];
    if (self) {
        _conversation = conversation;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *title = [defaults objectForKey:[NSString stringWithFormat:@"userId%@",_conversation.conversationId]];
        if (title.length<=0) {
            _title = _conversation.conversationId;
        }else{
            _title = title;
        }
        
        if (conversation.type == EMConversationTypeChat) {
            
            _avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];
        }
        else{
            _avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/group"];
        }
        
        

    }
    
    return self;
}

@end
