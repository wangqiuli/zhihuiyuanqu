//
//  ZHNeighborViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNeighborViewController.h"
#import "ZHNeighborTableViewCell.h"

#import "ZHGetDynamic.h"
#import "ZHDynamic.h"
#import "ZHDiscusslistModel.h"
#import "KeyBoardView.h"
//#import "UIButton+buttonName.h"
@interface ZHNeighborViewController ()<UITableViewDelegate,UITableViewDataSource,commentButtonClickDelegate,sendMessageDelegate>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
//    KeyBoardView *_board;
}
@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@property(nonatomic,strong)KeyBoardView *board;//键盘
@end

@implementation ZHNeighborViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lastId = 0;
    ret = YES;
    [self requestData];
    [self.view addSubview:self.tableView];

}


#pragma mark cell的协议方法
- (void)commentButtonClickDelegateAction{

    if (self.board.isClick==YES) {//键盘是展开的
        [self.board keyboardResignFirstResponder];
    }else{
        [self.board createTheBGView:self.view];
    }
}

- (KeyBoardView *)board{
    if (_board==nil) {
        _board = [[KeyBoardView alloc]init];
        _board.delegate = self;
    }
    return _board;
}

#pragma mark-------------sendMessageDelegate----------------
//获取发送消息的代理方法
- (void)getTheMessage:(NSString *)text
{
    NSLog(@"回复的内容:%@",text);
//    [_dataArr addObject:[text dealTheMessage]];
//    NSArray *arr = @[[NSIndexPath indexPathForRow:_dataArr.count-1 inSection:0]];
//    [_tableView insertRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationNone];
}
- (void)theKeyBoardWillShowWithKeyBoardHeight:(CGFloat)boardHeight
{
    self.tableView.frame = CGRectMake(0, 0, SCREENWIDTH, boardHeight);
}
- (void)theKeyBoardDidHiden
{
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40);
}


#pragma mark 请求数据
- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    if (token==nil) {
        return;
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"type":@"1002",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetDynamicURL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"邻里圈获取动态列表错误:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHGetDynamic *model = [[ZHGetDynamic alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        _lastId = 0;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId + 10;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
    
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNeighborTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHNeighborTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHGetDynamic *model = self.dataSource[indexPath.row];
    cell.model = model;
    cell.delegate = self;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNeighborTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHGetDynamic *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell.maxY;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //关闭键盘的通知
    NSNotification *notic = [NSNotification notificationWithName:@"releaseKeyBoard" object:nil];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
}

#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHNeighborTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        [self addRefreshControl];
        
    }
    return _tableView;
}
#pragma mark 数据源懒加载
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}


@end
