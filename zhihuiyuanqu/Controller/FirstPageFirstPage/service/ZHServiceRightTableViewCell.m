//
//  ZHServiceRightTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/26.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHServiceRightTableViewCell.h"
#import "ZHFirstPageFirstPageCommonView.h"
@interface ZHServiceRightTableViewCell ()

@property(nonatomic,strong)ZHFirstPageFirstPageCommonView *commonView;

@end
@implementation ZHServiceRightTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    
    // 在添加视图前, 先移除原来的视图
    for (UIView *subView in self.contentView.subviews) {
        
        [subView removeFromSuperview];
        
    }
    
    float width = (SCREEN_WIDTH-SCREEN_WIDTH/5.f-20)/3.f-10;
    for (int i=0; i<5; i++) {
        
        self.commonView = [[ZHFirstPageFirstPageCommonView alloc]initWithFrame:CGRectMake(15+(width+10)*(i%3), (width+10)*(i/3), width, width)];
        self.commonView.icon.image = [UIImage imageNamed:@"background"];
        self.commonView.label.text = @"查天气";
        self.commonView.tag = 1000 + i;
        [self.commonView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(commonViewAction:)]];
        [self.contentView addSubview:self.commonView];
        
    }

    self.maxY = CGRectGetMaxY(self.commonView.frame)+10;

}

- (void)commonViewAction:(UITapGestureRecognizer *)tap{
    self.commonView = [self.commonView viewWithTag:self.commonView.tag];

    if (self.setCommonViewActionBlock) {
        self.setCommonViewActionBlock(tap.view.tag - 1000);
    }
}
@end
