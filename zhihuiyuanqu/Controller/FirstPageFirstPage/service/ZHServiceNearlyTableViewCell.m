//
//  ZHServiceNearlyTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHServiceNearlyTableViewCell.h"

@interface ZHServiceNearlyTableViewCell ()
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *starLabel;
@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyLabel;
@property (strong, nonatomic) IBOutlet UIButton *cellPhoneButton;


@end
@implementation ZHServiceNearlyTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    
    self.maxY  = CGRectGetMaxY(self.phoneLabel.frame)+10;
}

- (IBAction)cellPhoneButtonAction:(id)sender {
    
}



@end
