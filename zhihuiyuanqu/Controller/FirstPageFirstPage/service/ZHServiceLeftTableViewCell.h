//
//  ZHServiceLeftTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/26.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;
@interface ZHServiceLeftTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *blueLabel;

@property(nonatomic,strong)ZHFirstPageAdsModel *model;

- (void)setSelected:(BOOL)selected tag:(NSIndexPath*)tag;
@end
