//
//  ZHServiceLeftTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/26.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHServiceLeftTableViewCell.h"

@implementation ZHServiceLeftTableViewCell

- (void)setModel:(ZHFirstPageAdsModel *)model{
    _model = model;
    self.titleLabel.text = @"汽车服务";
}

- (void)setSelected:(BOOL)selected tag:(NSIndexPath *)tag{
    if (selected && (tag == 0)) {
        self.titleLabel.textColor = [UIColor blueColor];
        self.blueLabel.backgroundColor = [UIColor blueColor];
        return;
    }
    else if (selected){
        self.titleLabel.textColor = [UIColor blueColor];
        self.blueLabel.backgroundColor = [UIColor blueColor];
        return;
    }
    self.titleLabel.textColor = [UIColor darkGrayColor];
    self.blueLabel.backgroundColor = [UIColor whiteColor];
    
}

@end
