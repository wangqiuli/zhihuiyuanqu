//
//  ZHServiceRightTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/26.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHFirstPageAdsModel;
@interface ZHServiceRightTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHFirstPageAdsModel *model;

@property(nonatomic,assign)CGFloat maxY;

@property(nonatomic,copy)void(^setCommonViewActionBlock)(NSInteger);
@end
