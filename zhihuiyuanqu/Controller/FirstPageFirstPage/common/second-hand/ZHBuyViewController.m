//
//  ZHBuyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/13.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBuyViewController.h"
#import "ZHSellTableViewCell.h"
#import "ZHSecondhandModel.h"
#import "ZHGetDiscountViewController.h"
@interface ZHBuyViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
    NSString *sort;//记录选中的是什么类型
}
@property (weak, nonatomic) IBOutlet UIButton *sortButton;//分类按钮
@property (weak, nonatomic) IBOutlet UIButton *nearbyButton;//附近按钮
@property(nonatomic,strong)UIScrollView *sortScrollView;//分类视图
@property(nonatomic,strong)UIScrollView *nearbyScrollView;//附近视图

@property(nonatomic,strong)NSArray *sortSourcedata;//分类数据源
@property(nonatomic,strong)NSArray *nearbySourcedata;//附近数据源

@property (nonatomic, strong) UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;

@property(nonatomic,strong)MJRefreshNormalHeader *header;
@property(nonatomic,strong)MJRefreshAutoNormalFooter *footer;

@end

@implementation ZHBuyViewController

#define button_Tag 100

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lastId = 0;
    ret = YES;
    sort = @"生活用品";
    [self requestData:sort];
    [self.view addSubview:self.tableView];
    [self changeButtonColor];
}

- (void)changeButtonColor{
    self.sortButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.nearbyButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark 请求数据
- (void)requestData:(NSString *)type{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat1 = [defaults objectForKey:@"lat"];
    NSString *lat = [NSString stringWithString:lat1];
    NSString *lng = [defaults objectForKey:@"lng"];
//    NSString *address = [defaults objectForKey:@"address"];

    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    if (token==nil||lat==nil||lng==nil) {
        return;
    }
    
    if (ret == YES) {
        lastId2 = @"0";
    }
    
    NSDictionary *parameters = @{@"token":token,
                                 @"tranferType":@"1001",
                                 @"type":type,
                                 @"address":@"",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SecondHand_Buy_URL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"二手置换求购获取数据失败:%@",error);
        [self stopRefresh];
    }];
    
}
- (void)tableViewData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
         ZHSecondhandModel *model = [[ZHSecondhandModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    self.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData:sort];
    }];
    self.tableView.mj_header = self.header;
    
    //2.上拉加载
    self.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId + 10;
        [self requestData:sort];
    }];
    self.tableView.mj_footer = self.footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHSellTableViewCell" owner:self options:nil].lastObject;
    }
    ZHSecondhandModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHSecondhandModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell.maxY+10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSecondhandModel *model = self.dataSource[indexPath.row];
    ZHGetDiscountViewController *detail = [[ZHGetDiscountViewController alloc]init];
    detail.id = model.transfer.ID;
    detail.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detail animated:YES];
}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH, SCREEN_HEIGHT-64-64) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHSellTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        [self addRefreshControl];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
        
    }
    return _dataSource;
}

#pragma mark 点击三种选项
- (IBAction)sortButtonAction:(UIButton *)sender {
    //判断classScrollView是否存在
    if (self.nearbyScrollView) {
        [self.nearbyScrollView removeFromSuperview];
        self.nearbyScrollView =nil;
        
    }
    if (self.sortScrollView) {
        [self.sortScrollView removeFromSuperview];
        self.sortScrollView =nil;
        
    }else{
        self.sortScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH/2.f, 0)];
        self.sortScrollView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.sortScrollView];
        [UIView animateWithDuration:0.2 animations:^{
            self.sortScrollView.frame = CGRectMake(0, 40, SCREEN_WIDTH/2.f, SCREEN_WIDTH/2.f);
        }];
        for (int i=0; i<self.sortSourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,40*i, SCREEN_WIDTH/2.f, 40);
            [button setTitle:self.sortSourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,5, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:17];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(sortButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.sortScrollView addSubview:button];
            // 设置contentSize
            self.sortScrollView.contentSize = CGSizeMake(SCREEN_WIDTH/2.f,CGRectGetMaxY(button.frame));
        }
    }
    
}
- (void)sortButAction:(UIButton *)button{
    [self.sortButton setTitle:self.sortSourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.sortButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.sortButton.contentEdgeInsets = UIEdgeInsetsMake(6,5, 6, 0);
    [self.sortScrollView removeFromSuperview];
    self.sortScrollView =nil;
    sort = button.titleLabel.text;
    [self requestData:sort];
}
- (IBAction)nearbyButtonAction:(UIButton *)sender {
    
    //判断classScrollView是否存在
    if (self.sortScrollView) {
        [self.sortScrollView removeFromSuperview];
        self.sortScrollView =nil;
        
    }
    if (self.nearbyScrollView) {
        [self.nearbyScrollView removeFromSuperview];
        self.nearbyScrollView =nil;
        
    }else{
        self.nearbyScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2.f, 40, SCREEN_WIDTH/2.f, 0)];
        self.nearbyScrollView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.nearbyScrollView];
        [UIView animateWithDuration:0.2 animations:^{
            self.nearbyScrollView.frame = CGRectMake(SCREEN_WIDTH/2.f, 40, SCREEN_WIDTH/2.f, SCREEN_WIDTH/2.f);
        }];
        for (int i=0; i<self.nearbySourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,40*i, SCREEN_WIDTH/2.f, 40);
            [button setTitle:self.nearbySourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,5, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:17];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(nearbyButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.nearbyScrollView addSubview:button];
            // 设置contentSize
            self.nearbyScrollView.contentSize = CGSizeMake(SCREEN_WIDTH/2.f,CGRectGetMaxY(button.frame));
        }
    }
    
}
- (void)nearbyButAction:(UIButton *)button{
    [self.nearbyButton setTitle:self.nearbySourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.nearbyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.nearbyButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.nearbyScrollView removeFromSuperview];
    self.nearbyScrollView =nil;
    sort = button.titleLabel.text;
    [self requestData:sort];
}
#pragma mark 数据源的懒加载
- (NSArray *)sortSourcedata{
    if (_sortSourcedata==nil) {
        _sortSourcedata = @[@"美食",@"生活用品",@"宠物",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    }
    return _sortSourcedata;
}
- (NSArray *)nearbySourcedata{
    if (_nearbySourcedata==nil) {
        _nearbySourcedata = @[@"生活用品",@"",@"宠物",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    }
    return _nearbySourcedata;
}


@end
