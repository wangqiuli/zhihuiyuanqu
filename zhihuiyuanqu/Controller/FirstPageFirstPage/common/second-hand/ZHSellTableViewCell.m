//
//  ZHSellTableViewCell.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSellTableViewCell.h"
#import "ZHSecondhandModel.h"
#import "UIImageView+WebCache.h"
@interface ZHSellTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end
@implementation ZHSellTableViewCell

- (void)setModel:(ZHSecondhandModel *)model{
    _model = model;
    Transfer1 *transfer = model.transfer;
    self.titleLabel.text = transfer.title;
    self.placeLabel.text = model.address;
    NSString *timeStr = [NSString stringWithFormat:@"%zd",transfer.createTime];

    self.moneyLabel.text = [NSString stringWithFormat:@"置换/价格:%@",transfer.price];
    
    if (transfer.image.length==0||(transfer.image == nil)) {
        self.icon.image = [UIImage imageNamed:@"1"];
    }else{
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@",PATH_HeaderImageURL,transfer.image];
        [self.icon sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    self.timeLabel.text = [timeStr timeChange];
    self.maxY = CGRectGetMaxY(self.timeLabel.frame);
}

@end
