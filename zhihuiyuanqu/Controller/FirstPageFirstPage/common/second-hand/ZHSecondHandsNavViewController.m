//
//  ZHSecondHandsNavViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/18.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHSecondHandsNavViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ZHSecondHandsNavTableViewCell.h"
#import "ZHSecondhandModel.h"
@interface ZHSecondHandsNavViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,uploadPhotosDelegate,changeSortButtonTextDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;//数据源
@property(nonatomic,strong)NSMutableArray *iconsArray;//存储读取的相片
@property(nonatomic,strong)NSMutableDictionary *iconsDict;//存储上传后的图片
@property(nonatomic,strong)UIButton *releaseButton;//发布按钮
@property(nonatomic,strong)NSMutableDictionary *dictData;//存储所有的字典
@end

@implementation ZHSecondHandsNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.releaseButton];
    [self.view bringSubviewToFront:self.releaseButton];
    
    [self setNotification];
    [self setUpNavigationItem];
}

#pragma mark cell的协议方法
- (void)uploadPhotos:(UIButton *)button{
    //读取相册和相机
    [self editImageSelect];
}
- (void)changeSortButtonText:(UIButton *)button{
    //改变button的text
    [self changeButtonText:button];
}
- (void)changeButtonText:(UIButton *)button{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选择分类" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action0 = [UIAlertAction actionWithTitle:@"智能数码" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [button setTitle:@"智能数码" forState:UIControlStateNormal];
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"时尚家居" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [button setTitle:@"时尚家居" forState:UIControlStateNormal];
        
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"生活用品" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [button setTitle:@"生活用品" forState:UIControlStateNormal];
        
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:action0];
    [alertController addAction:action1];
    [alertController addAction:action2];
    [alertController addAction:action3];
    
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark 读取相机和相册
- (void)editImageSelect{
    UIAlertController *alertController = [[UIAlertController alloc]init];
    
    UIAlertAction *action0 = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = YES;
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有摄像头"];
            
        }
        
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.allowsEditing = YES;
            controller.delegate = self;
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有相册"];
            
        }
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:action0];
    [alertController addAction:action1];
    [alertController addAction:action2];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        if ([picker allowsEditing]) {
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerEditedImage]];
            
        }else{
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerOriginalImage]];
            
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    //修改界面上的图片
    [self changeUserImage];
    [self.tableView reloadData];
}
- (void)changeUserImage{
    
    NSNotification *notic = [NSNotification notificationWithName:@"SecondHandreadPhoto" object:nil userInfo:@{@"image":self.iconsArray}];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
    //将图片存到本地
    [self saveImage:self.iconsArray.firstObject];
}
- (void)saveImage:(UIImage *)image{
    NSData* imageData = UIImageJPEGRepresentation(image, 0.5);
    NSDictionary *fileParameters = @{@"image":imageData,
                                     @"fileKey":@"file",
                                     @"nameKey":@"file",
                                     @"mimeType":@"image/jpeg"};
    
    [ZHRequestData uploadFilesWithSubpath:PATH_uploadFileURL parameters:nil fileArray:@[fileParameters] success:^(id json) {
        NSLog(@"二手置换发布图片上传成功%@",json);
        [self.iconsDict setObject:json[@"returning"] forKey:@"image"];
        
    } failure:^(NSError *error) {
        NSLog(@"二手置换发布图片上传失败:%@",error);
    }];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"发布交易信息";
    self.tabBarController.tabBar.hidden = YES;
    
}

#pragma mark 设置键盘弹起和收缩
- (void)setNotification{
    // 设置键盘监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //接受信息填写不完整的通知
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(center1NoticAction) name:@"SecondHand Incomplete information" object:nil];
    
    //接受信息填写完整的通知
    NSNotificationCenter *center2 = [NSNotificationCenter defaultCenter];
    [center2 addObserver:self selector:@selector(center2NoticAction:) name:@"SecondHand Complete information" object:nil];
}
- (void)didKeyboardShow:(NSNotification *)noti
{
    CGRect frame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.view.frame.size.height - frame.size.height +40);
    }];
}

- (void)didKeyboardHide:(NSNotification *)noti
{
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40);
    }];
}

#pragma mark 发布按钮
- (UIButton *)releaseButton{
    if (_releaseButton==nil) {
        _releaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _releaseButton.frame = CGRectMake(0, SCREEN_HEIGHT-104, SCREEN_WIDTH, 40);
        _releaseButton.backgroundColor = [UIColor lightGrayColor];
        _releaseButton.layer.borderWidth = 1;
        [_releaseButton setTitle:@"确认发布" forState:UIControlStateNormal];
        [_releaseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_releaseButton addTarget:self action:@selector(releaseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _releaseButton;
}
- (void)releaseButtonAction:(UIButton *)button{
    
    if (self.iconsDict[@"image"]==nil) {
        [self showAlert:@"提示" message:@"请上传图片" click:^(BOOL ret) {
            
        }];
    }else{
        
        //点击确认发布按钮的通知(传送当前地址)
        NSNotification *notic = [NSNotification notificationWithName:@"SecondHandClickReleaseButton" object:nil userInfo:@{@"address":self.dictData[@"address"],@"image":self.iconsDict[@"image"]}];
        [[NSNotificationCenter defaultCenter]postNotification:notic];
    }
}
- (void)center1NoticAction{
    [self showHint:@"信息填写不完整"];
}
- (void)center2NoticAction:(NSNotification *)notification{
    NSDictionary *dict = notification.userInfo;
    NSString *type = dict[@"type"];
    
    [self.dictData setValue:dict forKey:@"transfer"];
    [self.dictData setValue:type forKey:@"type"];
    [self.dictData setValue:dict[@"transferType"] forKey:@"transferType"];
    
    //请求数据
    [self requestData:self.dictData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SecondHand Incomplete information" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SecondHand Complete information" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark 请求数据
- (void)requestData:(NSDictionary *)dict{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    [dict setValue:lat forKey:@"lat"];
    [dict setValue:lng forKey:@"lng"];
    
//    NSError *parseError = nil;
//    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
//    NSString *jsonValue = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *parameters = @{@"token":token,@"jsonStr":[dict jsonData]};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_releaseSecondHandURL parameters:parameters success:^(id json) {
        NSLog(@"二手置换成功：%@",json[@"message"]);
        
    } failure:^(NSError *error) {
        NSLog(@"二手置换失败：%@",error);
    }];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHSecondHandsNavTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHSecondHandsNavTableViewCell"];
    if (self.iconsArray.count>0) {
        cell.photosArray = self.iconsArray;
        return cell.maxY;
    }
    ZHSecondhandModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHSecondHandsNavTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHSecondHandsNavTableViewCell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHSecondHandsNavTableViewCell" owner:self options:nil].lastObject;
    }
    cell.uploadDelegate = self;
    cell.changeSortDelegate = self;
    ZHSecondhandModel *model = self.dataSource.firstObject;
    if (self.iconsArray!=nil) {
        cell.photosArray = self.iconsArray;
    }
    cell.model = model;
    return cell;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //滑动tableView收缩键盘
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHSecondHandsNavTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZHSecondHandsNavTableViewCell"];
    }
    return _tableView;
}

- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
        
    }
    return _dataSource;
}
- (NSMutableArray *)iconsArray{
    if (_iconsArray==nil) {
        _iconsArray = [NSMutableArray array];
    }
    return _iconsArray;
}
- (NSMutableDictionary *)iconsDict{
    if (_iconsDict == nil) {
        _iconsDict = [NSMutableDictionary dictionary];
    }
    return _iconsDict;
}
- (NSMutableDictionary *)dictData{
    if (_dictData==nil) {
        _dictData = [NSMutableDictionary dictionary];
    }
    return _dictData;
}
@end
