//
//  ZHSellTableViewCell.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/14.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZHSecondhandModel;
@interface ZHSellTableViewCell : UITableViewCell

@property(nonatomic,strong)ZHSecondhandModel *model;
@property(nonatomic,assign)CGFloat maxY;
@end
