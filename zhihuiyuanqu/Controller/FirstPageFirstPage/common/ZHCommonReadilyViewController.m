//
//  ZHCommonReadilyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCommonReadilyViewController.h"

#import "ZHCommonReadiyTableViewCell.h"
#import "ZHGetDynamic.h"
#import "ZHDynamic.h"
@interface ZHCommonReadilyViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
}
@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHCommonReadilyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lastId = 0;
    ret = YES;
    [self setUpNavigationItem];
    [self requestData];
    [self.view addSubview:self.tableView];
}

#pragma mark 请求数据
- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    if (token==nil) {
        [self showHint:@"请先登录"];
        return;
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"type":@"1001",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetDynamicURL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"随手拍获取动态列表错误:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHGetDynamic *model = [[ZHGetDynamic alloc]initWithDictionary:info error:nil];
        
        NSDictionary *infodynamic = info[@"dynamic"];
        
        ZHDynamic *dynamic = [[ZHDynamic alloc]initWithDictionary:infodynamic error:nil];
        
        model.dynamic = dynamic;
        
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        _lastId = 0;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId + 10;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
    
}

- (void)setUpNavigationItem{

    self.title = @"随手拍";
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCommonReadiyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHGetDynamic *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCommonReadiyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHCommonReadiyTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHGetDynamic *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell;
}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHCommonReadiyTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
        
    }
    return _dataSource;
}
- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
@end
