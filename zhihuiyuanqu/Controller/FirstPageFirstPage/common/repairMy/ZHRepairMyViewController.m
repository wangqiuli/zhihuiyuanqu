//
//  ZHRepairMyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/29.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHRepairMyViewController.h"
#import "ZHRepairMyTableViewCell.h"
#import "ZHRepairModel.h"
@interface ZHRepairMyViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
}
@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHRepairMyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lastId = 1;
    ret = YES;
    [self setUpNavgation];
    [self requestData];
    [self.view addSubview:self.tableView];
}

#pragma mark 请求数据
- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    NSDictionary *parameters = @{@"token":token,
                                 @"nowPage":lastId2};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetRepairURL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"获取物业维修列表数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{

    NSArray *allData = data[@"returning"][@"list"];
    for (NSDictionary *info in allData) {
        ZHRepairModel *model = [[ZHRepairModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        _lastId = 1;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId ++;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHRepairMyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHRepairModel *model = self.dataSource[indexPath.section];
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHRepairMyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHRepairModel *model = self.dataSource[indexPath.section];
    cell.model = model;
    return cell.maxY;
}
#pragma mark 删除功能
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *userId = [NSString stringWithFormat:@"%@",[defaults objectForKey:@"userId"]];
    ZHRepairModel *model = self.dataSource[indexPath.section];
    
    NSDictionary *dict = @{@"type":model.type,@"detail":model.detail,@"doTime":model.doTime,@"image":model.image,@"phone":model.phone,@"id":[NSString stringWithFormat:@"%zd",model.id],@"createTime":[NSString stringWithFormat:@"%zd",model.createTime],@"process":@"3",@"rework":[NSString stringWithFormat:@"%zd",model.rework],@"hidden":[NSString stringWithFormat:@"%zd",model.hidden],@"userId":userId};
    NSDictionary *parameters = @{@"token":token,@"jsonStr":[dict jsonData]};
    [ZHRequestData serviceUploadFilesWithSubpath:APTH_updateRepairURL parameters:parameters success:^(id json) {

        [self.dataSource removeObjectAtIndex:indexPath.section];
        [self.tableView reloadData];

        } failure:^(NSError *error) {
        NSLog(@"取消收藏错误:%@",error);
    }];
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addRefreshControl];
        [_tableView registerClass:[ZHRepairMyTableViewCell class] forCellReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

#pragma mark 导航栏
- (void)setUpNavgation{
    self.title = @"我的报修";
    self.tabBarController.tabBar.hidden = YES;
}

//- (void)viewWillDisappear:(BOOL)animated{
//    self.tabBarController.tabBar.hidden = NO;
//}
@end
