//
//  ZHCarShareViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCarShareViewController.h"

#import "ZHHaveCarViewController.h"
#import "ZHLookCarViewController.h"
#import "ZHCarDetailViewController.h"
#import "TitleLebal.h"
@interface ZHCarShareViewController ()<UIScrollViewDelegate>

@property(nonatomic,strong)UIScrollView *titleScrollView;
@property(nonatomic,strong)UIScrollView *contentScrollView;
@property(nonatomic,strong)NSArray *titles;
@property (nonatomic, strong) UIView *topBackView;

@end

@implementation ZHCarShareViewController

#define number 2 //头上有多少label

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNavigationItem];
    [self initScorllView];
    [self addChildViewController];
    [self scrollViewDidEndScrollingAnimation:self.contentScrollView];
    [self scrollViewDidScroll:self.contentScrollView];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"车位共享";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"tianjia"] style:UIBarButtonItemStyleDone target:self action:@selector(rightItemAction)];
    
    self.tabBarController.tabBar.hidden = YES;
}
- (void)rightItemAction{
    
    ZHCarDetailViewController *carDetail = [[ZHCarDetailViewController alloc]init];
    
    [self.navigationController pushViewController:carDetail animated:YES];
}

#pragma mark 添加子视图控制器
- (void)addChildViewController{
    ZHHaveCarViewController *haveCar = [[ZHHaveCarViewController alloc]init];
    [self addChildViewController:haveCar];
    
    ZHLookCarViewController *lookCar = [[ZHLookCarViewController alloc]init];
    [self addChildViewController:lookCar];
    
}

#pragma mark UIScrollViewDelegate
//人为的拖拽 会调用
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

#pragma mark -- 核心代码
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    
    //取出需要显示的控制器
    //拿到索引
    NSInteger  index =  scrollView.contentOffset.x/scrollView.frame.size.width;
    UIViewController *willShow =  self.childViewControllers[index];
    
    willShow.view.frame = CGRectMake(scrollView.contentOffset.x, 0, scrollView.frame.size.width, scrollView.frame.size.height);
    
    [self.contentScrollView addSubview:willShow.view];
}

//实时监控scrollView 的滚动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    CGFloat scale = scrollView.contentOffset.x/scrollView.frame.size.width;
    //获得需要操作的索引
    NSInteger  leftIndex =  scale;
    
    NSInteger  max = self.contentScrollView.contentSize.width/self.contentScrollView.frame.size.width;
    
    if (leftIndex == max -1) return;
    NSInteger rightIndex = leftIndex +1;
    //黑色 0 0 0
    //红色 1 0 0
    //取出lebal
    
    TitleLebal *leftLbl = self.titleScrollView.subviews[leftIndex];
    TitleLebal *rightLbl = self.titleScrollView.subviews[rightIndex];
    
    CGFloat rightScale = scale - leftIndex;
    
    CGFloat leftScale  = 1 - rightScale;
    
    //字体颜色
    //字体大小
    leftLbl.scale = leftScale;
    rightLbl.scale = rightScale;
    
    CGFloat lelW = self.titleScrollView.subviews[0].frame.size.width;
    
    CGFloat jishu = scrollView.contentOffset.x/scrollView.frame.size.width;
    
    CGPoint cent = CGPointMake(lelW/2, _topBackView.center.y);
    cent.x =  cent.x + jishu * lelW < SCREEN_WIDTH/number ? SCREEN_WIDTH/2/number:cent.x + jishu * lelW;
    
    _topBackView.center = cent;
    
}

#pragma mark 懒加载
#pragma mark 懒加载
-(void)initScorllView
{
    _titleScrollView = [[UIScrollView alloc]init];
    _titleScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    _titleScrollView.showsHorizontalScrollIndicator = NO;
    _titleScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
    [self.view addSubview:_titleScrollView];
    
    //初始化内容scrollView
    _contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleScrollView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.titleScrollView.frame)-64)];
    _contentScrollView.backgroundColor = UICOLOR_RGB;
    _contentScrollView.contentSize = CGSizeMake(SCREEN_WIDTH*number, 0);
    _contentScrollView.showsHorizontalScrollIndicator = NO;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    _contentScrollView.pagingEnabled = YES;
    _contentScrollView.delegate = self;
    [self.view addSubview:_contentScrollView];
    
    //添加标题
    [self setupTitle];
    
}

-(void)setupTitle
{
    for (int i = 0; i< number; i++) {
        self.titles = @[@"我有车位",@"我找车位"];
        TitleLebal *label = [[TitleLebal alloc]init];
        
        label.frame = CGRectMake(SCREEN_WIDTH/number*i, 0, SCREEN_WIDTH/number, 40);
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.titles[i];
        [label addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelCick:)]];
        label.userInteractionEnabled = YES;
        [self.titleScrollView addSubview:label];
    }
    
    self.topBackView = [[UIView alloc]initWithFrame:CGRectMake(0, 38, SCREEN_WIDTH/number, 2)];
    self.topBackView.backgroundColor = [UIColor redColor];
    [self.titleScrollView addSubview:self.topBackView];
}
-(void)labelCick:(UITapGestureRecognizer *)tap;
{
    //取出当前点击的lebal 的index
    NSInteger index = [tap.view.superview.subviews indexOfObject:tap.view];
    
    //取出当前的偏移量
    CGPoint  offset = self.contentScrollView.contentOffset;
    offset.x = index * SCREEN_WIDTH;
    
    //让内容的scollView 滚动到相应位置
    [self.contentScrollView setContentOffset:offset animated:YES];
    [self scrollViewDidEndScrollingAnimation:self.contentScrollView];
}

- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
@end
