//
//  ZHLiftViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLiftViewController.h"

#import "ZHLiftCollectionViewCell.h"

@interface ZHLiftViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collection;
@property(nonatomic,strong)NSArray *dataSource;
@property(nonatomic,strong)NSArray *iconArray;

@end

@implementation ZHLiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNavgation];
    [self.view addSubview:self.collection];
}

#pragma mark collection的协议方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ZHLiftCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.icon.image = [UIImage imageNamed:self.iconArray[indexPath.row]];
    [cell.titleButton setTitle:self.dataSource[indexPath.row] forState:UIControlStateNormal];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark collection懒加载
- (UICollectionView *)collection{
    if (_collection==nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake(SCREEN_WIDTH/3.f,SCREEN_WIDTH/3.f+10);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        _collection = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collection.backgroundColor = [UIColor whiteColor];
        _collection.delegate = self;
        _collection.dataSource = self;
        [_collection registerNib:[UINib nibWithNibName:@"ZHLiftCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    }
    return _collection;
}
- (NSArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = @[@"水费",@"电费",@"燃气费",@"有线电视",@"手机充值",@"物业费"];
    }
    return _dataSource;
}
- (NSArray *)iconArray{
    if (_iconArray == nil) {
        _iconArray = @[@"chewei",@"bianmin",@"zhekou",@"gonggao",@"jiaofei",@"baoxiu"];
    }
    return _iconArray;
}

#pragma mark 导航栏
- (void)setUpNavgation{
    self.title = @"生活缴费";
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
@end
