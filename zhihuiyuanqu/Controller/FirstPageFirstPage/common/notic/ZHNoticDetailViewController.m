//
//  ZHNoticDetailViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNoticDetailViewController.h"

#import "ZHNoticDetailTableViewCell.h"
#import "ZHFirstPageAdsModel.h"

@interface ZHNoticDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHNoticDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpNavgation];
    [self.view addSubview:self.tableView];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNoticDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNoticDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell.maxY;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHNoticDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
        _dataSource = @[@"",@"",@"",@"",@"",@"",@""];
    }
    return _dataSource;
}

#pragma mark 导航栏
- (void)setUpNavgation{
    self.title = @"物业公告详情";
    self.tabBarController.tabBar.hidden = YES;
}

@end
