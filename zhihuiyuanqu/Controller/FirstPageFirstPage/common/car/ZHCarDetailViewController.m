//
//  ZHCarDetailViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHCarDetailViewController.h"

#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "ZHCarSharesModel.h"
#import "ZHCarDetailTableViewCell.h"

@interface ZHCarDetailViewController ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate,AMapSearchDelegate>

{
    MAMapView *_mapView;
    AMapSearchAPI  *_search;
}

/** 界面展示 */
@property (nonatomic, strong) UITableView *tableView;

/** 全局数组, 用来存放分组的数据 */
@property (nonatomic, strong) NSArray *dataSource;
@property(nonatomic,strong)NSMutableDictionary *dictData;//大字典
@property(nonatomic,strong)NSMutableDictionary *dict;//小字典
@property(nonatomic,strong)UIButton *releaseButton;//发布按钮
@end

@implementation ZHCarDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.releaseButton];
    [self.view bringSubviewToFront:self.releaseButton];
    
    [self setUpNavigationItem];
    [self setNotification];
    [self createMAMapServices];
    
}

#pragma mark 高德地图
- (void)createMAMapServices{
    //配置用户Key
    [MAMapServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";
    
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;//YES 为打开定位，NO为关闭定位
    [_mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];//地图跟着位置移动
    
}
//当位置更新时，会进定位回调，通过回调函数，能获取到定位点的经纬度坐标，
- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"当前的经纬度坐标：latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
        NSString *lat = [NSString stringWithFormat:@"%f",userLocation.coordinate.latitude];
        NSString *lng = [NSString stringWithFormat:@"%f",userLocation.coordinate.longitude];
        [self.dictData setValue:lat forKey:@"lat"];
        [self.dictData setValue:lng forKey:@"lng"];
        [self createSearchAPIWithLatitude:userLocation.coordinate.latitude Longitude:userLocation.coordinate.longitude];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:lat forKey:@"lat"];
        [defaults setObject:lng forKey:@"lng"];
    }
 
}
- (void)createSearchAPIWithLatitude:(CGFloat)latitude Longitude:(CGFloat)longitude{
    [AMapSearchServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";

    //初始化检索对象
    _search = [[AMapSearchAPI alloc] init];
    _search.delegate = self;
    
    //构造AMapReGeocodeSearchRequest对象
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    regeo.location = [AMapGeoPoint locationWithLatitude:latitude     longitude:longitude];
    regeo.radius = 10000;
    regeo.requireExtension = YES;
    
    //发起逆地理编码
    [_search AMapReGoecodeSearch: regeo];
}
//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
//        NSString *result1 = [NSString stringWithFormat:@"%@", response.regeocode.addressComponent.province];//省
//        NSString *result2 = [NSString stringWithFormat:@"%@", response.regeocode.addressComponent.city];//市
//        NSString *result3 = [NSString stringWithFormat:@"%@", response.regeocode.addressComponent.district];//区
        NSString *result4 = [NSString stringWithFormat:@"%@", response.regeocode.formattedAddress];//详细
        NSLog(@"当前位置: %@",result4);
        [self.dict setValue:result4 forKey:@"address"];
        [self.dictData setValue:result4 forKey:@"address"];
        
        NSUserDefaults *defults = [NSUserDefaults standardUserDefaults];
        [defults setObject:result4 forKey:@"address"];
    }
}

#pragma mark - notification handler
- (void)setNotification{
    // 设置键盘监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //接受信息填写不完整的通知
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(center1NoticAction) name:@"Incomplete information" object:nil];
    
    //接受信息填写完整的通知
    NSNotificationCenter *center2 = [NSNotificationCenter defaultCenter];
    [center2 addObserver:self selector:@selector(center2NoticAction:) name:@"Complete information" object:nil];
}
- (void)didKeyboardShow:(NSNotification *)noti
{
    CGRect frame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.view.frame.size.height - frame.size.height +20);
    }];
}

- (void)didKeyboardHide:(NSNotification *)noti
{
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40);
    }];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"车位详情";
    self.tabBarController.tabBar.hidden = YES;
}


#pragma mark 发布按钮
- (UIButton *)releaseButton{
    if (_releaseButton==nil) {
        _releaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _releaseButton.frame = CGRectMake(0, SCREEN_HEIGHT-104, SCREEN_WIDTH, 40);
        _releaseButton.backgroundColor = [UIColor lightGrayColor];
        _releaseButton.layer.borderWidth = 1;
        [_releaseButton setTitle:@"确认发布" forState:UIControlStateNormal];
        [_releaseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_releaseButton addTarget:self action:@selector(releaseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _releaseButton;
}
- (void)releaseButtonAction:(UIButton *)button{
    
    //点击确认发布按钮的通知(传送当前地址)
    NSNotification *notic = [NSNotification notificationWithName:@"clickReleaseButton" object:nil userInfo:@{@"address":self.dictData[@"address"]}];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
}
- (void)center1NoticAction{
    [self showHint:@"信息填写不完整"];
}
- (void)center2NoticAction:(NSNotification *)notification{
    NSDictionary *dict = notification.userInfo;
    NSString *type = dict[@"type"];
    
    [self.dictData setValue:dict forKey:@"parkingshare"];
    [self.dictData setValue:type forKey:@"type"];
    
    //请求数据
    [self requestData:self.dictData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Incomplete information" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Complete information" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 请求数据
- (void)requestData:(NSDictionary *)dict{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];

    NSDictionary *parameters = @{@"token":token,@"jsonStr":[dict jsonData]};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_releaseParkingURL parameters:parameters success:^(id json) {
        if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
            
            [self showHint:@"发布成功"];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"发布车位失败：%@",error);
    }];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SCREEN_WIDTH;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCarDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCarDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHCarDetailTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _mapView;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-80) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //滑动tableView收缩键盘
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHCarDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

- (NSArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = @[@""];
        
    }
    return _dataSource;
}
- (NSMutableDictionary *)dictData{
    if (_dictData==nil) {
        _dictData = [NSMutableDictionary dictionary];
    }
    return _dictData;
}
- (NSMutableDictionary *)dict{
    if (_dict==nil) {
        _dict = [NSMutableDictionary dictionary];
    }
    return _dict;
}

@end
