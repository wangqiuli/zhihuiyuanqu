//
//  ZHLookCarViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/12.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLookCarViewController.h"

#import "ZHLookCarTableViewCell.h"
#import "ZHCarSharesModel.h"
#import "ZHGetCarViewController.h"
@interface ZHLookCarViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property(nonatomic,strong)UISearchController *searchControl;//搜索框
@property(nonatomic,strong)NSMutableArray *searchResults;
@property(nonatomic,strong)MJRefreshNormalHeader *header;
@property(nonatomic,strong)MJRefreshAutoNormalFooter *footer;
@end

@implementation ZHLookCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _lastId = 0;
    ret = YES;
    [self.view addSubview:self.tableView];
    [self requestData:@""];
    self.navigationController.navigationBar.translucent = NO;
    
}

#pragma mark 请求数据
- (void)requestData:(NSString *)address{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    
    if (ret == YES) {
        lastId2 = @"0";
    }
    
    NSDictionary *parameters = @{@"token":token,
                                 @"type":@"1002",
                                 @"address":address,
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_HaveCar_URL parameters:parameters success:^(id json) {
        
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"我找车位获取数据失败:%@",error);
        [self stopRefresh];
    }];
    
}
- (void)tableViewData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHCarSharesModel *model = [[ZHCarSharesModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    self.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData:@""];
    }];
    self.tableView.mj_header = self.header;
    
    //2.上拉加载
    self.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        _lastId = _lastId+10;
        ret = NO;
        [self requestData:@""];
    }];
    self.tableView.mj_footer = self.footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark searchController的协议方法
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    //添加之前先清空原来的内容
    [self.searchResults removeAllObjects];
    
    //遍历所有的数据，将符合条件的添加到搜索的结果中
    for (ZHCarSharesModel *model in self.dataSource) {
        Parkingshare *parkingshare = model.parkingshare;
        //搜索的判断
        if ([parkingshare.home containsString:self.searchControl.searchBar.text]||[parkingshare.address containsString:self.searchControl.searchBar.text]||[parkingshare.price containsString:self.searchControl.searchBar.text]||[parkingshare.time containsString:self.searchControl.searchBar.text]) {
            [self.searchResults addObject:model];
        }
    }
    [self.tableView reloadData];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.searchControl.active) {
        return self.searchResults.count;
    }
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHLookCarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (self.searchControl.active) {
        ZHCarSharesModel *model = self.searchResults[indexPath.row];
        cell.model = model;
    }else{
        ZHCarSharesModel *model = self.dataSource[indexPath.row];
        cell.model = model;
    }
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHLookCarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (self.searchControl.active) {
        ZHCarSharesModel *model = self.searchResults[indexPath.row];
        cell.model = model;
    }else{
        ZHCarSharesModel *model = self.dataSource[indexPath.row];
        cell.model = model;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHCarSharesModel *model;
    if (self.searchControl.active) {
        self.searchControl.searchBar.hidden = YES;
        model = self.searchResults[indexPath.row];
    }else{
        model = self.dataSource[indexPath.row];
    }
    
    ZHGetCarViewController *selfCar = [[ZHGetCarViewController alloc]init];
    selfCar.id = model.parkingshare.ID;
    selfCar.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:selfCar animated:YES];
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40-64) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.tableHeaderView = self.searchControl.searchBar;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHLookCarTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        [self addRefreshControl];
    }
    return _tableView;
}
- (UISearchController *)searchControl{
    if (_searchControl==nil) {
        _searchControl = [[UISearchController alloc]initWithSearchResultsController:nil];
        _searchControl.hidesNavigationBarDuringPresentation = NO;
        _searchControl.dimsBackgroundDuringPresentation = NO;
        _searchControl.searchResultsUpdater = self;
        _searchControl.searchBar.delegate = self;
        _searchControl.searchBar.placeholder = @"输入小区名称";
    }
    return _searchControl;
}

- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (NSMutableArray *)searchResults{
    if (_searchResults==nil) {
        _searchResults = [NSMutableArray array];
    }
    
    return _searchResults;
}
- (void)viewWillAppear:(BOOL)animated{
    self.searchControl.searchBar.hidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated{
    if (self.searchControl.isActive) {
        self.searchControl.active = NO;
        self.searchControl.searchBar.hidden = YES;
    }
}
@end
