//
//  ZHServiceViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/26.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHServiceViewController.h"

#import "ZHServiceLeftTableViewCell.h"
#import "ZHServiceRightTableViewCell.h"
#import "ZHFirstPageAdsModel.h"

#import "ZHServiceNearlyViewController.h"
@interface ZHServiceViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    CGFloat heightRight;//右侧cell的高度
    NSIndexPath *selectCell;
    NSInteger select ;
}
@property(nonatomic,strong)UITableView *leftTableView;

@property(nonatomic,strong)UITableView *rightTableView;

@property(nonatomic,strong)NSMutableArray *leftDataSource;

@property(nonatomic,strong)NSMutableArray *rightDataSource;

@property(nonatomic,strong)NSMutableArray *selectDataSource;


@end

@implementation ZHServiceViewController

#define Left_Tag 200
#define Rifgt_Tag 300

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    selectCell = 0;
    select = -1;
    
    [self setUpNavgation];
    [self.view addSubview:self.leftTableView];
    [self.view addSubview:self.rightTableView];
}

#pragma mark 协议方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView.tag == Rifgt_Tag) {
        return self.leftDataSource.count;
    }else{
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == Rifgt_Tag) {
        return 1;
    }else{
        return self.leftDataSource.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == Left_Tag) {
        //左视图
        ZHServiceLeftTableViewCell *cellLeft = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (cellLeft==nil) {
            cellLeft = [[NSBundle mainBundle]loadNibNamed:@"ZHServiceLeftTableViewCell" owner:self options:nil].lastObject;
        }
        ZHFirstPageAdsModel *modelLeft = self.leftDataSource[indexPath.row];
        cellLeft.model = modelLeft;
        cellLeft.selectionStyle = NO;
        
        if (indexPath.row==select) {
            [cellLeft setSelected:YES tag:indexPath];

        }else{
            [cellLeft setSelected:NO tag:indexPath];
        }

        
        return cellLeft;
    }else{
        //右视图
        ZHServiceRightTableViewCell *cellRight = [tableView dequeueReusableCellWithIdentifier:@"cellRight"];
        ZHFirstPageAdsModel *modelLeft = self.leftDataSource[indexPath.section];
        cellRight.model = modelLeft;
        cellRight.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //选中跳转界面
        cellRight.setCommonViewActionBlock = ^(NSInteger tag){
            switch (tag) {
                case 0:{
                    ZHServiceNearlyViewController *nearly = [[ZHServiceNearlyViewController alloc]init];
                    [self.navigationController pushViewController:nearly animated:YES];
                    break;
                }
                default:
                    break;
            }
        };
        
        return cellRight;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == Left_Tag) {
        return 44;
    }else{
        ZHServiceRightTableViewCell *cellRight = [tableView dequeueReusableCellWithIdentifier:@"cellRight"];
        ZHFirstPageAdsModel *modelLeft = self.leftDataSource[indexPath.section];
        cellRight.model = modelLeft;
        heightRight = cellRight.maxY;
        return cellRight.maxY;
    }

}
//设置组头
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    CGFloat width = (SCREEN_WIDTH/5*4-10*4)/3;
    
    if (tableView.tag == Left_Tag) {
        return nil;
    }else{
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/5, 0, SCREEN_WIDTH/5*4, 30)];
        
        UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(10, 14, width, 2)];
        UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label1.frame)+10, 0, width, 30)];
        UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label2.frame)+10, 14, width, 2)];
        
        label1.backgroundColor = [UIColor lightGrayColor];
        label3.backgroundColor = [UIColor lightGrayColor];
        label2.text = self.leftDataSource[section];
        label2.textAlignment = NSTextAlignmentCenter;
        
        [view addSubview:label1];
        [view addSubview:label2];
        [view addSubview:label3];
        return view;
    }
    
}
//设置组头的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag == Left_Tag) {
        return 0.1;
    }else{
        return 30;
    }
}
//设置组尾的高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section

{
    
    if(tableView.tag == Left_Tag){
        
        [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:section inSection:8] animated:YES scrollPosition:UITableViewScrollPositionNone];
        
    }
    
}

//选中
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == Left_Tag) {
        
        NSInteger yCon = (NSUInteger)indexPath.row;
        
        ZHServiceLeftTableViewCell *cellLeftLow = [tableView cellForRowAtIndexPath:indexPath];
        [cellLeftLow setSelected:YES tag:indexPath];

        select = yCon;

        [self.rightTableView setContentOffset:CGPointMake(0, yCon*(heightRight+30)) animated:NO];

    }
}
//反选
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == Left_Tag) {
        
        ZHServiceLeftTableViewCell *cellLeft = [tableView cellForRowAtIndexPath:indexPath];
        [cellLeft setSelected:NO tag:indexPath];

    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    if (scrollView == self.rightTableView) {
        NSArray *array = [self.rightTableView indexPathsForVisibleRows];
        NSIndexPath *indexPath = array.firstObject;
        
        [self tableView:_leftTableView didDeselectRowAtIndexPath:[NSIndexPath indexPathForItem:select inSection:0]];
        
        select = indexPath.section;
        
        [self tableView:self.leftTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:indexPath.section inSection:0]];
        
        
        [_leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:indexPath.section inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];//设置选中第一行（默认有蓝色背景）
    }
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.rightTableView) {
        NSArray *array = [self.rightTableView indexPathsForVisibleRows];
        NSIndexPath *indexPath = array.firstObject;
        
        [self tableView:_leftTableView didDeselectRowAtIndexPath:[NSIndexPath indexPathForItem:select inSection:0]];
        
        select = indexPath.section;
        
        [self tableView:self.leftTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:indexPath.section inSection:0]];
        
        
        [_leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:indexPath.section inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];//设置选中第一行（默认有蓝色背景）
    }

}

#pragma mark 左视图
- (UITableView *)leftTableView{
    if (_leftTableView==nil) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/5.f, SCREEN_HEIGHT-24) style:UITableViewStyleGrouped];
        _leftTableView.backgroundColor = [UIColor whiteColor];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.separatorStyle = NO;
        _leftTableView.tag = Left_Tag;
        
        _leftTableView.layer.borderWidth = 1;
       _leftTableView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        [_leftTableView registerNib:[UINib nibWithNibName:@"ZHServiceLeftTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
        //设置第一行为默认选中，蓝色
        if(self.leftDataSource.count>0)
            
        {
            
            [_leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];//设置选中第一行（默认有蓝色背景）
            
            [self tableView:_leftTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];//实现点击第一行所调用的方法
            
        }

    }
    
    return _leftTableView;
}
- (NSMutableArray *)leftDataSource{
    if (_leftDataSource==nil) {
        _leftDataSource = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19"];
    }
    return _leftDataSource;
}
#pragma mark 右视图
- (UITableView *)rightTableView{
    if (_rightTableView==nil) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/5.f, 0, SCREEN_WIDTH/5*4, SCREEN_HEIGHT-64) style:UITableViewStyleGrouped];
        _rightTableView.backgroundColor = [UIColor whiteColor];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.tag = Rifgt_Tag;
        _rightTableView.separatorStyle = UITableViewCellAccessoryNone;
        [_rightTableView registerClass:[ZHServiceRightTableViewCell class] forCellReuseIdentifier:@"cellRight"];
    }
    return _rightTableView;
}
- (NSMutableArray *)rightDataSource{
    if (_rightDataSource==nil) {
        _rightDataSource = @[@"1"];
    }
    return _rightDataSource;
}
- (NSMutableArray *)selectDataSource{
    if (_selectDataSource==nil) {
        _selectDataSource = [NSMutableArray array];
    }
    return _selectDataSource;
}
#pragma mark 导航栏
- (void)setUpNavgation{
    self.title = @"便民服务";
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
@end
