//
//  ZHNoticeViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHNoticeViewController.h"

#import "ZHNoticeTableViewCell.h"
#import "ZHFirstPageAdsModel.h"
#import "ZHNoticDetailViewController.h"

@interface ZHNoticeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHNoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNavgation];
    [self.view addSubview:self.tableView];
}
#pragma mark tableView的协议方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHNoticeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource[indexPath.section];
    cell.model = model;
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    label.text = @"5分钟前";
    label.textColor = [UIColor lightGrayColor];
    label.backgroundColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNoticeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHFirstPageAdsModel *model = self.dataSource[indexPath.section];
    cell.model = model;
    return cell.maxY;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNoticDetailViewController *detail = [[ZHNoticDetailViewController alloc]init];
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = NO;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHNoticeTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
        _dataSource = @[@"",@"",@"",@"",@"",@"",@""];
    }
    return _dataSource;
}

#pragma mark 导航栏
- (void)setUpNavgation{
    self.title = @"物业公告";
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}

@end
