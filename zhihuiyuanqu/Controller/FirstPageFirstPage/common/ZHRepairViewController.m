//
//  ZHRepairViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHRepairViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ZHRepairMyViewController.h"

@interface ZHRepairViewController ()<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;//物业电话
@property (strong, nonatomic) IBOutlet UIView *homeFixView;
@property (strong, nonatomic) IBOutlet UIButton *homeFixButton;//房屋维修
@property (strong, nonatomic) IBOutlet UIButton *waterButton;//水电燃气
@property (strong, nonatomic) IBOutlet UIButton *publicButton;//公共设施
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;//称呼
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;//电话号码
@property (strong, nonatomic) IBOutlet UITextField *timetextField;//希望处理时间
@property (weak, nonatomic) IBOutlet UIButton *workTimeButton;//工作日
@property (weak, nonatomic) IBOutlet UIButton *weekedTimeButton;//周末
@property (weak, nonatomic) IBOutlet UIButton *anyTimeButton;//不限

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property(strong,nonatomic)UIImageView *icon;
@property(nonatomic,assign)NSInteger tagIndex;

//具体情况
@property(nonatomic,strong)UIView *thingView;
@property(nonatomic,strong)UITextView *textView;//具体的情况
@property(nonatomic,strong)UILabel *thingLabel;
@property(nonatomic,strong)UIButton *thingButton;//读取相册按钮
@property(strong,nonatomic)UIButton *submitButton;//提交按钮
@property(strong,nonatomic)NSMutableArray *iconsArray;//存储相册图片
@property(nonatomic,strong)NSMutableDictionary *dictSource;//存储所有填写的数据
@property(nonatomic,strong)NSString *iconStr;//拼接上传后的图片
@end

@implementation ZHRepairViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setUpNavgation];
    [self setNotification];
    
    self.ScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.ScrollView addSubview:self.thingView];
    
    [self.ScrollView addSubview:self.submitButton];
}

//判断维修类型
- (IBAction)homeFixButtonAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
        [self.dictSource setValue:sender.titleLabel.text forKey:@"type"];
    }
    switch (sender.tag-100) {
        case 0:{
            self.waterButton.selected = NO;
            self.publicButton.selected = NO;
            [self.dictSource setValue:@"房屋维修" forKey:@"type"];
            break;
        }
        case 1:{
            self.homeFixButton.selected = NO;
            self.publicButton.selected = NO;
            [self.dictSource setValue:@"水电燃气" forKey:@"type"];
            break;
        }
        case 2:{
            self.waterButton.selected = NO;
            self.homeFixButton.selected = NO;
            [self.dictSource setValue:@"公共设施" forKey:@"Type"];
            break;
        }
        default:
            break;
    }
    
}
//判断维修时间
- (IBAction)repairTime:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
        [self.dictSource setValue:sender.titleLabel.text forKey:@"doTime"];
    }
    switch (sender.tag-100) {
        case 0:{
            self.weekedTimeButton.selected = NO;
            self.anyTimeButton.selected = NO;
            break;
        }
        case 1:{
            self.workTimeButton.selected = NO;
            self.anyTimeButton.selected = NO;
            break;
        }
        case 2:{
            self.workTimeButton.selected = NO;
            self.weekedTimeButton.selected = NO;
            break;
        }
        default:
            break;
    }
}


#pragma mark 需要的服务或者遇到的情况
- (NSMutableArray *)iconsArray{
    if (_iconsArray==nil) {
        _iconsArray = [NSMutableArray array];
    }
    return _iconsArray;
}
- (UIView *)thingView{
    if (_thingView==nil) {
        _thingView = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(self.bottomView.frame)+21, SCREEN_WIDTH-20,SCREEN_WIDTH/3.f+(SCREEN_WIDTH-20-60)/5.f+20 )];
        _thingView.backgroundColor = [UIColor whiteColor];
        [_thingView addSubview:self.textView];
        [_thingView addSubview:self.thingButton];
    }
    return _thingView;
}
- (UITextView *)textView{
    if (_textView==nil) {
        _textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-20, SCREEN_WIDTH/3)];
        self.thingLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-20, 30)];
        _textView.delegate = self;
        self.thingLabel.text = @"您需要的服务以及遇到的情况";
        self.thingLabel.textColor = [UIColor lightGrayColor];
        [_textView addSubview:self.thingLabel];
    }
    return _textView;
}
- (UIButton *)thingButton{
    if (_thingButton==nil) {
        _thingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    
    float width = (SCREEN_WIDTH-20-60)/5.f;
    if (self.iconsArray.count==0||self.iconsArray==nil) {
        _thingButton.frame = CGRectMake(10,CGRectGetMaxY(self.textView.frame)+10, width, width);
        [_thingButton setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        _thingButton.layer.cornerRadius = 5;
        _thingButton.layer.masksToBounds = YES;
        _thingButton.backgroundColor = UICOLOR_RGB;
        [_thingButton addTarget:self action:@selector(thingButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }else{
        NSInteger i = self.iconsArray.count - 1;
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.textView.frame)+ 10+i/5*(width+10), width, width)];
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        self.icon.tag = self.tagIndex++;
        self.icon.image = self.iconsArray[i];
        self.icon.userInteractionEnabled = YES;
        [self.thingView addSubview:self.icon];
        
        _thingButton.frame = CGRectMake(self.iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.textView.frame)+ 10+self.iconsArray.count/5*(width+10), width, width );
        [_thingButton setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        _thingButton.layer.cornerRadius = 5;
        _thingButton.layer.masksToBounds = YES;
        _thingButton.backgroundColor = UICOLOR_RGB;
        [_thingButton addTarget:self action:@selector(thingButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [_thingView addSubview:_thingButton];
        
        //读取相片后，修改thingView和submitButton的frame
        self.thingView.frame = CGRectMake(10, CGRectGetMaxY(self.bottomView.frame)+21, SCREEN_WIDTH-20, CGRectGetMaxY(_thingButton.frame)+10);
        self.submitButton.frame =CGRectMake(0, CGRectGetMaxY(self.thingView.frame)+10, SCREEN_WIDTH, 40);
        [self viewDidAppear:YES];
    }
    return _thingButton;
}
- (void)iconAction:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag ;
    UIImageView *icon = [self.thingView viewWithTag:tag];
    NSUInteger index = [self.iconsArray indexOfObject:icon.image];
    [self.iconsArray removeObjectAtIndex:index];
    for (UIView* uiImageView in self.thingView.subviews) {
        if( [uiImageView  isKindOfClass :[UIImageView class]]){
            [uiImageView removeFromSuperview];
        }
    }
    
    float width = (SCREEN_WIDTH-60-20)/5.f;
    
    for (int i = 0 ; i < self.iconsArray.count ; i++) {
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.textView.frame)+ 10+i/5*(width+10), width, width)];
        self.icon.image = self.iconsArray[i];
        self.icon.tag = self.tagIndex++;
        self.icon.userInteractionEnabled = YES;
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        [self.thingView addSubview:self.icon];
        
    }
    
    self.thingButton.frame = CGRectMake(self.iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.textView.frame)+ 10+self.iconsArray.count/5*(width+10), width, width );
    [self.thingButton setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
    self.thingButton.layer.cornerRadius = 5;
    self.thingButton.layer.masksToBounds = YES;
    self.thingButton.backgroundColor = UICOLOR_RGB;
    [self.thingButton addTarget:self action:@selector(thingButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.thingView addSubview:self.thingButton];
   
    self.thingView.frame = CGRectMake(10, CGRectGetMaxY(self.bottomView.frame)+21, SCREEN_WIDTH-20, CGRectGetMaxY(_thingButton.frame)+10);
    self.submitButton.frame =CGRectMake(0, CGRectGetMaxY(self.thingView.frame)+10, SCREEN_WIDTH, 40);
    [self viewDidAppear:YES];

}
- (void)thingButtonAction{
    //读取相册和相机
    [self editImageSelect];
}
#pragma mark 读取相机和相册
- (void)editImageSelect{
    UIAlertController *alertController = [[UIAlertController alloc]init];
    
    UIAlertAction *action0 = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = YES;
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有摄像头"];
            
        }
        
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.allowsEditing = YES;
            controller.delegate = self;
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有相册"];
            
        }
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:action0];
    [alertController addAction:action1];
    [alertController addAction:action2];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        if ([picker allowsEditing]) {
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerEditedImage]];
            
        }else{
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerOriginalImage]];
            
        }
        
        if (self.thingButton!=nil) {
            [self.thingButton removeFromSuperview];
            self.thingButton =nil;
            [self thingButton];
        }
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self changeUserImage];
}
- (void)changeUserImage{
    
    //将图片存到本地
    [self saveImage:self.iconsArray];
}
#pragma mark 网络请求
//上传多张图片
- (void)saveImage:(NSArray *)iconsArray{
    for (int i=0; i<iconsArray.count; i++) {
        NSData* imageData = UIImageJPEGRepresentation(iconsArray[i], 0.5);
        NSDictionary *fileParameters = @{@"image":imageData,
                                         @"fileKey":@"file",
                                         @"nameKey":@"file",
                                         @"mimeType":@"image/jpeg"};
        [ZHRequestData uploadFilesWithSubpath:PATH_uploadFileURL parameters:nil fileArray:@[fileParameters] success:^(id json) {

            if (self.iconStr==nil) {
                self.iconStr = [@"" stringByAppendingString:json[@"returning"]];
            }else{
                self.iconStr = [self.iconStr stringByAppendingFormat:@";%@",json[@"returning"]];
            }
            
            //上传多张图片成功后，调用发布物业维修接口
            if (i==iconsArray.count-1) {
                [self requestData];
            }
        } failure:^(NSError *error) {
            NSLog(@"物业维修图片上传错误:%@",error);
        }];
    }
    
}
- (void)requestData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSNumber *userId1 = [defaults objectForKey:@"userId"];
    NSString *userId = [NSString stringWithFormat:@"%@",userId1];
    if (self.textView.text.length==0||self.phoneTextField.text.length==0||userId.length==0) {
        return;
    }
    
    [self.dictSource setValue:self.textView.text forKey:@"detail"];
    [self.dictSource setValue:self.phoneTextField.text forKey:@"phone"];
    [self.dictSource setValue:userId forKey:@"userId"];
    [self.dictSource setValue:self.iconStr forKey:@"image"];
    
    NSDictionary *parameters = @{@"token":token,@"jsonStr":[self.dictSource jsonData]};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_ReleaseRepairURL parameters:parameters success:^(id json) {
        [self showHint:@"发布成功"];
        NSLog(@"发布物业维修成功：%@",json[@"message"]);
        
    } failure:^(NSError *error) {
        NSLog(@"发布物业维修错误：%@",error);
    }];
}


#pragma mark textView的协议方法
- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length==0) {
        self.thingLabel.hidden = NO;
    }else{
        self.thingLabel.hidden = YES;
    }
}

#pragma mark 提交按钮懒加载
- (UIButton *)submitButton{
    if (_submitButton==nil) {
        _submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitButton.backgroundColor = [UIColor whiteColor];
        _submitButton.frame = CGRectMake(10, CGRectGetMaxY(self.thingView.frame)+10, SCREEN_WIDTH-20, 40);
        _submitButton.layer.cornerRadius = 10;
        _submitButton.layer.masksToBounds = YES;
        [_submitButton setTitle:@"提交" forState:UIControlStateNormal];
        [_submitButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_submitButton addTarget:self action:@selector(submitButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}
- (void)submitButtonAction{
    
    //1.先上传图片
    [self changeUserImage];
    
}

#pragma mark 设置键盘弹起和收缩
- (void)setNotification{
    // 设置键盘监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)didKeyboardShow:(NSNotification *)noti
{
    CGRect frame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:0.25f animations:^{
   
        CGFloat textheight ;
        if (self.nameTextField.isFirstResponder) {
            
            textheight = CGRectGetMaxY(self.nameTextField.frame) + CGRectGetMinY(self.bottomView.frame);
            
        }else if (self.phoneTextField.isFirstResponder){
            textheight = CGRectGetMaxY(self.phoneTextField.frame)+ CGRectGetMinY(self.bottomView.frame);
        }else if (self.timetextField.isFirstResponder){
            textheight = CGRectGetMaxY(self.timetextField.frame)+ CGRectGetMinY(self.bottomView.frame);
        }else{
            textheight = CGRectGetMaxY(self.thingButton.frame)+ CGRectGetMinY(self.thingView.frame);
        }
            
        CGFloat scrollHeight = self.view.frame.size.height - frame.size.height - textheight ;
        
        if(scrollHeight > 0 ){
            scrollHeight = 0;
        }else{
            scrollHeight = scrollHeight - 10;
        }

        [self.ScrollView setContentOffset:CGPointMake(0, -scrollHeight) animated:YES];
    }];
}
- (void)didKeyboardHide:(NSNotification *)noti
{
    [UIView animateWithDuration:0.25f animations:^{

        [self.ScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }];
}

#pragma mark 导航栏
- (void)setUpNavgation{
    self.title = @"物业维修";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"历史" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemAction)];
}
- (void)rightBarButtonItemAction{
    ZHRepairMyViewController *repair = [[ZHRepairMyViewController alloc]init];
    repair.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:repair animated:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.ScrollView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.5];
    self.ScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.submitButton.frame)-40);
    
}
- (void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
}
- (NSMutableDictionary *)dictSource{
    if (_dictSource == nil) {
        _dictSource = [NSMutableDictionary dictionary];
    }
    return _dictSource;
}
@end
