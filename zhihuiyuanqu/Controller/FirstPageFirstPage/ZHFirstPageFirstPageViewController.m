//
//  ZHFirstPageFirstPageViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageFirstPageViewController.h"

#import "ZHFirstPageFirstPageHeaderView.h"
#import "ZHFirstPageTableViewCell.h"

#import "ZHCommonReadilyViewController.h"
#import "ZHServiceViewController.h"
#import "ZHCarShareViewController.h"
#import "ZHSecondHandViewController.h"
#import "ZHFirstPageDiscountViewController.h"
#import "ZHNoticeViewController.h"
#import "ZHLiftViewController.h"
#import "ZHRepairViewController.h"
#import "ZHTransferViewController.h"

#import "ZHFirstPageCommonModel.h"
#import "ZHAdsFirstPageModel.h"
#import "ZHDiscountModel.h"
@interface ZHFirstPageFirstPageViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong)ZHFirstPageFirstPageHeaderView  *headerView;// 头视图
@property (nonatomic, strong) NSMutableArray *dataSource;//数据源
@property(nonatomic,strong)NSMutableArray *adsDataSource;//广告栏数据源
@property(nonatomic,strong)NSMutableArray *commonDataSource;//功能栏数据源
@end

@implementation ZHFirstPageFirstPageViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _lastId = 0;
    ret = YES;
    [self requestData];//请求数据
    [self.view addSubview:self.tableView];
}

#pragma mark 请求广告栏数据
- (void)requestData{
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_FirstPage_ADSURL parameters:@{@"id":@"0"} success:^(id json) {
        if (self.adsDataSource.count!=0) {
            [self.adsDataSource removeAllObjects];
        }
        [self adsData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"首页广告栏网络请求失败，原因%@",error);
        [self stopRefresh];
    }];
    
    [self commonData];
    
    [self requestTableViewData];
}
- (void)requestTableViewData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];

    if (token==nil) {
        [self showHint:@"请先登录"];
        return;
    }
    if (ret == YES) {
        lastId2 = @"0";
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"type":@"美容",
                                 @"address":@"",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};

    [ZHRequestData serviceUploadFilesWithSubpath:PATH_Discount_URL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"折扣获取数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)adsData:(NSDictionary *)data{
    
    // 1. 获取存放所有广告信息的数组
    NSArray *allAds = data[@"returning"];
    

     for (NSDictionary *info in allAds) {
        // 字典转模型
        ZHAdsFirstPageModel *model = [[ZHAdsFirstPageModel alloc] initWithDictionary:info error:nil];

        // 添加到数组中做存储
        [self.adsDataSource addObject:model];
         
    }
    
    // 3. 把数据给headerView做界面展示
    [self.headerView setAdsData:self.adsDataSource clickCallBack:^(ZHAdsFirstPageModel *model) {
//        NSLog(@"广告栏图片的ID是%zd",model.id);
    }];
}
- (void)commonData{
 
    NSArray *allModels = @[@"",@"",@"",@"",@"",@"",@"",@""];
    
    __weak ZHFirstPageFirstPageViewController *weakSelf = self;
    // 设置所有的数据, 并设置点击的回调
    [self.headerView setCommonData:allModels clickCallBacl:^(NSInteger integer) {
        switch (integer) {
            case 0:{
                [weakSelf.navigationController pushViewController:[[ZHCarShareViewController alloc]init] animated:YES];
                break;
            }
            case 1:{
                [weakSelf.navigationController pushViewController:[[ZHServiceViewController alloc]init] animated:YES];
                break;
            }
            case 2:{
                
                // 这里是点击分类的回调
                [weakSelf.navigationController pushViewController:[[ZHSecondHandViewController alloc]init] animated:YES];
                break;
            }
            case 3:{
                
                // 这里是点击分类的回调
                UIStoryboard *story=[UIStoryboard  storyboardWithName:@"Main" bundle:nil];
                
                ZHFirstPageDiscountViewController *discount = [story instantiateViewControllerWithIdentifier:@"ZHFirstPageDiscountViewController"];
                discount.hidesBottomBarWhenPushed = YES;
                [weakSelf.navigationController pushViewController:discount animated:YES];
                break;
            }
            case 4:{
                ZHNoticeViewController *notice = [[ZHNoticeViewController alloc]init];
                [weakSelf.navigationController pushViewController:notice animated:YES];
                break;
            }
            case 5:{
                
                // 这里是点击分类的回调
                [weakSelf.navigationController pushViewController:[[ZHCommonReadilyViewController alloc]init] animated:YES];
                break;
            }
            case 6:{
                ZHLiftViewController *lift = [[ZHLiftViewController alloc]init];
                [weakSelf.navigationController pushViewController:lift animated:YES];
                break;
            }
            case 7:{
                ZHRepairViewController *repair = [[ZHRepairViewController alloc]init];
                [weakSelf.navigationController pushViewController:repair animated:YES];
                break;
            }
            default:
                break;
        }
        
    }];

}
- (void)tableViewData:(NSDictionary *)data{

    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHDiscountModel *model = [[ZHDiscountModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId + 10;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
    
}

#pragma mark tableView的协议方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHFirstPageTableViewCell" owner:self options:nil].lastObject;
    }
    cell.model = self.dataSource[indexPath.section];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.dataSource[indexPath.section];
    return cell.maxY;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHTransferViewController *detail = [[ZHTransferViewController alloc]init];
    ZHDiscountModel *model = self.dataSource[indexPath.section];
    detail.id = model.discount.id;
    [self.navigationController pushViewController:detail animated:YES];

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-80-40) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        // 头视图
        _tableView.tableHeaderView = self.headerView;
        
        // 刷新集成
        [self addRefreshControl];
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHFirstPageTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (ZHFirstPageFirstPageHeaderView *)headerView{
    if (_headerView==nil) {
        _headerView = [[ZHFirstPageFirstPageHeaderView alloc]init];

    }
    return _headerView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (NSMutableArray *)adsDataSource{
    if (_adsDataSource==nil) {
        _adsDataSource = [NSMutableArray array];
    }
    return _adsDataSource;
}
- (NSMutableArray *)commonDataSource{
    if (_commonDataSource==nil) {
        _commonDataSource = [NSMutableArray array];
    }
    return _adsDataSource;
}

- (void)viewWillAppear:(BOOL)animated{
    if (self.tableView) {
        [self.tableView reloadData];
    }
}
@end
