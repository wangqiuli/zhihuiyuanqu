//
//  ZHFirstPageDiscountViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageDiscountViewController.h"
#import "ZHReleaseDiscountViewController.h"
#import "ZHFirstPageTableViewCell.h"
#import "ZHTransferViewController.h"
#import "ZHDiscountModel.h"

#define button_Tag 100
@interface ZHFirstPageDiscountViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
}
@property (weak, nonatomic) IBOutlet UIButton *classButton;//分类按钮
@property (weak, nonatomic) IBOutlet UIButton *sortButton;//智能排序按钮
@property (weak, nonatomic) IBOutlet UIButton *nearbyButton;//附近按钮

@property(nonatomic,strong)UIScrollView *scrollView;//分类视图
@property(nonatomic,strong)UIScrollView *sortScrollView;//智能排序视图
@property(nonatomic,strong)UIScrollView *nearbyScrollView;//附近视图

@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,strong)NSArray *classSourcedata;//分类数据源
@property(nonatomic,strong)NSArray *sortSourcedata;//智能排序数据源
@property(nonatomic,strong)NSArray *nearbySourcedata;//附近数据源

@property(nonatomic,strong)UITableView *tableView;

@end

@implementation ZHFirstPageDiscountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lastId = 0;
    ret = YES;
    [self setUpNavigationItem];
    [self requestData];
    [self.view addSubview:self.tableView];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"折扣信息";

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"tianjia"] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemAction)];
    
}
- (void)rightBarButtonItemAction{
    ZHReleaseDiscountViewController *release = [[ZHReleaseDiscountViewController alloc]init];
    release.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:release animated:YES];
}

#pragma mark 网络请求
- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];

    if (token==nil) {
        [self showHint:@"请先登录"];
        return;
    }
    if (ret == YES) {
        lastId2 = @"0";
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"type":@"美容",
                                 @"address":@"",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_Discount_URL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"折扣获取数据失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)tableViewData:(NSDictionary *)data{

    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHDiscountModel *model = [[ZHDiscountModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId + 10;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark tableView的协议方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHFirstPageTableViewCell" owner:self options:nil].lastObject;
    }
    
    cell.model = self.dataSource[indexPath.section];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHFirstPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.dataSource[indexPath.section];
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZHTransferViewController *detail = [[ZHTransferViewController alloc]init];
    ZHDiscountModel *model = self.dataSource[indexPath.row];
    detail.id = model.discount.id;
    detail.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detail animated:YES];
}
#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-80-40) style:UITableViewStyleGrouped];
        NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
        //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
        [center1 addObserver:self selector:@selector(noticAction) name:@"updateTableViewY" object:nil];

        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        // 刷新集成
        [self addRefreshControl];
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHFirstPageTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}
- (void)noticAction{
#warning 上面的类型选择被覆盖
//    self.tableView.frame = CGRectMake(0, 40, SCREEN_WIDTH, SCREEN_HEIGHT-104-40);
//    NSLog(@"%f",CGRectGetMinY(self.tableView.frame));
//    [self.tableView reloadData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateTableViewY" object:nil];
}
#pragma mark 点击三种选项
- (IBAction)classButtonAction:(id)sender {
    
    //判断classScrollView是否存在
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
        self.scrollView =nil;
        
    }else{
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 40, SCREEN_WIDTH/3.f, 0)];
        
        [self.view addSubview:self.scrollView];
        [UIView animateWithDuration:0.2 animations:^{
            self.scrollView.frame = CGRectMake(0, 40, SCREEN_WIDTH/3.f, SCREEN_WIDTH/2.f);
        }];
        self.scrollView.backgroundColor = [UIColor lightGrayColor];
        for (int i=0; i<self.classSourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,40*i, SCREEN_WIDTH/3.f, 40);
            [button setTitle:self.classSourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:17];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(classButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
            // 设置contentSize
            self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH/3.f,CGRectGetMaxY(button.frame));
        }
    }
    
}

- (void)classButAction:(UIButton *)button{
    
    [self.classButton setTitle:self.classSourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.classButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.classButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.scrollView removeFromSuperview];
    self.scrollView =nil;
}

- (IBAction)sortButtonAction:(id)sender {
    //判断classScrollView是否存在
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
        self.scrollView =nil;
        
    }else{
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3.f, 40, SCREEN_WIDTH/3.f, 0)];
        self.scrollView.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:self.scrollView];
        [UIView animateWithDuration:0.2 animations:^{
            self.scrollView.frame = CGRectMake(SCREEN_WIDTH/3.f, 40, SCREEN_WIDTH/3.f, SCREEN_WIDTH/2.f);
        }];
        for (int i=0; i<self.sortSourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,40*i, SCREEN_WIDTH/3.f, 40);
            [button setTitle:self.sortSourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:17];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(sortButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
            // 设置contentSize
            self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH/3.f,CGRectGetMaxY(button.frame));
        }
    }

}
- (void)sortButAction:(UIButton *)button{
    [self.sortButton setTitle:self.sortSourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.sortButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.sortButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.scrollView removeFromSuperview];
    self.scrollView =nil;
}

- (IBAction)nearbyButtonAction:(id)sender {
    //判断classScrollView是否存在
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
        self.scrollView =nil;
        
    }else{
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/3.f*2, 40, SCREEN_WIDTH/3.f, 0)];
        self.scrollView.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:self.scrollView];
        [UIView animateWithDuration:0.2 animations:^{
            self.scrollView.frame = CGRectMake(SCREEN_WIDTH/3.f*2, 40, SCREEN_WIDTH/3.f, SCREEN_WIDTH/2.f);
        }];
        for (int i=0; i<self.nearbySourcedata.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            button.frame = CGRectMake(0,40*i, SCREEN_WIDTH/3.f, 40);
            [button setTitle:self.nearbySourcedata[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);//button上的字距离左边的偏移量
            button.titleLabel.font = [UIFont systemFontOfSize:17];
            button.tag = button_Tag+i;
            [button addTarget:self action:@selector(nearbyButAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.scrollView addSubview:button];
            // 设置contentSize
            self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH/3.f,CGRectGetMaxY(button.frame));
        }
    }
}
- (void)nearbyButAction:(UIButton *)button{
    [self.nearbyButton setTitle:self.nearbySourcedata[button.tag-button_Tag] forState:UIControlStateNormal];
    self.nearbyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.nearbyButton.contentEdgeInsets = UIEdgeInsetsMake(6,10, 6, 0);
    [self.scrollView removeFromSuperview];
    self.scrollView =nil;
}
#pragma mark 数据源懒加载
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (NSArray *)classSourcedata{
    if (_classSourcedata==nil) {
        _classSourcedata = @[@"美食",@"美容",@"宠物",@"食品"];
    }
    return _classSourcedata;
}
- (NSArray *)sortSourcedata{
    if (_sortSourcedata==nil) {
        _sortSourcedata = @[@"美食",@"美容",@"宠物"];
    }
    return _sortSourcedata;
}
- (NSArray *)nearbySourcedata{
    if (_nearbySourcedata==nil) {
        _nearbySourcedata = @[@"美食",@"",@"宠物"];
    }
    return _nearbySourcedata;
}

@end
