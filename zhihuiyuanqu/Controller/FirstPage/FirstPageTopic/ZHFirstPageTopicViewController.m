//
//  ZHFirstPageTopicViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageTopicViewController.h"

#import "ZHTopicTableViewCell.h"
#import "ZHActivitysModel.h"
#import "ZHParkingShareViewController.h"
@interface ZHFirstPageTopicViewController ()<UITableViewDelegate,UITableViewDataSource,ZHTopicTableViewCellDelegate>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
}
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation ZHFirstPageTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _lastId = 0;
    ret = YES;
    [self requestData];
    [self.view addSubview:self.tableView];
    
}

- (void)requestData{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    if (token==nil) {
        [self showHint:@"请先登录"];
        return;
    }
    NSString *lat1 = [defaults objectForKey:@"lat"];
    NSString *lat = [NSString stringWithString:lat1];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    
    if (ret == YES) {
        lastId2 = @"0";
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_Active_URL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"活动获取数据错误:%@",error);
        [self stopRefresh];
    }];
    
}
- (void)tableViewData:(NSDictionary *)data{

    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHActivitysModel *model = [[ZHActivitysModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        _lastId = _lastId + 10;
        ret = NO;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    
    //1.停止下拉刷新
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
    
}

#pragma mark cell的协议方法
//协议方法
- (void)tableViewCell:(ZHTopicTableViewCell *)cell didClickedLinkWithData:(ZHActivitysModel *)model{

    ZHParkingShareViewController *detail = [[ZHParkingShareViewController alloc]init];
    detail.id = model.activity.id;
    [self.navigationController pushViewController:detail animated:YES];
}
#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHTopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHTopicTableViewCell" owner:self options:nil].lastObject;
    }
    
    cell.delegate = self;
    ZHActivitysModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHTopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHActivitysModel *model = self.dataSource[indexPath.row];
    cell.model = model;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark tableView的懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-70-40) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;

        // 刷新集成
        [self addRefreshControl];
        
        [_tableView registerNib:[UINib nibWithNibName:@"ZHTopicTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

@end
