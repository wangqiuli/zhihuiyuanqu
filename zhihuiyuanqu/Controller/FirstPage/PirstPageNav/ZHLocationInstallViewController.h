//
//  ZHLocationInstallViewController.h
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHBaseViewController.h"

@interface ZHLocationInstallViewController : ZHBaseViewController

@property(nonatomic,strong)NSArray *defaultSource;

@property(nonatomic,strong)NSArray *addSource;

@end
