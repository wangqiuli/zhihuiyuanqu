//
//  ZHReadilyViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/11.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHReadilyViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "WQLNavViewController.h"

#define Icon_Tag 300

@interface ZHReadilyViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>

@property(nonatomic,strong)UIScrollView *scrollView;

@property(nonatomic,strong)UITextView *textView;
@property(nonatomic,strong)UIButton *button;//点击读取按钮
@property(nonatomic,strong)UILabel *label;

@property(nonatomic,strong)NSMutableArray *iconsArray;//存储读取的图片
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,assign)NSInteger tagIndex;
@end

@implementation ZHReadilyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setUpNavigationItem];
    
    [self.view addSubview:self.scrollView];
    
    self.tagIndex = 0;
}

#pragma mark 输入框
- (UITextView *)textView{
    if (_textView==nil) {
        _textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, SCREEN_WIDTH/2)];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.layer.cornerRadius = 5;
        _textView.layer.masksToBounds = YES;
        _textView.layer.borderWidth = 1.5;
        _textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        _textView.delegate = self;
        self.label = [[UILabel alloc]initWithFrame:CGRectMake(10,0, SCREEN_WIDTH-20, 30)];
        self.label.text = @"拍下小区情况 反馈给物业...";
        self.label.enabled = NO;
        self.label.backgroundColor = [UIColor clearColor];
        [_textView addSubview:self.label];
    }
    return _textView;
}

#pragma mark textView的协议方法
- (void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length==0) {
        self.label.hidden = NO;
    }else{
        self.label.hidden = YES;
    }
}

- (UIButton *)button{
    if (_button==nil) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    
    float width = (SCREEN_WIDTH-60)/5.f;
    
    if(self.iconsArray.count == 0){
        _button.frame = CGRectMake(10,CGRectGetMaxY(self.textView.frame)+10, width, width);
        
        [_button setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        _button.backgroundColor = [UIColor lightGrayColor];
        [_button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    }else{
        
        NSInteger i = self.iconsArray.count - 1;
        
        
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.textView.frame)+ 10+i/5*(width+10), width, width)];
        self.icon.image = self.iconsArray[i];
        self.icon.tag = self.tagIndex++;
        self.icon.userInteractionEnabled = YES;
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        [self.scrollView addSubview:self.icon];
        
        _button.frame = CGRectMake(self.iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.textView.frame)+ 10+self.iconsArray.count/5*(width+10), width, width );
        [_button setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        _button.backgroundColor = [UIColor lightGrayColor];
        [_button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:_button];
        self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(_button.frame)+74);
    }
    
    return _button;
}

//删除读取的相片
- (void)iconAction:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag ;
    UIImageView *icon = [self.scrollView viewWithTag:tag];
    NSUInteger index = [self.iconsArray indexOfObject:icon.image];
    [self.iconsArray removeObjectAtIndex:index];
    [self delete];
}


- (void)delete{
    
    
    for (UIView* uiImageView in self.scrollView.subviews) {
        if( [uiImageView  isKindOfClass :[UIImageView class]]){
            [uiImageView removeFromSuperview];
        }
    }
    
    float width = (SCREEN_WIDTH-60)/5.f;
    
    for (int i = 0 ; i < self.iconsArray.count ; i++) {
    
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(i%5*(width+10)+10, CGRectGetMaxY(self.textView.frame)+ 10+i/5*(width+10), width, width)];
        self.icon.image = self.iconsArray[i];
        self.icon.tag = self.tagIndex++;
        self.icon.userInteractionEnabled = YES;
        [self.icon addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iconAction:)]];
        [self.scrollView addSubview:self.icon];
        
       
    }
    
    _button.frame = CGRectMake(self.iconsArray.count%5 * (width+10)+10,CGRectGetMaxY(self.textView.frame)+ 10+self.iconsArray.count/5*(width+10), width, width );
    [_button setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
    _button.backgroundColor = [UIColor lightGrayColor];
    [_button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:_button];
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(_button.frame)+74);
}
- (void)buttonAction{
    //读取相册和相机
    [self editImageSelect];
}
#pragma mark 读取相机和相册
- (void)editImageSelect{
    UIAlertController *alertController = [[UIAlertController alloc]init];
    
    UIAlertAction *action0 = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = YES;
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showAlert:@"提示" message:@"你没有摄像头" click:^(BOOL ret) {
            }];
        }
        
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.allowsEditing = YES;
            controller.delegate = self;

            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showAlert:@"提示" message:@"你没有相册" click:^(BOOL ret) {
            }];
        }
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:action0];
    [alertController addAction:action1];
    [alertController addAction:action2];
    
    [self presentViewController:alertController animated:YES completion:nil];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        if ([picker allowsEditing]) {
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerEditedImage]];

        }else{
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerOriginalImage]];
            
        }
        NSLog(@"self.iconsArray添加了%zd个",self.iconsArray.count);
        if (self.button!=nil) {
            [self.button removeFromSuperview];
            self.button =nil;
            [self button];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma scrollView的懒加载
- (UIScrollView *)scrollView{
    if (_scrollView==nil) {
        _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
        _scrollView.contentOffset = CGPointMake(0, 0);
        _scrollView.pagingEnabled = YES;
        
        [_scrollView addSubview:self.textView];
        [_scrollView addSubview:self.button];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.button.frame));
    }
    return _scrollView;
}
#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"随手拍";
    self.navigationController.navigationBar.hidden = NO;

    self.navigationController.navigationBar.translucent = YES;
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(rightItem)];
}
- (void)rightItem{
    
}

- (NSMutableArray *)iconsArray{
    if (_iconsArray==nil) {
        _iconsArray = [NSMutableArray array];
    }
    return _iconsArray;
}
- (void)viewWillAppear:(BOOL)animated{
    //隐藏tabBar
    self.tabBarController.tabBar.hidden = YES;
}
@end
