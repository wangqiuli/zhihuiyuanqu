//
//  ZHUploadActiveViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/28.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHUploadActiveViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ZHUploadActiveTableViewCell.h"
#import "PickerChoiceView.h"
@interface ZHUploadActiveViewController ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate,AMapSearchDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,uploadPhotosDelegate,StartPickerDelegate,UIGestureRecognizerDelegate>
{
    MAMapView *_mapView;
    AMapSearchAPI  *_search;
    MAPointAnnotation *pointAnnotation;
    BOOL ret;//判断从首次进入的时候调用一次
}
/** 界面展示 */
@property (nonatomic, strong) UITableView *tableView;

/** 全局数组, 用来存放分组的数据 */
@property (nonatomic, strong) NSArray *dataSource;
@property(nonatomic,strong)NSMutableDictionary *dictData;//大字典
@property(nonatomic,strong)NSMutableDictionary *dict;//小字典
@property(nonatomic,strong)UIButton *releaseButton;//发布按钮
@property(nonatomic,strong)NSMutableArray *iconsArray;//存储读取的相片
@property(nonatomic,strong)NSMutableDictionary *iconsDict;//存储上传后的图片

@end

@implementation ZHUploadActiveViewController

#define PATH_SearchHomeURL @"http://restapi.amap.com/v3/place/around?"
#define SearchHomeAPI @"d91713e7995b7ce5df6f6d250914e753"

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ret = YES;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.releaseButton];
    [self.view bringSubviewToFront:self.releaseButton];
    
    [self setUpNavigationItem];
    [self setNotification];
    [self createMAMapServices];
}
#pragma mark PickChoiceView的协议方法
- (void)StartPickerSelectorIndixString:(NSString *)str{
    NSNotification *notic = [NSNotification notificationWithName:@"UploadActivestartButtonStr" object:nil userInfo:@{@"startButton":str}];
    //发送消息
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
}

#pragma mark cell的协议方法
- (void)uploadPhotos:(UIButton *)button{
    //读取相册和相机
    [self editImageSelect];
}

#pragma mark 读取相机和相册
- (void)editImageSelect{
    UIAlertController *alertController = [[UIAlertController alloc]init];
    
    UIAlertAction *action0 = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = YES;
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有摄像头"];
        }
        
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *controller = [[UIImagePickerController alloc]init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.allowsEditing = YES;
            controller.delegate = self;
            
            [self presentViewController:controller animated:YES completion:nil];
        }else{
            [self showHint:@"你没有相册"];
        }
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:action0];
    [alertController addAction:action1];
    [alertController addAction:action2];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        if ([picker allowsEditing]) {
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerEditedImage]];
            
        }else{
            [self.iconsArray addObject:[info objectForKey:UIImagePickerControllerOriginalImage]];
            
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    //修改界面上的图片
    [self changeUserImage];
    [self.tableView reloadData];
}
- (void)changeUserImage{
    
    NSNotification *notic = [NSNotification notificationWithName:@"UploadActiveDiscountreadPhoto" object:nil userInfo:@{@"image":self.iconsArray}];
    [[NSNotificationCenter defaultCenter]postNotification:notic];
    
    //将图片存到本地
    [self saveImage:self.iconsArray.firstObject];
}
- (void)saveImage:(UIImage *)image{
    NSData* imageData = UIImageJPEGRepresentation(image, 0.5);
    NSDictionary *fileParameters = @{@"image":imageData,
                                     @"fileKey":@"file",
                                     @"nameKey":@"file",
                                     @"mimeType":@"image/jpeg"};
    
    [ZHRequestData uploadFilesWithSubpath:PATH_uploadFileURL parameters:nil fileArray:@[fileParameters] success:^(id json) {
        NSLog(@"折扣发布图片上传成功%@",json);
        [self.iconsDict setObject:json[@"returning"] forKey:@"image"];
        
    } failure:^(NSError *error) {
        NSLog(@"折扣发布图片上传失败:%@",error);
    }];
}

#pragma mark 高德地图
- (void)createMAMapServices{
    //配置用户Key
    [MAMapServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";
    
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
    _mapView.userInteractionEnabled = YES;
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;//YES 为打开定位，NO为关闭定位
    [_mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];//地图跟着位置移动
    UITapGestureRecognizer *mTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    mTap.delegate = self;
    [_mapView addGestureRecognizer:mTap];
}
//当位置更新时，会进定位回调，通过回调函数，能获取到定位点的经纬度坐标，
- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"当前的经纬度坐标：latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
        NSString *lat = [NSString stringWithFormat:@"%f",userLocation.coordinate.latitude];
        NSString *lng = [NSString stringWithFormat:@"%f",userLocation.coordinate.longitude];
        [self.dictData setValue:lat forKey:@"lat"];
        [self.dictData setValue:lng forKey:@"lng"];
        if (ret==YES) {
            [self createSearchAPIWithLatitude:userLocation.coordinate.latitude Longitude:userLocation.coordinate.longitude];
            ret = NO;
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:lat forKey:@"lat"];
        [defaults setObject:lng forKey:@"lng"];
    }
    
}
- (void)createSearchAPIWithLatitude:(CGFloat)latitude Longitude:(CGFloat)longitude{
    [AMapSearchServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";
    
    //初始化检索对象
    _search = [[AMapSearchAPI alloc] init];
    _search.delegate = self;
    
    //构造AMapReGeocodeSearchRequest对象
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    regeo.location = [AMapGeoPoint locationWithLatitude:latitude     longitude:longitude];
    regeo.radius = 10000;
    regeo.requireExtension = YES;
    
    //发起逆地理编码
    [_search AMapReGoecodeSearch: regeo];
}
//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        NSString *result4 = [NSString stringWithFormat:@"%@", response.regeocode.formattedAddress];//详细
        NSLog(@"当前位置: %@",result4);
        [self.dictData setValue:result4 forKey:@"address"];
        NSUserDefaults *defults = [NSUserDefaults standardUserDefaults];
        [defults setObject:result4 forKey:@"address"];
    }
}
#pragma 点击地图的手势
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
- (void)tapPress:(UIGestureRecognizer*)gestureRecognizer {
    
    CGPoint touchPoint = [gestureRecognizer locationInView:_mapView];//这里touchPoint是点击的某点在地图控件中的位置
    CLLocationCoordinate2D touchMapCoordinate =
    [_mapView convertPoint:touchPoint toCoordinateFromView:_mapView];//这里touchMapCoordinate就是该点的经纬度了
    
    if (pointAnnotation) {
        [_mapView removeAnnotation:pointAnnotation];
    }
    pointAnnotation = [[MAPointAnnotation alloc] init];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake(touchMapCoordinate.latitude, touchMapCoordinate.longitude);
    [_mapView addAnnotation:pointAnnotation];
    [self requestSearchHomeWithLng:touchMapCoordinate.longitude Lat:touchMapCoordinate.latitude];
}
#pragma mark 网络请求搜索小区
- (void)requestSearchHomeWithLng:(CGFloat)lng Lat:(CGFloat)lat{
    
    NSString *location = [NSString stringWithFormat:@"%f,%f",lng,lat];
    
    NSDictionary *parameters = @{@"key":SearchHomeAPI,@"location":location,@"types":@"110000"};
    [ZHRequestData postWithURL:PATH_SearchHomeURL params:parameters success:^(id json) {
        
        [self searchData:json];
    } failure:^(NSError *error) {
        NSLog(@"搜索地点失败：%@",error);
    }];
    
}
- (void)searchData:(NSDictionary *)data{
    NSArray *allData = data[@"pois"];
    NSDictionary *info = allData.firstObject;
    NSNotification *notic = [NSNotification notificationWithName:@"updateUserPlace" object:nil userInfo:@{@"address":info[@"name"]}];
    //发送消息
    [[NSNotificationCenter defaultCenter]postNotification:notic];

}

#pragma mark - notification handler
- (void)setNotification{
    // 设置键盘监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //接受信息填写不完整的通知
    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
    [center1 addObserver:self selector:@selector(center1NoticAction) name:@"UploadActiveDiscount Incomplete information" object:nil];
    
    //接受信息填写完整的通知
    NSNotificationCenter *center2 = [NSNotificationCenter defaultCenter];
    [center2 addObserver:self selector:@selector(center2NoticAction:) name:@"UploadActiveDiscount Complete information" object:nil];
    
    //接受点击开始按钮的通知
    NSNotificationCenter *startCenter = [NSNotificationCenter defaultCenter];
    [startCenter addObserver:self selector:@selector(startAction) name:@"UploadActiveDiscountStartButton" object:nil];

}
- (void)startAction{
    PickerChoiceView *picker = [[PickerChoiceView alloc]initWithFrame:self.view.bounds];
    picker.StartDelegate = self;
    picker.arrayType = DeteArray;
    [self.view addSubview:picker];
}
- (void)stopAction{
    PickerChoiceView *picker = [[PickerChoiceView alloc]initWithFrame:self.view.bounds];
    picker.StartDelegate = self;
    picker.arrayType = DeteArray;
    [self.view addSubview:picker];
}

- (void)didKeyboardShow:(NSNotification *)noti
{
    CGRect frame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64 - frame.size.height );
    }];
}

- (void)didKeyboardHide:(NSNotification *)noti
{
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-104);
    }];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"发布活动信息";

}
#pragma mark 发布按钮
- (UIButton *)releaseButton{
    if (_releaseButton==nil) {
        _releaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _releaseButton.frame = CGRectMake(0, SCREEN_HEIGHT-104, SCREEN_WIDTH, 40);
        _releaseButton.backgroundColor = [UIColor lightGrayColor];
        _releaseButton.layer.borderWidth = 1;
        [_releaseButton setTitle:@"确认发布" forState:UIControlStateNormal];
        [_releaseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_releaseButton addTarget:self action:@selector(releaseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _releaseButton;
}
- (void)releaseButtonAction:(UIButton *)button{
    
    if (self.iconsDict[@"image"]==nil) {
        [self showHint:@"请上传图片"];
        
    }else{
        
        //点击确认发布按钮的通知(传送当前地址)
        NSNotification *notic = [NSNotification notificationWithName:@"UploadActiveDiscountClickReleaseButton" object:nil userInfo:@{@"address":self.dictData[@"address"],@"image":self.iconsDict[@"image"]}];
        [[NSNotificationCenter defaultCenter]postNotification:notic];
    }
    
}
- (void)center1NoticAction{
    [self showHint:@"信息填写不完整"];
}
- (void)center2NoticAction:(NSNotification *)notification{
    NSDictionary *dict = notification.userInfo;
    [self.dictData setValue:dict forKey:@"activity"];
    //请求数据
    [self requestData:self.dictData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UploadActiveDiscount Incomplete information" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UploadActiveDiscount Complete information" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"UploadActiveDiscountStartButton" object:nil];
}

#pragma mark 请求数据
- (void)requestData:(NSDictionary *)dict{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    
    NSDictionary *parameters = @{@"token":token,@"jsonStr":[dict jsonData]};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_ReleaseActiveURL parameters:parameters success:^(id json) {
        if ([json[@"message"] isEqualToString:@"接口调用成功！"]) {
            
            [self showHint:@"发布成功"];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"发布活动失败：%@",error);
    }];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SCREEN_WIDTH;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHUploadActiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (self.iconsArray.count>0) {
        cell.photosArray = self.iconsArray;
        return cell.maxY;
    }
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHUploadActiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHUploadActiveTableViewCell" owner:self options:nil].lastObject;
    }
    cell.uploadDelegate = self;
    if (self.iconsArray!=nil) {
        cell.photosArray = self.iconsArray;
    }
    ZHFirstPageAdsModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _mapView;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-104) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //滑动tableView收缩键盘
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHUploadActiveTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = @[@""];
        
    }
    return _dataSource;
}
- (NSMutableArray *)iconsArray{
    if (_iconsArray==nil) {
        _iconsArray = [NSMutableArray array];
    }
    return _iconsArray;
}
- (NSMutableDictionary *)dictData{
    if (_dictData==nil) {
        _dictData = [NSMutableDictionary dictionary];
    }
    return _dictData;
}
- (NSMutableDictionary *)dict{
    if (_dict==nil) {
        _dict = [NSMutableDictionary dictionary];
    }
    return _dict;
}
- (NSMutableDictionary *)iconsDict{
    if (_iconsDict == nil) {
        _iconsDict = [NSMutableDictionary dictionary];
    }
    return _iconsDict;
}

@end
