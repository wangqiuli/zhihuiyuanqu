//
//  ZHUploadHomeViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/6/20.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHUploadHomeViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "ZHUploadHomeTableViewCell.h"
#import "ZHSearchHomeModel.h"
@interface ZHUploadHomeViewController ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate,AMapSearchDelegate,UIGestureRecognizerDelegate>
{
    MAMapView *_mapView;
    AMapSearchAPI  *_search;
    MAPointAnnotation *pointAnnotation;
    BOOL ret;//判断从首次进入的时候调用一次
}
/** 界面展示 */
@property (nonatomic, strong) UITableView *tableView;

/** 全局数组, 用来存放分组的数据 */
@property (nonatomic, strong) NSMutableArray *dataSource;
@property(nonatomic,strong)NSMutableDictionary *dict;//小字典
@end

@implementation ZHUploadHomeViewController

//搜索小区url，API
#define PATH_SearchHomeURL @"http://restapi.amap.com/v3/place/around?"
#define SearchHomeAPI @"d91713e7995b7ce5df6f6d250914e753"
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ret = YES;
    [self.view addSubview:self.tableView];
    [self createMAMapServices];
    [self setNavigationItem];
    [self setNotification];
}
#pragma mark - notification handler
- (void)setNotification{
    // 设置键盘监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)didKeyboardShow:(NSNotification *)noti
{
    CGRect frame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.view.frame.size.height - frame.size.height-10 );
    }];
}

- (void)didKeyboardHide:(NSNotification *)noti
{
    [UIView animateWithDuration:0.25f animations:^{
        self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40);
    }];
}

#pragma mark 上传小区
- (void)setNavigationItem{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(rightItemAction)];
}
- (void)rightItemAction{
    [self requestUploadData];
}
- (void)requestUploadData{
    ZHSearchHomeModel *model = self.dataSource.firstObject;
    NSString *location = model.location;
    NSArray *locations = [location componentsSeparatedByString:@","];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSDictionary *dict = @{@"address":model.address,@"home":@{@"address":model.address,@"home":model.name,@"phone":model.tel},@"homeName":model.name,@"lat":locations.lastObject,@"lng":locations.firstObject};
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_ReleaseHomeURL parameters:@{@"token":token,@"jsonStr":[dict jsonData]} success:^(id json) {
        NSLog(@"%@",json);
        if (json[@"returning"]) {
            [self showHint:@"上传小区成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else if ([json[@"message"] isEqualToString:@"该小区名称已存在"]){
            [self showHint:@"该小区名称已存在"];
        }
    } failure:^(NSError *error) {
        NSLog(@"上传小区错误:%@",error);
    }];
}

#pragma 点击地图的手势
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
- (void)tapPress:(UIGestureRecognizer*)gestureRecognizer {
    
    CGPoint touchPoint = [gestureRecognizer locationInView:_mapView];//这里touchPoint是点击的某点在地图控件中的位置
    CLLocationCoordinate2D touchMapCoordinate =
    [_mapView convertPoint:touchPoint toCoordinateFromView:_mapView];//这里touchMapCoordinate就是该点的经纬度了

    if (pointAnnotation) {
        [_mapView removeAnnotation:pointAnnotation];
    }
    pointAnnotation = [[MAPointAnnotation alloc] init];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake(touchMapCoordinate.latitude, touchMapCoordinate.longitude);
    [_mapView addAnnotation:pointAnnotation];
    [self requestSearchHomeWithLng:touchMapCoordinate.longitude Lat:touchMapCoordinate.latitude];
}

#pragma mark 网络请求搜索小区
- (void)requestSearchHomeWithLng:(CGFloat)lng Lat:(CGFloat)lat{

    NSString *location = [NSString stringWithFormat:@"%f,%f",lng,lat];
    
    NSDictionary *parameters = @{@"key":SearchHomeAPI,@"location":location,@"types":@"120000"};
    [ZHRequestData postWithURL:PATH_SearchHomeURL params:parameters success:^(id json) {

        if (self.dataSource.count!=0) {
            [self.dataSource removeAllObjects];
        }
        [self searchData:json];
    } failure:^(NSError *error) {
        NSLog(@"搜索小区失败：%@",error);
    }];
 
}
- (void)searchData:(NSDictionary *)data{
    NSArray *allData = data[@"pois"];
    NSDictionary *info = allData.firstObject;
    ZHSearchHomeModel *model = [[ZHSearchHomeModel alloc]init];
    model.ID = info[@"id"];
    model.name = info[@"name"];
    model.address = info[@"address"];
    model.location = info[@"location"];
    if ([info[@"tel"] isKindOfClass:[NSArray class]]) {
        model.tel = @"";
    }else{
    model.tel = info[@"tel"];
    }
    [self.dataSource addObject:model];
    [self.tableView reloadData];
}

#pragma mark 高德地图
- (void)createMAMapServices{
    //配置用户Key
    [MAMapServices sharedServices].apiKey = @"d243522a442101b43ddf8d9088676ef3";
    
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2.f)];
    _mapView.delegate = self;
    _mapView.userInteractionEnabled = YES;
    _mapView.showsUserLocation = YES;//YES 为打开定位，NO为关闭定位
    [_mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];//地图跟着位置移动
    UITapGestureRecognizer *mTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    mTap.delegate = self;
    [_mapView addGestureRecognizer:mTap];
}
//当位置更新时，会进定位回调，通过回调函数，能获取到定位点的经纬度坐标，
- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"当前的经纬度坐标：latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
        NSString *lat = [NSString stringWithFormat:@"%f",userLocation.coordinate.latitude];
        NSString *lng = [NSString stringWithFormat:@"%f",userLocation.coordinate.longitude];
        if (ret==YES) {
            [self requestSearchHomeWithLng:userLocation.coordinate.longitude Lat:userLocation.coordinate.latitude];
            ret = NO;
        }

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:lat forKey:@"lat"];
        [defaults setObject:lng forKey:@"lng"];
    }
    
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SCREEN_HEIGHT/2.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHUploadHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ZHSearchHomeModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell.maxY;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHUploadHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHUploadHomeTableViewCell" owner:self options:nil].lastObject;
    }
    
    ZHSearchHomeModel *model = self.dataSource.firstObject;
    cell.model = model;
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _mapView;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-40) style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        //滑动tableView收缩键盘
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHUploadHomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (NSMutableDictionary *)dict{
    if (_dict==nil) {
        _dict = [NSMutableDictionary dictionary];
    }
    return _dict;
}


@end
