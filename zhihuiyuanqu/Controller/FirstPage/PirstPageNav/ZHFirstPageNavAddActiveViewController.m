//
//  ZHFirstPageNavAddActiveViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageNavAddActiveViewController.h"

#import "ZHReadilyViewController.h"
#import "ZHUploadActiveViewController.h"
@interface ZHFirstPageNavAddActiveViewController ()
@property(nonatomic,strong)UIImageView *imageView;
@end

@implementation ZHFirstPageNavAddActiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    [self.view addSubview:self.imageView];
    [self.view sendSubviewToBack:self.imageView];
   
}
- (UIImageView *)imageView{
    if (_imageView==nil) {
        _imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"background"]];
        _imageView.frame = self.view.bounds;
    }
    return _imageView;
}
- (IBAction)readilyButton:(id)sender {
    ZHReadilyViewController *readily = [[ZHReadilyViewController alloc]init];
    [self.navigationController pushViewController:readily animated:YES];
}
- (IBAction)activeButton:(id)sender {
    
    ZHUploadActiveViewController *readily = [[ZHUploadActiveViewController alloc]init];
    [self.navigationController pushViewController:readily animated:YES];
}
- (IBAction)neighborButton:(id)sender {
}
- (IBAction)backButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}

@end
