//
//  ZHAddPlaceViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHAddPlaceViewController.h"
#import "ZHUploadHomeViewController.h"
#import "ZHAddPlacesTableViewCell.h"
#import "ZHHomeModel.h"

@interface ZHAddPlaceViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    NSInteger _lastId;//从第几条开始
    BOOL ret;//判断是刷新还是加载（YES是刷新）
    BOOL isSearch;//判断有没有编辑
}
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UISearchBar *search;
@property(nonatomic,strong)NSMutableDictionary *checkedDictionary;//判断是否选中
@property(nonatomic,strong)NSMutableArray *dataSource;
@property(nonatomic,strong)NSMutableArray *searchResults;//搜索结果
@property(nonatomic,strong)NSMutableArray *selectSource;//存储选中的小区model
@property(nonatomic,strong)NSMutableArray *arrayDict;//把保存的小区字典再放到数组里面

@end

@implementation ZHAddPlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lastId = 0;
    ret = YES;
    isSearch = NO;
    //在导航栏上添加搜索栏
    [self addSearchBar];
    [self requestData];
    [self setUpNavigationItem];
    [self.view addSubview:self.tableView];
}

#pragma mark 请求数据
- (void)requestData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];
    NSString *lastId1 = [NSString stringWithFormat:@"%zd",_lastId];
    NSString *lastId2 = [NSString stringWithString:lastId1];
    if (token==nil||lat==nil||lng==nil) {
        return;
    }
    if (ret == YES) {
        lastId2 = @"0";
    }
    NSDictionary *parameters = @{@"token":token,
                                 @"keyword":@"",
                                 @"address":@"",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":lastId2,
                                 @"distance":@"10000"};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_HomeListURL parameters:parameters success:^(id json) {
        if (ret == YES ) {
            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        }
        
        [self tableViewData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"添加小区失败:%@",error);
        [self stopRefresh];
    }];

}
- (void)tableViewData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHHomeModel *model = [[ZHHomeModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        ret = YES;
        [self requestData];
    }];
    self.tableView.mj_header = header;
    
    //2.上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        ret = NO;
        _lastId = _lastId + 10;
        [self requestData];
    }];
    self.tableView.mj_footer = footer;
}
- (void)stopRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(rightBatButtonItemAction)];
    
}
- (void)rightBatButtonItemAction{
   
    [self showAlert:@"提示" message:@"确定要保存并退出？" click:^(BOOL ret1) {
            if (ret1) {
                
                //调用保存常用小区的接口
                [self requestDataSaveHome];
                [self.search removeFromSuperview];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                NSLog(@"取消");
            }
        }];
   
}

#pragma mark 保存常用小区
- (void)requestDataSaveHome{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *userId = [defaults objectForKey:@"userId"];
    if (token==nil) {
        [self showHint:@"用户没有登录"];
        return;
    }
    
    if (self.arrayDict!=nil) {
        [self.arrayDict removeAllObjects];
    }
    
    for (ZHHomeModel *model in self.selectSource) {
        Home *home = model.home;
        NSString *homeId1 = [NSString stringWithFormat:@"%zd",home.id];
        NSString *homeId = [NSString stringWithString:homeId1];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:homeId forKey:@"homeId"];
        [dict setValue:@"0" forKey:@"state"];
        [dict setValue:userId forKey:@"userId"];
        [self.arrayDict addObject:dict];
    }
    
    NSError *parseError = nil;
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:self.arrayDict options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonValue = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSDictionary *parameters = @{@"token":token,
                                 @"jsonStr":jsonValue
                                 };
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SaveHomeURL parameters:parameters success:^(id json) {
        NSLog(@"保存常用小区成功:%@",json[@"message"]);
    } failure:^(NSError *error) {
        NSLog(@"保存常用小区失败:%@",error);
        
    }];

}

- (void)addSearchBar{
    self.search = [[UISearchBar alloc]initWithFrame:CGRectMake(100, 14, SCREEN_WIDTH-200, 51)];
    self.search.showsCancelButton = NO;
    self.search.barTintColor = [UIColor lightGrayColor];
    self.search.delegate = self;
    //去除searchBar的边框线
    self.search.layer.borderWidth = 1;
    self.search.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.search.placeholder = @"小区名称";
    [self.navigationController.view addSubview:self.search];
}
#pragma mark 搜索框的协议方法
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    isSearch = YES;
    [self.tableView reloadData];
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    //添加之前先清空原来的内容
    if (self.searchResults!=nil) {
        [self.searchResults removeAllObjects];
    }
    //遍历所有的数据，将符合条件的添加到搜索的结果中
    for (ZHHomeModel *model in self.dataSource) {
        
        //搜索的判断
        if ([model.homeName containsString:searchText]) {
            [self.searchResults addObject:model];
        }
    }
    [self.tableView reloadData];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isSearch) {
        return self.searchResults.count+1;
    }
    return self.dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellSystem = [tableView dequeueReusableCellWithIdentifier:@"cellSystem"];
    if (cellSystem==nil) {
        cellSystem = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellSystem"];
    }
    
    ZHAddPlacesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHAddPlacesTableViewCell" owner:self options:nil].lastObject;
    }
    if (isSearch) {//搜索状态
        if (indexPath.row==self.searchResults.count||self.searchResults.count==0) {//最后一行状态
            cellSystem.textLabel.textAlignment = NSTextAlignmentCenter;
            cellSystem.textLabel.text = @"没有找到?点击上传您需要的小区";
            cellSystem.selectionStyle = NO;
            return cellSystem;
        }else{//其他行
            ZHHomeModel *model = self.searchResults[indexPath.row];
            cell.model = model;
            return cell;
        }
    }else{
        ZHHomeModel *model = self.dataSource[indexPath.row];
        cell.model = model;
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    ZHAddPlacesTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    ZHHomeModel *model;
    if (isSearch) {
        if (indexPath.row==self.searchResults.count||self.searchResults.count==0) {
            ZHUploadHomeViewController *upload = [[ZHUploadHomeViewController alloc]init];
            [self.navigationController pushViewController:upload animated:YES];
            //点击跳转
            return;
        }else{
        model = self.searchResults[indexPath.row];
        }
    }else{
        model = self.dataSource[indexPath.row];
    }
    
    if(self.checkedDictionary[[NSString stringWithFormat:@"%zd",indexPath.row]]){
        [self.selectSource removeObjectAtIndex:indexPath.row];
        self.checkedDictionary[[NSString stringWithFormat:@"%zd",indexPath.row]] =  nil;
    }else{
       
        [self.selectSource addObject:model];
        [self.checkedDictionary setValue:@"YES" forKey:[NSString stringWithFormat:@"%zd",indexPath.row]];
    }
    
    //改变按钮的选中状态
    [cell clickButton];

}
#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];

        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addRefreshControl];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHAddPlacesTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource==nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (NSMutableArray *)searchResults{
    if (_searchResults==nil) {
        _searchResults = [NSMutableArray array];
    }
    
    return _searchResults;
}
- (NSMutableDictionary *)checkedDictionary{
    if (_checkedDictionary==nil) {
        _checkedDictionary = [NSMutableDictionary dictionary];
    }
    return _checkedDictionary;
}
- (NSMutableArray *)selectSource{
    if (_selectSource==nil) {
        _selectSource = [NSMutableArray array];
    }
    return _selectSource;
}

- (NSMutableArray *)arrayDict{
    if (_arrayDict==nil) {
        _arrayDict = [NSMutableArray array];
    }
    return _arrayDict;
}
- (void)viewWillDisappear:(BOOL)animated{
    [self.search removeFromSuperview];
    self.search = nil;
}
- (void)viewDidAppear:(BOOL)animated{
    if (self.search==nil) {
        [self addSearchBar];
    }
}
@end
