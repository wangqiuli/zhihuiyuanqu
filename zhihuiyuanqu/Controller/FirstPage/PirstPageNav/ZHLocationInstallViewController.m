//
//  ZHLocationInstallViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/6.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHLocationInstallViewController.h"

#import "ZHLoactionInstallTableViewCell.h"
#import "ZHAddHomeModel.h"

@interface ZHLocationInstallViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableDictionary *dataSource;
@end

@implementation ZHLocationInstallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self setUpNavigationItem];
}

#pragma mark 请求数据
- (void)requestData:(NSString *)homeId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *userId1 = [NSString stringWithFormat:@"%zd",[defaults objectForKey:@"userId"]];
    NSString *userId = [NSString stringWithString:userId1];
    [self.dataSource setValue:homeId forKey:@"homeId"];
    [self.dataSource setValue:@"1" forKey:@"state"];
    [self.dataSource setValue:userId forKey:@"userId"];
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_SetDefaultHomeURL parameters:@{@"token":token,@"jsonStr":[self.dataSource jsonData]} success:^(id json) {
        NSLog(@"修改默认小区成功:%@",json);
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        NSLog(@"修改默认小区失败:%@",error);
        
    }];

}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"选择默认";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(rightBatButtonItemAction)];
    
}

- (void)rightBatButtonItemAction{
    [self showAlert:@"提示" message:@"确定要保存并退出？" click:^(BOOL ret) {
        if (ret) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *defaultHome = [defaults objectForKey:@"defaultHome"];
            for (ZHAddHomeModel *model in self.addSource) {
                if ([model.home isEqualToString:defaultHome]) {
                    [self requestData:[NSString stringWithFormat:@"%zd",model.id]];
                }
            }
            
        }else{
            NSLog(@"取消");
        }
    }];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHLoactionInstallTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHLoactionInstallTableViewCell" owner:self options:nil].lastObject;
    }
    cell.defaultSource = self.defaultSource;
    cell.addSource = self.addSource;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHLoactionInstallTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.addSource = self.addSource;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.separatorStyle = NO;
        [_tableView registerNib:[UINib nibWithNibName:@"ZHLoactionInstallTableViewCell"  bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableDictionary *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableDictionary dictionary];
    }
    return _dataSource;
}

@end
