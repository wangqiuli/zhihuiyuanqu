//
//  ZHFirstPageNavLocationViewController.m
//  zhihuiyuanqu
//
//  Created by leo on 16/4/5.
//  Copyright © 2016年 wangqiuli. All rights reserved.
//

#import "ZHFirstPageNavLocationViewController.h"
#import "ZHLocationInstallViewController.h"
#import "ZHAddPlaceViewController.h"

#import "ZHNavLocationTableViewCell.h"
#import "ZHHomeModel.h"
#import "ZHAddHomeModel.h"
@interface ZHFirstPageNavLocationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;//存储GPS定位小区
@property(nonatomic,strong)NSMutableArray *defaultSource;//存储默认小区
@property(nonatomic,strong)NSMutableArray *addSource;//存储添加小区

@end

@implementation ZHFirstPageNavLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    [self setUpNavigationItem];
    [self requestGPSData];
    [self.view addSubview:self.tableView];
    
}

#pragma mark 请求GPS定位数据
- (void)requestGPSData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    NSString *lat = [defaults objectForKey:@"lat"];
    NSString *lng = [defaults objectForKey:@"lng"];


    NSDictionary *parameters = @{@"token":token,
                                 @"keyword":@"",
                                 @"address":@"",
                                 @"lat":lat,
                                 @"lng":lng,
                                 @"lastId":@"0",
                                 @"distance":@"10000"};
    
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_HomeListURL parameters:parameters success:^(id json) {

            if (self.dataSource.count!=0) {
                [self.dataSource removeAllObjects];
            }
        
        [self tableViewData:json];
        [self requestDefaultHomeData];
        
    } failure:^(NSError *error) {
        NSLog(@"GPS定位小区失败:%@",error);
        [self stopRefresh];
    }];
    
}
- (void)tableViewData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHHomeModel *model = [[ZHHomeModel alloc]initWithDictionary:info error:nil];
        [self.dataSource addObject:model];
    }
}

#pragma mark 请求默认小区数据
- (void)requestDefaultHomeData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetDefaultHomeURL parameters:@{@"token":token} success:^(id json) {
        if (self.defaultSource.count!=0) {
            [self.defaultSource removeAllObjects];
        }
        
        [self defaultHomeData:json];
        [self requestAddHomeData];
    } failure:^(NSError *error) {
        NSLog(@"添加小区失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)defaultHomeData:(NSDictionary *)data{
    NSDictionary *info = data[@"returning"];
    ZHAddHomeModel *model = [[ZHAddHomeModel alloc]initWithDictionary:info error:nil];
    [self.defaultSource addObject:model];
}

#pragma mark 请求添加常用小区数据
- (void)requestAddHomeData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"token"];
    [ZHRequestData serviceUploadFilesWithSubpath:PATH_GetCommonHomeURL parameters:@{@"token":token} success:^(id json) {
            if (self.addSource.count!=0) {
                [self.addSource removeAllObjects];
            }
        
        [self addHomeData:json];
        [self stopRefresh];
    } failure:^(NSError *error) {
        NSLog(@"添加小区失败:%@",error);
        [self stopRefresh];
    }];
}
- (void)addHomeData:(NSDictionary *)data{
    NSArray *allData = data[@"returning"];
    for (NSDictionary *info in allData) {
        ZHAddHomeModel *model = [[ZHAddHomeModel alloc]initWithDictionary:info error:nil];
        [self.addSource addObject:model];
    }
    [self.tableView reloadData];
}


#pragma mark 刷新
- (void)addRefreshControl{
    
    //1.下拉刷新集成
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestGPSData];
    }];
    self.tableView.mj_header = header;
    
}
- (void)stopRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark cell的协议方法
- (void)defaultPart:(NSString *)name{
  
}

#pragma mark 导航栏
- (void)setUpNavigationItem{
    
    self.title = @"选择小区";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"<返回" style:UIBarButtonItemStyleDone target:self action:@selector(leftBatButtonItemAction)];
    
}
- (void)leftBatButtonItemAction{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNavLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ZHNavLocationTableViewCell" owner:self options:nil].lastObject;
    }
    ZHHomeModel *model = self.dataSource.firstObject;
    cell.addSource = self.addSource;
    cell.model = model;
    ZHAddHomeModel *addHomeModel = self.defaultSource.firstObject;
    cell.addHomeModel = addHomeModel;
    
    //跳转设置界面
    cell.setButtonActionBlock = ^(void){
        
        ZHLocationInstallViewController *install = [[ZHLocationInstallViewController alloc]init];
        install.defaultSource = self.defaultSource;
        install.addSource = self.addSource;
        [self.navigationController pushViewController:install animated:YES];
    };
    
    //跳转添加界面
    cell.addButtonActionBlock = ^(void){
        ZHAddPlaceViewController *add = [[ZHAddPlaceViewController alloc]init];
        [self.navigationController pushViewController:add animated:YES];
    };
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZHNavLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    cell.addSource = self.addSource;
    return cell.maxY;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark 懒加载
- (UITableView *)tableView{
    if (_tableView==nil) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = NO;
        [self addRefreshControl];
        [_tableView registerNib:[UINib nibWithNibName:@"ZHNavLocationTableViewCell"  bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
- (NSMutableArray *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    
    return _dataSource;
}
- (NSMutableArray *)defaultSource{
    if (_defaultSource==nil) {
        _defaultSource = [NSMutableArray array];
    }
    return _defaultSource;
}
- (NSMutableArray *)addSource{
    if (_addSource==nil) {
        _addSource = [NSMutableArray array];
    }
    return _addSource;
}
- (void)viewDidAppear:(BOOL)animated{
    if (self.tableView) {
        [self requestGPSData];
    }
}
@end
